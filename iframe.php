<?php
// payment methods in <iframe>

echo '<!DOCTYPE html><html lang="en"><head>';
include('inc/header_head.php');
if( !$oUser->loginCheck() || !$oUser->getID() ):
	header( 'location:'.$site->baseURL() );
	exit;
endif;
?>
<style>
	html { padding-bottom: 0; }
	body { margin: 0;  }
	table { font-size: 20px; }
	.tbl-heading { border: none !important; }
	form { margin-bottom: 0; }
	div.row { margin-left:0 !important; margin-right:0 !important; }
</style>
</head>
<body>

<div id="loading-overlay" style="display:none"></div>

<?php
$oAccount = new Account( $oUser );
$oUser->_view( $oUser->getID() );
$userROW = $oUser->Record;
$FORM_VISIBLE = 1;

$p = isset($p) ? $p:false;

// form or result? get path
if( ( substr($p,0,4)=='3gdp'     && $p!='3gdp')         ||
    ( substr($p,0,8)=='wirecard' && $p!='wirecard')     ||
    ( substr($p,0,4)=='rave'     && $p!='rave' )        ||
    ( substr($p,0,8)=='paystack' && $p!='paystack' )    ||
    ( substr($p,0,9)=='cashenvoy' && $p!='cashenvoy' )  ||
    ( substr($p,0,6)=='bitpay' && $p!='bitpay' ) ) $path = 'upanel/';
else $path = 'upanel/inc/deposit-';

if( $p && file_exists( $site->basePath( "$path$p.php" ) ) ) {
	include("$path$p.php");
} else {
	header( 'location:'.$site->baseURL( 'error' ) );
	exit;
}
?>

<link rel="stylesheet" type="text/css" href="<?= $site->baseURLM('css/font-awesome.min.css?v='.CSSVERSION) ?>">
<script src="<?= $site->baseURL('assets/js/jquery.validate.js?v='.JSVERSION) ?>"></script>
<script src="<?= $site->baseURL('js/account.min.js?v='.JSVERSION) ?>"></script>
<script src="<?= $site->baseURL('assets/countdown/jquery.countdown.min.js?v='.JSVERSION) ?>"></script>
<script src="<?= $site->baseURLM('js/intlTelInput.min.js?v='.JSVERSION) ?>"></script>
<script src="<?= $site->baseURLM('js/common.min.js?v='.JSVERSION) ?>"></script>

</body></html>
