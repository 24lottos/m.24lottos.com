<?php
ob_start();
session_start();
require_once('./config/config.php');
require_once(PATH2CONFIG.'constants.php');
require_once(BASE_PATH.'config/functions.php');

function __autoload($classname) {
	$lower = strtolower($classname);
	include(BASE_PATH."config/class.{$lower}.php");
}

$club  = false; // isset($_GET['club']); // confirm Membership Club Packages
$site  = new Site;
$oUser = new User;
$p = $_GET ? @$_GET['p'] : NULL;

if($oUser->loginCheck()) {
	$oUser->resetSession($oUser->getID());
	$bet = new Bet;
	$username = $oUser->getName();
	unset($_SESSION['CONFIRMATION_FAILED']);

	$DURATION = isset($_COOKIE['DURATION']) ? (int)$_COOKIE['DURATION'] : 0;
	if( $DURATION>0 && !isset($PACKAGE_DISCOUNT[$DURATION]) ) { // wrong DURATION !!!
		setcookie('DURATION','0',0,'/'); // reset duration to 'single'
		header( 'location: /' );
		exit;
	}

	if( $bet->checkBalance( $club, $DURATION ) ) { // is user having enought funds?
		setcookie('DURATION','0',0,'/'); // reset duration to 'single'
		$order_total = $bet->placeAnOrder( $club, $DURATION );

		// affiliate tracking. DISABLED 2017-11-16, count it on deposit done event
		//if( AFFPRO_PATH && $order_total ) { // a bet has been made - add affiliate-pro sale track ============
		//	$sale_amount = $order_total;
		//	$product	 = AFFPRO_PRODUCTNAME; // 'A bet has been made'
		//	$_include	= AFFPRO_PATH.AFFPRO_TRACK_SALE;
		//	if( is_file($_include) ) @include_once( $_include );
		//}

		header('location:'.$site->baseURLm('account/tickets'));

	} else { //  returns false if there is no enough funds to play or profile does not have a name to print on the ticket.
		$_SESSION['CONFIRMATION_FAILED'] = true;
		header('location:'.$site->baseURLm('account/deposit'));
	}
} else {
	header('location:'.$site->baseURLm('login'));
}
