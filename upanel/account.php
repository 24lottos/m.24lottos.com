<?php
if( isset($_POST['btnAccountUpdate']) ) {
	$oUser->update( $oUser->cleanU(), $oUser->getID() );
	header( 'location:'.$site->baseURLm( 'account/account' ) );
	exit();
}
?>
<style> .lessScroll { display: none !important; } </style>
<section>
	<div class="banner"><?php include('inc/timeline.php') ?></div>
	<?php
	$o = new User;
	$o->_view( $oUser->getID() );
	$o->resetSession( $oUser->getID() );
	if( @$_COOKIE['FILLPROFILEMSG']==1 ) {
		//if( empty($o->Record->name) || empty($o->Record->phone) || empty($o->Record->country) )
		// empty($o->Record->street) || empty($o->Record->city) || empty($o->Record->state)
		setcookie( 'FILLPROFILEMSG', '0', time() - 24*3600, '/' ); // clear cookie
	?><h5 class="green">Congratulations <?= ucfirst( $o->Record->name ) ?>!</h5>
	<p>You have successfully completed 1<sup>st</sup> step in the registration process.<br/>
	Please complete your registration and get ready to play!</p>
	<?php } ?>
	<form class="account-form" id="frmAccountUpdate" method="POST">
		<input type="hidden" name="oldPrefCurrency" value="<?= $_SESSION['logged_pref_currency'] ?>"/>
		<input type="hidden" name="oldCurBalance" value="<?= $_SESSION['logged_cur_balance'] ?>"/>
		<input type="hidden" name="btnAccountUpdate" value="dummy"/>
		<input type="hidden" name="country" id="countryname"/>

		<div class="row">
			<label>Full Name</label>
			<input name="name" class="form-control input-sm required" type="text" value="<?= $o->Record->name ?>">
		</div>
		<div class="row">
			<label>Gender</label>
			<select name="gender" class="input-sm required">
				<option value="Male" <?= $o->Record->gender == 'Male' ? 'selected':'' ?>>Male</option>
				<option value="Female" <?= $o->Record->gender == 'Female' ? 'selected':'' ?>>Female</option>
			</select>
		</div>
		<div class="row">
			<label>Email Address</label>
			<input type="email" name="email" maxlength="50" class="form-control input-sm"
				   value="<?= $o->Record->email ?>" disabled="disabled" style='background:#eee'/>
		</div>
		<div class="row">
			<label>Phone Number</label>
			<input name="phone" maxlength="13" class="form-control input-sm required number" type="text"
				   value="<?= $o->Record->phone ?>"/>
		</div>
		<div class="row">
			<label>Street</label>
			<input name="street" maxlength="200" type="text" class="form-control input-sm"
				   value="<?= $o->Record->street ?>"/>
		</div>
		<div class="row">
			<label>City</label>
			<input name="city" maxlength="50" type="text" class="form-control input-sm"
				   value="<?= $o->Record->city ?>"/>
		</div>
		<div class="row">
			<label>State</label>
			<input name="state" maxlength="50" type="text" class="form-control input-sm"
				   value="<?= $o->Record->state ?>"/>
		</div>
		<div class="row">
			<label>Country</label>
			<select name="countrycode" id='countrycode' class="form-control input-sm required">
				<option value="">---</option>
				<?php
				$geoIPCountryName = $site->country->name;
				$oGen->getCountries();
				while( $oGen->nextRecord() ) {
					$_code = strtolower($oGen->Record->code);
					$selected = ($o->Record->country==$oGen->Record->name || $o->Record->countrycode==$_code) ? 'selected="selected"' : '';
					?>
					<option value="<?= $_code ?>" <?= $selected ?>><?= $oGen->Record->name ?></option>
				<?php } ?>
			</select>
		</div>
<?php if( $o->Record->unsubscribed>0 ) { ?>
		<div class="row" style='font-size:13px'>
			<label>Unsubscribed</label>
			<input name="unsubscribed" value='1' <?= $o->Record->unsubscribed>0 ? 'checked':'' ?> type="checkbox" class="form-control input-sm" style='display:inline-block; float:-left;  margin-top:-2px; width:24px'/>
				 - Promotional Emails <?= $o->Record->unsubscribed>0 ? 'disabled':'enabled' ?>
		</div>
<?php } ?>
		<div class="row">
			<div class="center">
				<button id="btnAccountUpdate" class="blue-btn padding10 nomargin pull-left" type="submit">
					Update &nbsp;<i class="fa fa-arrow-circle-right"></i>
				</button>
				<button id="btnAccountUpdate" class="blue-btn padding10 nomargin pull-right link"
					type="button" data-link="<?= BASE_URLm ?>account/security"
					style='width:190px'>
					Change Password &nbsp;<i class="fa fa-arrow-circle-right"></i>
				</button>

			</div>
		</div>
	</form>
	<div style="clear:both"></div>
	<br>

	<?php // tickets Pending
	UI::packages_tables( 'pending', 'Confirm your Packages' );
	UI::tickets_tables( 'pending', 'Pending Tickets', 'mobile' );
	?>

</section>
