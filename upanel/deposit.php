<?php
	global $PACKAGE_DISCOUNT;
	$DURATION = isset($_COOKIE['DURATION']) ? (int)$_COOKIE['DURATION'] : 0;

	include(BASE_PATH.'inc/deposit.php'); // settings part

	// having tickets in a cart?
	$oLottery->areNumbersPicked(session_id());
	$areNumbersPicked = $oLottery->numRows(); // 0: cart is empty
	if( $areNumbersPicked && !isset($_SESSION['CONFIRMING_AMOUNT']) ) {
		$bet = new Bet;
		$bet->checkBalance( false, $DURATION );
	}

	$customAmount = 0;
	if( isset($_SESSION['CONFIRMING_AMOUNT']) ) { // preset amount for Membership Package
		//$customAmount = ceil($_SESSION['CONFIRMING_AMOUNT'] - $_SESSION['logged_cur_balance']);
		$customAmount = round( 1.05 * ($_SESSION['CONFIRMING_AMOUNT'] - $_SESSION['logged_cur_balance']), 2 ); // +5%
		$customAmount = max( 0, $customAmount );
		$_max = max( 1+$customAmount, $_max );
		unset($_SESSION['CONFIRMING_AMOUNT']);
	}

	unset($_SESSION['FIRST_DEPOSIT']);
	if( $oUser->depositStats( $oUser->getID())->numRows() ) {
		$oUser->nextRecord();
		if( $oUser->Record->count>1 && $oUser->Record->total>=5 ) { // 2+ payment with total amount > $5
			$fixedAmounts = [ 20, 50 ]; // set "20, 50, custom"
		} else if( $oUser->Record->count && $oUser->Record->max>=3 ) { // 1+ payment and max amount > $3
			$fixedAmounts = [ 10, 20 ]; // set "10, 20, custom"
		}
	} else $_SESSION['FIRST_DEPOSIT'] = true;
?>
<style>
.lessScroll { display: none !important; }
footer { display: none; }
</style>
<section>
	<?php

	// !empty($_SESSION['NO_ENOUGH_MONEY']): tried to confirm tickets but failed
	// unset($_SESSION['NO_ENOUGH_MONEY']);

	// show a message box
	$_msg = '';
	if( $oUser->getDiscount() ) $_msg = GETDISC_OGA;
	if( $areNumbersPicked ) { // cart is not empty
		if( $DURATION ) $_msg = GETDISC_SUBSCRIBE;
		elseif( $oUser->hasActivePackage() ) $_msg = GETDISC_YOUAREVIP;
	}
	echo $_msg;

	?>
	<div class="row" style='margin:10px 0'>
		<div class="blue pull-left bold" data-title='<?= $code.', '.$site->country->currencyCode ?>'>Deposit</div>
		<div class="blue pull-right bold current-balance" style='margin-top:0'>Current Balance: <b class="symbol" data-symbol="$"></b><b class='balValue'><?= $oUser->getCurrentBalance() ?></b></div>
	</div>
	<div style="clear: both"></div>

	<div class="row Deposit">
		<div class="blue-background">1. Choose Amount to Deposit:</div>
		<div class='amountBox light-blue-background' style='visibility:hidden'>
			<div class='amountOption' data-amount='<?= $fixedAmounts[0] ?>'><span class="symbol" data-symbol="$"></span><?= $fixedAmounts[0] ?></div>
			<div class='amountOption' data-amount='<?= $fixedAmounts[1] ?>'><span class="symbol" data-symbol="$"></span><?= $fixedAmounts[1] ?></div>
			<div class='amountOption' data-amount='<?= $_min ?>'>
				<span data-default='Enter an amount'>Enter an amount</span>
				<input name='amount' type='number' value='<?= $customAmount ?>'
					min='<?= $_min ?>' max='<?= $_max ?>' maxlength='<?= $_len+1 ?>' autocomplete="off"
					onkeypress='return event.charCode==0 || (event.charCode>=46 && event.charCode<=57)'
					onfocus='this.select();if(0&&INPROGRESS)this.blur()'
					oninput='if(this.value.length><?= $_len ?>)this.value=this.value.slice(0,<?= $_len ?>);' />
				&nbsp;
			</div>
		</div>

		<div class="blue-background methodOpen">
			<div class='methodSelected'><span class="cvv" style='right:27px;top:6px;background:#777'>?</span></div>
			2.<span class='hide-le480'> Choose</span> Payment Method:
		</div>
		<div class="method light-blue-background">
			<div class='methodBox' style='display:none'>
			<?php
			foreach( $Methods as $m ) {
				$i = $m[0];
				$Includes[$i] = $setIncludes[$i];
				echo '<div class="methodOption" data-method="'.$i.'"><img src="'.$m[1].'" alt="'.$m[0].'"/></div>';
			}
			?>
			</div>

		</div>

	</div>

	<div id="paymentFields" class='row light-blue-background'>
		<?php
		foreach( $Includes as $i ) include( $i ); // include FORMS here
		?>
	</div>

	<div style="clear: both"></div>
	<div id="yourLotteryPicksTableContainer">
	<?php
		$_n = 3;
		UI::packages_tables( 'pending', $_n.'. Confirm your Packages' );
		if( $oUser->getID() && (new Lottery)->getLotteryPackages($oUser->getID(),'pending') ) $_n++;
		$oLottery = new Lottery;
		$oLottery->areNumbersPicked( session_id() );
		if( $oLottery->numRows() ) {
			echo '<style>#checkout{display:none !important}</style>';
			UI::tickets_tables( 'pending', $_n.'. Confirm your Tickets', 'mobile' );
		}
	 ?>
	</div>

	<div id="myModal" class="modal fade">
		<div class="modal-backdrop in"></div>
		<div class="modal-dialog" style="padding-top:150px !important">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">VRVE Card Verification</h4>
				</div>
				<div class="modal-body">
					<p tyle="margin-bottom:10px">
						A verification code will be sent to you as a text message to protect your account from unauthorized access, the code will expire soon.
					</p>
				</div>
			</div>
		</div>
	</div>

</section>
<script src="https://bitpay.com/bitpay.min.js"></script>
<script type="text/javascript">
    bitpay.onModalWillLeave( function () {
        window.bitpayIframe.hideBitpayDialog();
    });

    function showBitpayDialog( bitpayInvoiceId ) {
        bitpay.enableTestMode(true);
        bitpay.showInvoice( bitpayInvoiceId );
    }

var AMOUNT=0, INPROGRESS=false;
function VerveWarning() { // show modal #myModal
	$("#myModal").fadeIn();
	setTimeout('$("#myModal").fadeOut()',10000);
}

// fixup iframe height;
//	obj	- iframe object
//	n	- object name 'n'=[init,max] - sizes for particular iframe
function resetIframeHeight(obj) {
	var _h_s=$(obj).data('hini'), _h_m=$(obj).data('hmax'), _h=_h_s;
	if(_h_s) $(obj).data('hini',0); // first run only
	else {
		obj.height=1;
		if(!$.browser.mozilla) {
			try { _h=obj.contentWindow.document.body.scrollHeight; }
			catch(e) { _h=_h_m; }
		} else { _h=_h_s>0 ? _h_s:_h_m; }
	}
	obj.height=_h;
}
function balReload() {
	$.ajax({
		url: '/ajax/index.php',
		type: "POST",
		cache: false,
        data: { action: 'get_balance' }
	}).done(function(data) { $('.balValue').html(data) });
}

$(document).ready(function(){
	$('.current-balance').click(function(){balReload()});
	$('.modal-backdrop').click(function(){ $('#myModal').fadeOut(); })
	$('#myModal .close').click(function(){ $('#myModal').fadeOut(); })
	// amount block
	$('.amountBox .amountOption').click(function(){ // serve amount click
		// if( INPROGRESS ) return false; // disabled
		$('.amountBox .amountOption').removeClass('active');
		$(this).addClass('active');
		AMOUNT=parseFloat($(this).data('amount'));
		//console.log("AMOUNT="+AMOUNT);
	});
	$('.amountBox .amountOption input').blur(function(){ // other amount type done
		// if( INPROGRESS ) return false; // disabled
		var o=$(this)[0], _mlen=o.maxLength, _min=parseInt(o.min), _max=parseInt(o.max), _v=parseFloat(o.value), _msg=$(this).prev();
		if(isNaN(_v)||_v<=0) { // empty field
			_msg.html(_msg.data('default'));
			o.value=o.defaultValue;
			_msg.removeClass('entered');
			$(this).parent().data('amount',_min);
			$('.amountBox .amountOption').first().trigger('click'); // reset to 1st
			return;
		} else if( _v<_min || _v>_max) { // not in range
			alert('Sorry, Amount must be between '+_min+' and '+_max);
			_v = _v<_min ? _min:_max;
			o.value = _v;
		}
		AMOUNT=_v; // store selected amount
		_msg.html('$'+AMOUNT); // show instead of "other"
		$(this).parent().data('amount',AMOUNT);
		_msg.addClass('entered');
		//console.log("AMOUNT="+AMOUNT);
	});
	$('.amountBox .amountOption').eq(1).trigger('click'); // init amount block #2
	setTimeout(function(){ $('.amountBox').css('visibility','visible'); }, 800);

<?php if($customAmount) { ?>
	$('.amountBox .amountOption').eq(2).trigger('click'); // init amount block #3
	$('.amountBox .amountOption input').eq(0).trigger('blur'); // init amount block #3
	<?php if( $DURATION ) { ?>
	$('.amountBox .amountOption').eq(0).hide();
	$('.amountBox .amountOption').eq(1).hide();
	<?php } ?>
<?php } else { ?>
	$('.amountBox .amountOption').eq(1).trigger('click'); // init amount block #2
<?php } ?>

	// method block
	var METHOD='';
	$('.Deposit .methodOpen').click(function(){ // open/close method select box
		// if( INPROGRESS ) return false; // disabled
		$('.methodBox').fadeToggle();
	});
	$('.methodBox .methodOption').click(function(){ // serve method click
		$('.msg.warning').hide(); // hide all forms
		METHOD=$(this).data('method'); // store selected option name
		//console.log("METHOD="+METHOD);
		$('#paymentFields div').hide(); // hide all forms
		$('#'+$(this).data('method')).show(); // show selected form
		$('.methodSelected').html($(this).html()); // copy selected to box
		$('.methodBox .methodOption').removeClass('active'); // unhighlight options
		$(this).addClass('active'); // highlight selected
		$('.methodBox').fadeOut(); // close dropdown
	});
	/* NO initial method set, 2017-04-28
	$('.methodBox .methodOption').first().trigger('click'); // init method block */
	// init methods
	setTimeout(function(){ 
		$('.methodBox').fadeIn();
		$('#loading-overlay').remove();
	}, 1000);
	// iframes lazy load
    $('iframe').each(function(){
    	if($(this).data('onload')) $(this).load( Function($(this).data('onload')) );
    	if($(this).data('src')) $(this).attr('src',$(this).data('src'));
    });
});
<?php if( GAENABLED ) { ?>
	dataLayer.push({
		'pageType': 'Deposit',
		'auth': '<?= $oUser->loginCheck() ? 1:0 ?>',
		'userId': '<?= $oUser->getID() ?>',
		'depositTotal': '<?= $oUser->getCurrentBalance() ?>'
	});
	<?php if( @$_SESSION['CONFIRMATION_FAILED'] ) { ?>
	dataLayer.push({
		'event': 'lottos',
		'eventCategory': 'numbers',
		'eventAction': 'purchase',
		'eventLabel': 'fail'
	});
	<?php
    unset($_SESSION['CONFIRMATION_FAILED']);
    }
} ?>
</script>
