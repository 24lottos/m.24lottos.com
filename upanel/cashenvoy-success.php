<?php
$message = 'No response';
$class = 'error';
$status  = false;
$FIRST_DEPOSIT = $status = false;
if( isset($_POST['ce_transref']) ):
    // check is it 1st deposit
    $FIRST_DEPOSIT = $oUser->isFirstDeposit( $oUser->getID(), 'Cashenvoy mobile' );
    // verify Cashenvoy
    $resp = ( new Cashenvoy )->doDepositCashEnvoy( $_POST, new MobileDetect, getBrowser() );
    $status = $resp['status'];
    $response_message = $resp['message'];
endif;

if( $status == 'Paid' ) { /* success */
    $message = 'Congratulations! Payment completed successfully.<br/>Thank you.';
    $class = 'success';

    if( GAENABLED ) { ?>
        <script>
            dataLayer.push({
                "event": "transaction",
                "eventCategory": "deposit",
                "eventAction": "purchase<?= $FIRST_DEPOSIT ? ' first time':'' ?>",
//			"eventValue": "<?//= @$_SESSION['cashenvoy_amount'] ?>//",
                "eventLabel": "success, CashEnvoy, mobile",
                "ecommerce": {
                    "purchase": {
                        "actionField": {
                            "id": "<?= $_SESSION['cashenvoy_transaction'] ?>",
                            "affiliation": "site",
                            //"revenue": "<?= $_SESSION['cashenvoy_amount'] ?>",
                            "coupon": ''
                        },
                        "products": [{
                            "name": "Deposit",
                            "category": "Deposit",
                            "price": "<?= $_SESSION['cashenvoy_amount'] ?>",
                            "quantity": 1
                        }]
                    }
                }
            });
        </script>
    <?php }
} else { /* FAIL */
    $message = 'Sorry! Server responded: ' .$response_message .'.';
    ?>
    <script>
        dataLayer.push({
            "event": "transaction",
            "eventCategory": "deposit",
            "eventAction": "purchase<?= $FIRST_DEPOSIT ? ' first time':'' ?>",
            "eventValue": "<?= @$_SESSION['cashenvoy_amount'] ?>",
            "eventLabel": "error - { <?= $status ?> }"
        });
    </script>
<?php } ?>
<br>
<div class='<?= $class ?>-msg'><?= $message ?></div>
<?php if( $status == 'Paid' ): ?>
    <script> parent.balReload() </script>
    <p><?= CLICK2CONTINUE ?></p>
    <button class="green-btn padding10" style="width:auto;margin-top:10px" onclick="this.disabled=true;parent.location.href='/vcheckout'">
        Continue &nbsp;<i class="fa fa-arrow-circle-right"></i>
    </button>
<?php else:
    include(BASE_PATH.'inc/deposit-country.php');
endif;
