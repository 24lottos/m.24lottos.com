<section>
	<div class="banner">
		<?php include('inc/timeline.php') ?>
	</div>
	<div style="clear: both;"></div>

	<?php if( isset($_POST['btnRedeemBonus']) ):
		@$oUser->updateBalanceFromBonus( $_POST, $oUser->getID(), $_SESSION['logged_pref_currency'], new General );
		echo '<div class="alert alert-success" style="width:99%; margin:auto;">
		<button data-dismiss="alert" class="close"></button>
		<strong>Success!</strong> Your Bonus points are added to your wallet. <a href="'.$site->baseURL().'">Play Now</a></div>';
	endif; ?>

	<div class="row">
		<div class="col-lg-3">
			<h5><strong>Redeem Bonus Points</strong></h5>
		</div>
		<div class="col-lg-9">
			<ul style="margin: 12px -19px 0px 0px;" class="list-inline centered pull-right">
				<li class="draw">Current Balance: <?= $oUser->getCurrentBalance() ?></li>
				<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
				<li class="draw">Bonus Balance: <?= $oUser->getBonusBalance() ?></li>
				<li>&nbsp;&nbsp;</li>
			</ul>
		</div>
	</div><!-- row -->

	<div class="table-responsive ">
		<table class="table table-striped payment-method">
			<thead>
			<tr>
				<th class="centered tbl-heading">&nbsp;</th>
			</tr>
			</thead>
			<tbody style="font-weight: 500;color: #333;">
			<tr>
				<td>
					<?php $a = $oGen->getBonusMinClaim( $userROW->preferred_currency ) ?>
					<div style="text-align:justify;"><p align="center">Claim your bonus points now! <br/>You must have
							minimum
							<?= $a['MinBonusForClaim'].' Bonus points to claim '.$userROW->preferred_currency.' '.$a['MinBonusMappingWithMoney'] ?>
							additional bonus amount in your wallet.<br/>
							Please be noted that bonus points are for playing ticket.</p></div>
				</td>
			</tr>
			<tr>
				<td><br/>
					<form method="post">
						<input type="hidden" name="minRedeem" id="minRedeem" value="<?= $a['MinBonusForClaim'] ?>"/>
						<input type="hidden" name="minRedeemMapped" id="minRedeemMapped"
							   value="<?= $a['MinBonusMappingWithMoney'] ?>"/>
						<input type="hidden" name="availBonus" id="availBonus" value="<?= $userROW->bonuswallet ?>"/>
						<input type="hidden" name="redeemName" value="<?= $userROW->name ?>"/>
						<input type="hidden" name="redeemRequest" value="<?= $userROW->username ?>"/>
						<input type="hidden" name="redeemEmail" value="<?= $userROW->email ?>"/>
						<input type="hidden" name="redeemCurrency" value="<?= $userROW->preferred_currency ?>"/> <input
							type="submit" name="btnRedeemBonus" id="btnRedeemBonus" class="btn btn-default btn-redeem"
							value="<?= $a['MinBonusForClaim'] <= $userROW->bonuswallet ? "Claim Bonus":"We're Sorry but you don't have enough Bonus Balance to Redeem You Bonus Points" ?>"
							style="width:auto;"/>
					</form>
					<br/></td>
			</tr>
			</tbody>
		</table>

	</div>

</section>
