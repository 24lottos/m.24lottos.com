<div class="row">
    <div class="col-lg-12">
        <div class="success-msg" style="margin: 0;">Your payment will be completed during 1 hour, depends of your payment preferences.
            We will send you an email message about this.</div>
        <button class="green-btn padding10" style="width:auto;" onclick="this.disabled=true;parent.location.href='/vcheckout'">
            Continue &nbsp;<i class="fa fa-arrow-circle-right"></i>
        </button>
    </div>
</div>