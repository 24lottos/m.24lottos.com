<?php
/**
* paystack-success.php - Payment validation
*
* Params:
*
* @category   PayStack payment gateway
* @author     Sergey V Musenko <info@ws123.net>
* @copyright  2017 (c) Web Solutions 123. All rights reserved.
* @date       2017-07-17
* @version    0.1
*/

$reference = isset($_GET['reference']) ? $_GET['reference'] : false;

$success = false;

if( $reference ) {
	//Load composer autoload
	require_once( dirname(__DIR__).'/vendor/autoload.php' );
	// initiate the Library's Paystack Object
	$paystack = new Yabacon\Paystack( PAYSTACK_SECKEY );
	try { // verify using the library
		$tranx = $paystack->transaction->verify([
			'reference' => $reference, // unique to transactions
		]);
		//echo "<hr>tranx:";vardump($tranx);

		if( 'success'===$tranx->data->status ) { // transaction validated
			$id       = (int)$tranx->data->id;
			$ref      = $reference;
			$amount   = (int)$tranx->data->amount;
			$currency = $tranx->data->currency;
			$deptype  = $tranx->data->channel=='bank' ? 'Paystack-bank':'Paystack';
			$userid   = $oUser->getID();

			// update deposit in db
			$oUser->query( "LOCK TABLES lotto_transactions WRITE" );
			$oUser->query( "
				SELECT response, status
				  FROM lotto_transactions
				 WHERE TrackingNumber='$reference' and amount=$amount" );
			$record = $oUser->singleRecord()->Record;

			// record not found! no Ref or different amount
			if( !$record ) {
				$oUser->query( "UNLOCK TABLES" );
				@testWithLog( "--- Paystack Error --- UserID=$userid, lotto_transactions not found: $reference, $amount" );

			// transaction found and in_progress - we have to update DB!
			} else if( $record->status=='in_progress' ) {
				$amountUSD = (float)$record->response; // in USD, NGN 100*amount is in `amount`
				// update lotto_transactions
				$oUser->query( "
					update lotto_transactions
					   set status='success', response_time='".time()."',
						   response='".serialize( $tranx->data )."'
					 where TrackingNumber='$reference'" );
				$oUser->query( "UNLOCK TABLES" );
				// create lotto_deposit record
				$br = getBrowser();
				$detect = new MobileDetect;
				$data = array(
					'userid'      => $userid,
					'amount'      => $amountUSD,
					'status'      => 'Paid',
					'deviceip'    => $_SERVER['REMOTE_ADDR'],
					'paiddate'    => gmdate('Y-m-d H:i:s'),
					'byadmin'     => 'No',
					'deposittype' => $deptype,
					'payment_id'  => $ref,
					'platform'    => $br['platform'],
					'browser'     => $br['name'],
					'version'     => $br['version'],
					'device'      => ($detect->isMobile() ? ($detect->isTablet() ? 'tablet':'mobile'):'desktop'),
					'payment_batch_num' => ($amount/100)." $currency",
				);
				$oUser->query( "
					INSERT INTO lotto_deposit (".implode( ',', array_keys( $data ) )." ) 
					VALUES ( '".implode( "' , '", $data )."')" );
				// add user balance
				if( $oUser->insertID() ) { // deposit record added
					$oUser->updateBalance( $amountUSD, 'C', $userid, $deptype.' ID:'.$ref );
					$oUser->clearUnfinishedPay( $userid ); // payment done so clear "unfinishedPay"
					// send confirmation email
					$data = new stdClass;
					$data->fundAmount = "$".number_format( $amountUSD, 2 );
					$data->toEmail    = $oUser->getEmail();
					$data->fullName   = $oUser->getName();
					$data->toName     = $oUser->getName();
					(new Email)->fundsDepositedByUser( $data );
				}

			// probably already verified; no need DB update or email
			} else {
				$oUser->query( "UNLOCK TABLES" );
			}

			$_SESSION['LASTPAYTRIED'] = false;
			$success = true;
		}

	} catch(\Yabacon\Paystack\Exception\ApiException $e){
		@testWithLog( "--- Paystack Error --- UserID=$user_id, CALLBACK, ".$e->getMessage() );
	}
}

?>
<div class="row">
	<div class="col-lg-12">
		<br/>
<?php
if( $success ) { // Success page
	echo '
<div class="success-msg">Payment completed successfully</div><br/>
<script> parent.balReload() </script>
<p>'.CLICK2CONTINUE.'</p>
<button class="green-btn padding10" style="width:auto;margin-top:10px" onclick="this.disabled=true;parent.location.href=\'/vcheckout\'">
	Continue &nbsp;<i class="fa fa-arrow-circle-right"></i>
</button>';

} else { // Failure page
	echo '<div class="msg danger margin" style="padding: 10px"><div class="fa fa-warning"></div>&nbsp; Payment Failed!</div>';
	include(BASE_PATH.'inc/deposit-country.php');
}
?>
	</div>
</div>
