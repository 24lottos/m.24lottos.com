<div class="row">
	<div class="col-lg-8">
		<div class="col-lg-12">
			<?php include('inc/timeline.php') ?>
		</div>
		<div class="col-lg-9">
			<ul class="list-inline pull-left upnlmnu" style="margin-left:4px;">
				<li class="link" data-link="<?= $site->baseURL( "account/dashboard" ) ?>">Dashboard</li>
				<li class="link" data-link="<?= $site->baseURL( "account/account" ) ?>">Account</li>
				<li class="link" data-link="<?= $site->baseURL( "account/tickets" ) ?>">My Tickets</li>
				<li class="link" data-link="<?= $site->baseURL( "account/wallet" ) ?>">Wallet</li>
				<li class="link" data-link="<?= $site->baseURL( "account/redeem" ) ?>">Bonus</li>
			</ul>
		</div>
		<div class="col-lg-3">
			<ul class="list-inline pull-right upnlmnu">
				<li class="green link" data-link="<?= $site->baseURL( "account/deposit" ) ?>">Quick Deposit</li>
			</ul>
		</div>

		<div style="clear: both;"></div>

		<div class="row">
			<div class="col-lg-3">
				<h5><strong>Deposit</strong></h5>
			</div>
			<div class="col-lg-9">
				<ul style="margin: 12px -19px 0px 0px;" class="list-inline centered pull-right">
					<li class="draw">Current Balance: <?= $oUser->getCurrentBalance() ?></li>
					<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
					<li class="draw">Bonus Balance: <?= $oUser->getBonusBalance() ?></li>
					<li>&nbsp;&nbsp;</li>
				</ul>
			</div>
		</div><!-- row -->

		<?php if( array_key_exists( 'PAYMENT_BATCH_NUM', $_POST ) && $_POST['PAYMENT_BATCH_NUM'] != 0 ): ?>
			<div class="table-responsive ">
				<script> parent.balReload() </script>
				<table class="table table-striped payment-method">
					<thead>
					<tr>
						<th class="centered tbl-heading" colspan="4">&nbsp;</th>
					</tr>
					</thead>
					<tbody style="font-weight: 500;color: #333;">
					<tr>
						<td colspan="4" class="centered"><strong>Congratulations! Your Payment Deposit is Successful.
								<br/>Thank you</strong></td>
					</tr>

					<tr>
						<td colspan="4">
							<div id="paymentDesc"><br/>
								<a href="<?= $site->baseURL() ?>" style="color:#019245">Play Now!</a> And make more and
								more money!<br/><br/>
								We wish you all the very best.<br/><br/></div>
						</td>
					</tr>

					</tbody>
				</table>
			</div>
		<?php endif; ?>

	</div>

</div>

<?php
$oPixel->getTrackingCode( $p );
while( $oPixel->nextRecord() ):
	echo html_entity_decode( $oPixel->Record->shortcode );
endwhile;
