<section>
	<div class="banner">
		<?php include('inc/timeline.php') ?>
	</div>

	<div style='position:relative'><div class='editAccount'>
		<a href="<?= BASE_URLm ?>account/account">Edit Account</a>
	</div></div>

	<?= isset($_SESSION['TICKET_HAS_BEEN_BOUGHT']) && $_SESSION['TICKET_HAS_BEEN_BOUGHT'] > 0 ? str_replace( '{count}', $_SESSION['TICKET_HAS_BEEN_BOUGHT'], TICKET_HAS_BEEN_BOUGHT ):'' ?>
	<?php
	if( isset($_SESSION['TICKET_HAS_NOT_BEEN_BOUGHT']) && $_SESSION['TICKET_HAS_NOT_BEEN_BOUGHT'] > 0 ) {
		$ERROR_PROCESSING_TICKET = ERROR_PROCESSING_TICKET;
		echo $ERROR_PROCESSING_TICKET = str_replace( '{TICKET_HAS_NOT_BEEN_BOUGHT_ERROR}', $_SESSION['TICKET_HAS_NOT_BEEN_BOUGHT_ERROR'], $ERROR_PROCESSING_TICKET );
		unset($_SESSION['TICKET_HAS_NOT_BEEN_BOUGHT_ERROR']);
		unset($_SESSION['TICKET_HAS_NOT_BEEN_BOUGHT']);
	};
	unset($_SESSION['TICKET_HAS_BEEN_BOUGHT']);
	?>
	<div class="dashbord">
		<?php
		$dashboard = $oAccount->dashboard( $logged_id );
		foreach( $dashboard as $k => $v ):
			echo '<div class="row">';
			echo '<span class="left-section">'.$v[0].'</span>';
			echo '<span class="right-section">'.$v[1].'</span>';
			echo '</div>';
		endforeach;
		?>
	</div>
	<div style="clear:both"></div>

	<?php // tickets Pending and Active
	UI::packages_tables( 'pending', 'Confirm your Packages' );

	$oLottery = new Lottery;
	$oLottery->areNumbersPicked( session_id() );
	if( $oLottery->numRows() ) {
		UI::tickets_tables( 'pending', 'Pending Tickets', 'mobile' );
	}
	$oLottery->areNumbersActive( $oUser->getID() );
	if( $oLottery->numRows() ) {
		UI::tickets_tables( 'active', 'Active Tickets', 'mobile' );
	} ?>


</section>