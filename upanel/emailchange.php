<section>
	<div class="banner">
		<?php include('inc/timeline.php') ?>
	</div>

	<form class="account-form " method="POST">
		<div class="row">
			<label>Current Email</label>
			<input type="email" class="form-control input-sm"/>
		</div>
		<div class="row">
			<label>New Email</label>
			<input type="email" class="form-control input-sm"/>
		</div>
		<div class="row">
			<label>Confirm Email</label>
			<input type="email" class="form-control input-sm"/>
		</div>
		<div class="row">
			<div class="center">
				<button class="blue-btn padding5" type="submit">Confirm & Save</button>
			</div>
		</div>
	</form>
</section>
