<style>#frmChangePass label.error {
		display: none !important;
	}</style>
<section>
	<?php if( isset($_POST['confirmChangePass']) ):echo $oUser->changeUserPassword( false );
		$_SESSION['pass_change'] = true;endif; ?>
	<form class="account-form" id="frmChangePass" method="POST">
		<input type="hidden" name="opSave" value="<?= $_SESSION['logged_pass'] ?>"/>
		<div class="row">
			<label>Current Password</label>
			<input type="password" class="form-control input-sm" name="op" id="op"/>
		</div>
		<div class="row">
			<label>New Password</label>
			<input type="password" class="form-control input-sm" name="np" id="np"/>
		</div>
		<div class="row">
			<label>Confirm Password</label>
			<input type="password" class="form-control input-sm" name="cp" id="cp"/>
		</div>
		<div class="row">
			<div class="center">
				<input name="confirmChangePass" value="dummy" type="hidden"/>
				<button type="submit" class="blue-btn" id="confirmChangePass" style='width:242px;border:none;height:38px;line-height:38px;padding:0'>
					Submit &nbsp;<i class="fa fa-arrow-circle-right"></i>
				</button>
			</div>
		</div>
	</form>
</section>
