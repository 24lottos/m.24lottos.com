<section>

	<div class="row">
		<div class="col-lg-3">
			<h5><strong>Deposit</strong></h5>
		</div>
		<div class="col-lg-9">
			<ul class="list-inline centered pull-right">
				<li class="draw">Current Balance: <?= $oUser->getCurrentBalance() ?></li>
			</ul>
		</div>
	</div>

	<div class="table-responsive ">
		<table class="table table-striped payment-method">
			<tbody style="font-weight: 500;color: #333;">
			<tr>
				<td colspan="4" class="centered"><strong>Error! Your Deposit Transaction is Failed.</strong></td>
			</tr>
			</tbody>
		</table>
	</div>
	<?php include(BASE_PATH.'inc/deposit-country.php'); ?>
</section>
