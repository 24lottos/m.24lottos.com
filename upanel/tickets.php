<style> .lessScroll { display: none !important; } </style>
<section>
	<div class="banner">
		<?php include('inc/timeline.php') ?>
	</div>
	<?php
	// show a message after doConfirm ---\\
	$_tic_total  = @$_SESSION['TICKET_HAS_TOTAL'];
	if( $_tic_total ) {
		$_msg = '';
		$_dur_bought  = trim(@$_SESSION['TICKET_DURATION']);
		$_dur_failed  = trim(@$_SESSION['TICKET_DURATION_ERROR']);
		$_tic_bought  = @$_SESSION['TICKET_HAS_BEEN_BOUGHT'];
		$_tic_failed  = @$_SESSION['TICKET_HAS_NOT_BEEN_BOUGHT'];
		$_tic_failres = @$_SESSION['TICKET_HAS_NOT_BEEN_BOUGHT_ERROR'];
		unset(
			$_SESSION['TICKET_HAS_TOTAL'],
			$_SESSION['TICKET_HAS_BEEN_BOUGHT'],
			$_SESSION['TICKET_HAS_NOT_BEEN_BOUGHT'],
			$_SESSION['TICKET_HAS_NOT_BEEN_BOUGHT_ERROR'],
			$_SESSION['TICKET_DURATION'],
			$_SESSION['TICKET_DURATION_ERROR']
		);

		if( $_tic_bought>0 ) { // on success
			$_msg.= '<div class="success-msg msg" style="margin-bottom:10px" onclick="$(this).fadeOut()"><i class="fa fa-check-circle"></i>
                Your ('.$_tic_bought.') ticket'.($_tic_bought>1 ? 's':'').' has been ordered. Good luck!</div>';
		}
		if( $_tic_failed>0 ) { // on error
			$_msg.= '<div class="error-msg msg" style="margin-bottom:10px" onclick="$(this).fadeOut()"><i class="fa fa-warning"></i>
				Sorry, ('.$_tic_failed.') ticket'.($_tic_bought>1 ? 's':'').' has NOT been ordered. Please try later or contact our Support.</div>';

		}

		if( !empty($_dur_bought) ) { // DURATION on success
			$_msg.= '<div class="success-msg msg" style="margin-bottom:10px" onclick="$(this).fadeOut()">
				<i class="fa fa-check-circle"></i>
				'.$_dur_bought.'</div>';
		}
		if( !empty($_dur_failed) ) { // DURATION on error
			$_msg.= '<div class="error-msg msg" style="margin-bottom:10px" onclick="$(this).fadeOut()">
				<i class="fa fa-warning"></i>
				'.$_dur_failed.'</div>';
		}

		echo $_msg;
	}
	// show a message after doConfirm ---//

	UI::packages_tables( 'pending', 'Confirm your Packages' );

	$oLottery = new Lottery;
	$shown = 0;

	// tickets Pending / Active / Previous
	$oLottery->areNumbersPicked( session_id() );
	if( $oLottery->numRows() ) {
		UI::tickets_tables( 'pending', 'Pending Tickets', 'mobile' );
		$shown++;
	}

	UI::packages_tables( 'active', 'Active VIP membership' );

	$oLottery->areNumbersActive( $oUser->getID() );
	if( $oLottery->numRows() ) {
		UI::tickets_tables( 'active', 'Active Tickets', 'mobile' );
		$shown++;
	}

	$oLottery->areNumbersPrevious( $oUser->getID() );
	if( $oLottery->numRows() ) {
		UI::tickets_tables( 'previous', 'Previous Tickets', 'mobile' );
		$shown++;
	}

	if( !$shown ) {
		echo '<h4>'.NO_TICKETS.'</h4>';
	}
	?>

</section>

<?php if ( isset($_SESSION['gtm_tickets_payment_push']) ) {

    echo "<script>";
    echo $_SESSION['gtm_tickets_payment_push'];
    echo "</script>";

    unset($_SESSION['gtm_tickets_payment_push']);
}
