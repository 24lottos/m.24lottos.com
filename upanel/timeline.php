<section>
	<div class="banner">
		<?php include('inc/timeline.php') ?>
	</div>

	<?php if( isset($_POST['btnUpdateTimeline']) ):
		$timelinephoto = uploadTheFile( $_FILES['fTimelinePic'], rand(), $site->basePath( 'images/' ) );
		$oUser->update( array( 'timelinephoto' => $timelinephoto ), $oUser->getID() );
		$oUser->setTimelinePic( $timelinephoto );
		header( 'Refresh:0;url='.$site->baseURLm( 'account/profile' ) );
	endif; ?>
	<form class="account-form" id="frmUpdateProfile" method="POST" enctype="multipart/form-data">
		<div class="row">
			<label>New Timeline Image</label>
			<input name="fTimelinePic" type="file" class="form-control required" style="padding:0"/>
			<input name="btnUpdateTimeline" type="hidden"/>
		</div>
		<div class="row">
			<div class="center">
				<button class="blue-btn padding5" id="btnUpdateTimeline" type="submit">Submit</button>
			</div>
		</div>
	</form>
</section>
