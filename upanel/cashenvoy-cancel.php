<section>

	<div class="row">
		<div class="col-lg-3">
			<h5><strong>Deposit</strong></h5>
		</div>
		<div class="col-lg-9 pull-right">
			<ul class="list-inline centered padding10">
				<li class="draw">Current Balance: <?= $oUser->getCurrentBalance() ?></li>
			</ul>
		</div>
	</div>

	<div class="table-responsive ">
		<table class="table table-striped payment-method">
			<tbody style="font-weight: 500;color: #333;">
			<tr>
				<td colspan="4" class="centered"><strong>Sorry to know that you have cancelled the transaction.</strong>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
	<?php include(BASE_PATH.'inc/deposit-country.php'); ?>
</section>
