<section>
	<div class="banner">
		<?php include('inc/timeline.php') ?>
	</div>

	<?php if( isset($_POST['btnUpdateProfile']) ):
		$profilephoto = $site->baseURL( 'images/' ).uploadTheFile( $_FILES['fProfilePic'], rand(), $site->basePath( 'images/' ) );
		$oUser->update( array( 'profilephoto' => $profilephoto ), $oUser->getID() );
		$oUser->setProfilePic( $profilephoto );
		header( 'Refresh:0;url='.$site->baseURLm( 'account/profile' ) );
	endif; ?>

	<form class="account-form" id="frmUpdateProfile" method="POST" enctype="multipart/form-data">
		<div class="row">
			<label>New Profile Image</label>
			<input name="fProfilePic" type="file" class="form-control required" style="padding:0"/>
			<input name="btnUpdateProfile" type="hidden"/>
		</div>
		<div class="row">
			<div class="center">
				<button class="blue-btn padding5" id="btnUpdateProfile" type="submit">Submit</button>
			</div>
		</div>
	</form>
</section>
