<?php
/**
* rave-success.php - Payment validation
*
* Params:
*	user		- User::getID()
*	resp		- response chargeResponseCode
*	ref			- flwRef
*	amount		- chagred amount
*	currency	- charged currency
*	amountUSD	- chagred amount in USD
*
* @category   RavePay payment gateway
* @author     Sergey V Musenko <info@ws123.net>
* @copyright  2017 (c) Web Solutions 123. All rights reserved.
* @date       2017-07-17
* @version    0.1
*/

$userid   = isset($_GET['user'])     ? $_GET['user'] : 0;
$resp     = isset($_GET['resp'])     ? $_GET['resp'] : 'RR';
$ref      = isset($_GET['ref'])      ? $_GET['ref'] : '0';
$amount   = isset($_GET['amount'])   ? $_GET['amount'] : 0;
$currency = isset($_GET['currency']) ? $_GET['currency'] : '';
$amountUSD= isset($_GET['amountUSD'])? $_GET['amountUSD'] : 0;
/*
ini_set( 'display_errors', 'On' );
error_reporting( E_ALL );
$userid=356;
$resp='00';
$ref='ACHG-1500455647482';
$amount=10;
$currency='NGN';
$amountUSD=1;
echo "userid:$userid, resp: $resp, ref:$ref, amount:$amount, currency:$currency, amountUSD:$amountUSD";
*/

$success = false;

if( $userid &&
	($resp=='00' || $resp=='0') &&
	$ref!='0' &&
	$amount!=0 && $amountUSD ) { // primary call was ok

	$query = array(
	    "SECKEY"    => RAVEPAY_SECKEY,
	    "flw_ref"   => $ref,
	    "normaliza" => "1"
	);
	$data_string = json_encode($query);
	$ch = curl_init( RAVEPAY_VALIDATE );
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
//curl_setopt( $ch, CURLOPT_VERBOSE, true);
	$response = curl_exec($ch);
//printf("<br>cUrl error (#%d): %s<br>\n", curl_errno($ch), htmlspecialchars(curl_error($ch)));
	$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
	$header = substr($response, 0, $header_size);
	$body = substr($response, $header_size);
	curl_close($ch);
	$resp = json_decode($response, true);
	$chargeResponse = $resp['data']['flwMeta']['chargeResponse'];
	$chargeAmount   = $resp['data']['amount'];
	$chargeCurrency = $resp['data']['transaction_currency'];
//vardump($resp);
//echo "<hr>$chargeResponse $chargeAmount $chargeCurrency"; exit;
	if( ($chargeResponse == "00" || $chargeResponse == "0") &&
		($chargeAmount == $amount) && ($chargeCurrency == $currency) ) {
		// update deposit in db - here!
		$oUser->query( "SELECT id FROM lotto_deposit WHERE payment_id='$ref'" );
		if( !$oUser->singleRecord()->Record ) { // it will be a new record
			// create lotto_deposit record
			$br = getBrowser();
			$detect = new MobileDetect;
			$data = array(
				'userid'      => $userid,
				'amount'      => $amountUSD,
				'status'      => 'Paid',
				'deviceip'    => $_SERVER['REMOTE_ADDR'],
				'paiddate'    => gmdate('Y-m-d H:i:s'),
				'byadmin'     => 'No',
				'deposittype' => 'Bank', // RavePay ???
				'payment_id'  => $ref, // flwRef
				'platform'    => $br['platform'],
				'browser'     => $br['name'],
				'version'     => $br['version'],
				'device'      => ($detect->isMobile() ? ($detect->isTablet() ? 'tablet':'mobile'):'desktop'),
				'payment_batch_num' => "$amount $currency",
			);
			$oUser->query( "
				INSERT INTO lotto_deposit (".implode( ',', array_keys( $data ) )." ) 
				VALUES ( '".implode( "' , '", $data )."')" );
			// add user balance
			if( $oUser->insertID() ) { // deposit record added
				$oUser->updateBalance( $amountUSD, 'C', $userid, 'RavePay ID:'.$ref );
				$oUser->clearUnfinishedPay( $userid ); // payment done so clear "unfinishedPay"
				// send confirmation email
				$data = new stdClass;
				$data->fundAmount = "$".number_format( $amountUSD, 2 );
				$data->toEmail    = $oUser->getEmail();
				$data->fullName   = $oUser->getName();
				$data->toName     = $oUser->getName();
				(new Email)->fundsDepositedByUser( $data );
			}
		} // else - probably this is a hacking - fake a success, no DB update, no email!
		// show success page
		$success = true;
	}
}

if( $success ) { // Success page
	echo '
<div class="success-msg">Payment completed successfully</div><br/>
<script> parent.balReload() </script>
<p>'.CLICK2CONTINUE.'</p>
<button class="green-btn padding10" style="width:auto;margin-top:10px" onclick="this.disabled=true;parent.location.href=\'/vcheckout\'">
	Continue &nbsp;<i class="fa fa-arrow-circle-right"></i>
</button>';


} else { // Failure page
	echo '<div class="error-msg msg" style="margin: 6px auto;">Payment Failed!</div>';
	include(BASE_PATH.'inc/deposit-country.php');
}
