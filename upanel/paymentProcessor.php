<section>
	<div class="banner"><?php include('inc/timeline.php') ?></div>
	<div class="black-background"></div>
	<br/><br/><br/>
	<?php
	if( isset($_POST['action']) ) {
		switch( $_POST['action'] ) {
			case 'submit':
				$raven = new RavenPayment();
				$resp = $raven->ravenSubmit( $_POST );
				break;
		}

		if( $resp['result'] == 'success' ) {
			$class_var = 'success';
		} else {
			$class_var = 'error';
		}
		echo "<div class='".$class_var."-msg'>".$resp['message']."</div><br/><br/><br/>";
	} else {
		?>
		<div class="error-msg">Error Occured!</div><br/><br/><br/>
	<?php
	} ?>

</section>
