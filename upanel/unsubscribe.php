<?php

if( $oUser->getId() ) $oUser->unsubscribe();

?>
<div class="row">
	<div class="col-lg-12">
		<?php include('inc/timeline.php') ?>
		<div>
			<h5 class="pageHeading"><strong>Are You Sure? We're Sorry to See You Leaving…</strong></h5>
			<p>We just got your request to unsubscribe you from our promotional emails.
			You won't receive them anymore. Please keep in mind that we cannot
			unsubscribe you from the email notifications regarding your scanned
			tickets and winnings. You can turn on the promotional emails in 
			<a href="/account/account"><u>your profile</u></a> any time.</p>
			<p>Thank you for staying with 24Lottos.com!</p>
		</div>
	</div>
</div>
