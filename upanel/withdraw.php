<?php
	$_min = 5;
	$_max = 200;
	$_len = strlen("$_max");
?>
<section>
	<div class="banner">
	<?php
	include('inc/timeline.php');

	if( isset($_POST['confirmWithdraw']) ) {
		if( trim( $_POST['bankName'] )=='' || trim( $_POST['bankAccountNumber'] )=='' || trim( $_POST['bankAccHolderName'] )=='' ) {
			echo '<div class="error-msg">
				<strong>Error!</strong>
				Complete information of Bank is missed. Request would not be sent.
			</div>';

		} elseif( (float)$_POST['withAmount']<$_min ) {
			echo '<div class="error-msg">
				<strong>Error!</strong>
				Sorry, amount must be greater than '.$_min.' USD.
			</div>';

		} else if( $oUser->updateBankDetailsNWallet() && $requestID=$oUser->putWithdrawRequest() ) {
			$data = new stdClass();
			$data->toEmail = $oUser->getEmail();
			$data->toName = $oUser->getName();
			$data->requestID = $requestID;
			$data->firstName = $oUser->getName();
			$data->amount = $_POST['withAmount'];
			( new Email )->withdrawlRequest( $data );
			echo '<div class="success-msg">Success! Your withdrawal request has been sent to Administration.</div>';
		}
	}

	$witdAmnt = $oUser->getCurrentBalance();
	$witdAmntF = number_format( $witdAmnt, 2, '.', ',' );
	$oUser->getBankDetails( $oUser->getID() );
	?>

	<br/><div class="row">
		<h5 class="blue pull-left nomargin">Withdraw</h5>
		<h5 class="blue pull-right nomargin">Current Balance: $<?= $witdAmntF ?></h5>
	</div>

	<div class="table-responsive ">
		Your 24Lottos account will automatically credited with all of your winnings into your account balance, you can always withdraw your deposit but only to a bank account with your name on it that you own.<br/>
			Before you send a withdraw request, Kindly check that you have the amount you want to withdraw in your 24Lottos account balance.<br/>
			Kindly note that you may only withdraw from winnings and the minimum amount you can withdraw is $10.<br/>
			Type the amount to withdraw and add your bank account details, settling will take between 3 to 10 working days.<br/>
			See more at: <a href="<?= $site->baseURL( 'terms-and-conditions' ) ?>">Terms and Conditions.</a>
		<h5 style='margin-top:20px'>Please enter your bank account details</h5>
		<form method="post" id="frmWithdraw" onsubmit="return confirm('Are you sure you want to withdraw entered amount?')">
			<input type="hidden" name="maxWithdraw" id="maxWithdraw" value="<?= $witdAmnt ?>"/>
			<input type="hidden" name="withName" value="<?= $_SESSION['logged_name'] ?>"/>
			<input type="hidden" name="withRequest" value="<?= $_SESSION['logged_email'] ?>"/>
			<input type="hidden" name="withCurrency" value="<?= $_SESSION['logged_pref_currency'] ?>"/>
			<div class="col-lg-12 bank_details_wrapper">
				<input type="text" class="form-control" name="bankAccHolderName" id="bankAccHolderName"
					placeholder="Beneficiary Name*" required="required" autocomplete="off"
					value="<?= $oUser->Record->bankAccHolderName ?>"/>
				<input type="text" class="form-control" name="bankAccountNumber" id="bankAccountNumber"
					placeholder="Beneficiary Account*" required="required" autocomplete="off"
					value="<?= $oUser->Record->bankAccountNumber ?>"/>
				<input type="text" class="form-control" name="bankName" id="bankName"
					placeholder="Bank Name*" required="required" autocomplete="off"
					value="<?= $oUser->Record->bankName ?>"/>
				<input type="text" class="form-control" name=" bankBranch" id="bankBranch"
					placeholder="Branch Name*" required="required" autocomplete="off"
					value="<?= $oUser->Record->bankBranch ?>" />
				<input type="text" class="form-control" name="bankBranchCode" id="bankBranchCode"
					placeholder="Branch Code*"  required="required" autocomplete="off"
					value="<?= $oUser->Record->bankBranchCode ?>"/>
				<input type="text" class="form-control" name="bankOtherDetails"
					placeholder="Other Details" autocomplete="off"
					value="<?= $oUser->Record->bankOtherDetails ?>"/>
				<input type="text" class="form-control required number" name="withAmount" id="withAmount"
					min='<?= $_min ?>' max='<?= $_max ?>' maxlength='<?= $_len ?>' required="required"
					oninput='maxLengthCheck(this)' onblur='getWDsignature(this)'
					placeholder="Enter your amount ($<?= $witdAmntF ?>)" autocomplete="off"/>
			</div>
			<button id="confirmWithdraw" class="blue-btn padding10" type="submit">
				Confirm Withdraw &nbsp;<i class="fa fa-arrow-circle-right"></i>
			</button>
		</form>
	</div>
</section>

<script>
	function getWDsignature(o) {
		if( !o || o.tagName!='INPUT' ) return;
		// check input value
		var _v = parseFloat(o.value), _min = parseFloat(o.min), _max = parseFloat(o.max), _having = <?= $witdAmnt ?>;
		_min = isNaN(_min) ? <?= $_min ?>:_min; _max = isNaN(_max) ? <?= $_max ?>:_max;
		if( isNaN(_v) ) o.value = '';
		else if( _v>_having ) _v=0;
		if( _v<_min || _v>_max ) {
			alert('Amount must be between '+_min+' and '+_max+' USD\nYour Wallet Amount is <?= $witdAmntF ?> USD.');
			_v = _v<_min ? '':_max;
			o.value = _v;
			o.focus();
		}
	}
</script>
