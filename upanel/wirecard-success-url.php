<?php
$wc = new Wirecard;
$secret = $wc->getSeceret();

// checks if the parameter paymentState is available and set
$paymentState = isset($_POST["paymentState"]) ? $_POST["paymentState"]:"undefined";
// initiates the message text
$class = 'error';
$message = "The message text has not been set.";

if( strcmp( $paymentState, "CANCEL" ) == 0 ):
	$message = "The payment transaction has been cancelled by the user.";
    ?>
    <script>
        dataLayer.push({
            "event": "transaction",
            "eventCategory": "deposit",
            "eventAction": "purchase<?= $FIRST_DEPOSIT ? ' first time':'' ?>",
            "eventValue": "<?= $_POST['amount'] ?>",
            "eventLabel": "error - {<?=$message?>}"
        });
    </script>
    <?php

elseif( strcmp( $paymentState, "PENDING" ) == 0 ):
	$message = "The payment is pending and not yet finished.";
    ?>
    <script>
        dataLayer.push({
            "event": "transaction",
            "eventCategory": "deposit",
            "eventAction": "purchase<?= $FIRST_DEPOSIT ? ' first time':'' ?>",
            "eventValue": "<?= $_POST['amount'] ?>",
            "eventLabel": "error - {<?=$message?>}"
        });
    </script>
    <?php

elseif( strcmp( $paymentState, "FAILURE" ) == 0 ):
	// There was something wrong with the initiation or a
	// fatal error occured during the processing of the payment transaction.
	$message = "There occured an error during the payment transaction.";
    ?>
    <script>
        dataLayer.push({
            "event": "transaction",
            "eventCategory": "deposit",
            "eventAction": "purchase<?= $FIRST_DEPOSIT ? ' first time':'' ?>",
            "eventValue": "<?= $_POST['amount'] ?>",
            "eventLabel": "error - {<?=$message?>}"
        });
    </script>
    <?php

elseif( strcmp( $paymentState, "SUCCESS" ) == 0 ):
	// The payment transaction has been completed successfully.

	// Collects fingerprint details for checking if response fingerprint
	// sent from Wirecard is correct.
	$responseFingerprintOrder = isset($_POST["responseFingerprintOrder"]) ? $_POST["responseFingerprintOrder"]:"";
	$responseFingerprint = isset($_POST["responseFingerprint"]) ? $_POST["responseFingerprint"]:"";

	$fingerprintString = ""; // contains the values for computing the fingerprint
	$mandatoryFingerPrintFields = 0; // contains the number of received mandatory fields for the fingerprint
	$secretUsed = 0; // flag which contains 0 if secret has not been used or 1 if secret has been used
	$order = explode( ",", $responseFingerprintOrder );

	for( $i = 0; $i < count( $order ); $i++ ) {
		$key = $order[$i];
		$value = isset($_POST[$order[$i]]) ? $_POST[$order[$i]]:"";
		// checks if there are enough fields in the responsefingerprint
		if( (strcmp( $key, "paymentState" )) == 0 && (strlen( $value ) > 0) ) $mandatoryFingerPrintFields++;
		if( (strcmp( $key, "orderNumber" )) == 0 && (strlen( $value ) > 0) )  $mandatoryFingerPrintFields++;
		if( (strcmp( $key, "paymentType" )) == 0 && (strlen( $value ) > 0) )  $mandatoryFingerPrintFields++;
		// adds secret to fingerprint string
		if( strcmp( $key, "secret" ) == 0 ) {
			$fingerprintString .= $secret;
			$secretUsed = 1;
		} else {
			// adds parameter value to fingerprint string
			$fingerprintString .= $value;
		}
	}

	// computes the fingerprint from the fingerprint string
	$fingerprint = hash_hmac( "sha512", $fingerprintString, $secret );

	if( (strcmp( $fingerprint, $responseFingerprint ) == 0) && ($mandatoryFingerPrintFields == 3) && ($secretUsed == 1) ) {
		// check is it 1st deposit
		$FIRST_DEPOSIT = $oUser->isFirstDeposit( $oUser->getID(), 'Wirecard mobile' );
		// everything is ok, store the successfull payment in db
		$trans_id = $wc->addTransactionInDB( $_POST );
		$oUser->updateBalance( $_POST['amount'], 'C', $oUser->getID(), "WireCard ID:$trans_id" );
		$oUser->clearUnfinishedPay(); // payment done so clear "unfinishedPay"

		// send confirmation email, tpl: funds-deposited-by-user
		$data = [
			'fullName'  => $oUser->getName(),
			'fundAmount'=> '$'.$_POST['amount'],
			'toEmail'   => $oUser->getEmail(),
			'toName'    => $oUser->getName(),
		];
		(new Email)->fundsDepositedByUser( (object)$data );

		$class = 'success';
		$message = "Payment completed successfully.";
testWithLog( "--- 1st dep GTM --- UserID=".$oUser->getID()." purchase".( $FIRST_DEPOSIT ? ' first time':'' ) );
		if( GAENABLED ) {
		?>
		<script>
		dataLayer.push({
			"event": "transaction",
			"eventCategory": "deposit",
			"eventAction": "purchase<?= $FIRST_DEPOSIT ? ' first time':'' ?>",
//			"eventValue": "<?//= $_POST['amount'] ?>//",
			"eventLabel": "success, WireCard, mobile",
			"ecommerce": {
				"purchase": {
					"actionField": {
						"id": "<?= $trans_id ?>",
						"affiliation": "site",
						//"revenue": "<?= $_POST['amount'] ?>",
						"coupon": ''
					},
					"products": [{
						"name": "Deposit",
						"category": "Deposit",
						"price":  "<?= $_POST['amount'] ?>",
						"quantity": 1
					}]
				}
			}
		});
		</script>
		<?php
		}
	} else {
		// there is something strange, maybe an unauthorized
		// call of this page or a wrong secret
		$message = "The verification of the response data was not successful.";
	}
else:
	// unauthorized call of this page
	$message = "Error: The payment state is not valid.";
endif;
?>
<br/>
<div class='<?= $class ?>-msg'><?= $message ?></div>
<script> parent.balReload() </script>
<p><?= CLICK2CONTINUE ?></p>
<button class="green-btn padding10" style="width:auto;margin-top:10px" onclick="this.disabled=true;parent.location.href='/vcheckout'">
	Continue &nbsp;<i class="fa fa-arrow-circle-right"></i>
</button>
