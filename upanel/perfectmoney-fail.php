<div class="row">
	<div class="col-lg-8">
		<div class="col-lg-12">
			<?php include('inc/timeline.php') ?>
		</div>
		<div class="col-lg-9">
			<ul class="list-inline pull-left upnlmnu" style="margin-left:4px;">
				<li class="link" data-link="<?= $site->baseURL( "account/dashboard" ) ?>">Dashboard</li>
				<li class="link" data-link="<?= $site->baseURL( "account/account" ) ?>">Account</li>
				<li class="link" data-link="<?= $site->baseURL( "account/tickets" ) ?>">My Tickets</li>
				<li class="link" data-link="<?= $site->baseURL( "account/wallet" ) ?>">Wallet</li>
				<li class="link" data-link="<?= $site->baseURL( "account/redeem" ) ?>">Bonus</li>
			</ul>
		</div>
		<div class="col-lg-3">
			<ul class="list-inline pull-right upnlmnu">
				<li class="green link" data-link="<?= $site->baseURL( "account/deposit" ) ?>">Quick Deposit</li>
			</ul>
		</div>

		<div style="clear: both;"></div>

		<div class="row">
			<div class="col-lg-3">
				<h5><strong>Deposit</strong></h5>
			</div>
			<div class="col-lg-9">
				<ul style="margin: 12px -19px 0px 0px;" class="list-inline centered pull-right">
					<li class="draw">Current Balance: <?= $oUser->getCurrentBalance() ?></li>
					<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
					<li class="draw">Bonus Balance: <?= $oUser->getBonusBalance() ?></li>
					<li>&nbsp;&nbsp;</li>
				</ul>
			</div>
		</div><!-- row -->

		<?php if( array_key_exists( 'PAYMENT_BATCH_NUM', $_POST ) && $_POST['PAYMENT_ID'] == 'NULL' && array_key_exists( 'ERROR', $_POST ) ): ?>
			<div class="table-responsive ">
				<table class="table table-striped payment-method">
					<thead>
					<tr>
						<th class="centered tbl-heading" colspan="4">&nbsp;</th>
					</tr>
					</thead>
					<tbody style="font-weight: 500;color: #333;">
					<tr>
						<td colspan="4" class="centered"><strong>Oops! Your Deposit Transaction is Failed.</strong></td>
					</tr>
					<tr>
						<td colspan="4">
							<div id="paymentDesc"><br/>
								Relax! Nothing to worry though.<br/><br/>
								In case money doesn't credit back to your account within next 24 hours, please contact
								us.<br/><br/></div>
						</td>
					</tr>

					</tbody>
				</table>
			</div>
		<?php else: ?>
			<div class="table-responsive ">
				<table class="table table-striped payment-method">
					<thead>
					<tr>
						<th class="centered tbl-heading" colspan="4">&nbsp;</th>
					</tr>
					</thead>
					<tbody style="font-weight: 500;color: #333;">
					<tr>
						<td colspan="4" class="centered"><strong>Sorry to know that you have cancelled the
								transaction.</strong></td>
					</tr>
					<tr>
						<td colspan="4">
							<div id="paymentDesc"><br/>
								Please let us know if you have any concerns.<br/><br/>
								OR if we can help you out in anything. We are eager to serve you and see you make money.<br/><br/>
							</div>
						</td>
					</tr>

					</tbody>
				</table>
			</div>
		<?php endif; ?>

	</div>

</div>
