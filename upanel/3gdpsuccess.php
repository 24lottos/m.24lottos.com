<section>
	<?php
	$ID = $userID = $TransRef = 0;
	$TransactionToken = str_replace( ["'",';',chr(0)],'', trim(@$_GET['TransactionToken']) );
	$oUser->query( "
		SELECT id, user_id, response
		  FROM lotto_transactions
		 WHERE type='createToken' AND status='success' AND
			   TrackingNumber='".$TransactionToken."' " );
	if( $rec = $oUser->singleRecord()->Record ) {
		$ID      = $rec->id;
		$userID  = $rec->user_id;
		$TransRef= $rec->response;
	}
	// check is it 1st deposit
	$FIRST_DEPOSIT = $oUser->isFirstDeposit( $userID, '3gdp mobile' );
	// verify 3gdp
	$pay3gdp = new payment3gdp();
	$resp = $pay3gdp->verifyToken( $oUser, $ID, $TransRef );
	if( $resp['result']=='success' ) {
		$oUser->clearUnfinishedPay(); // payment done so clear "unfinishedPay"
		$class = 'success';
		if( GAENABLED ) { ?>
		<script>
		dataLayer.push({
			"event": "transaction",
			"eventCategory": "deposit",
			"eventAction": "purchase<?= $FIRST_DEPOSIT ? ' first time':'' ?>",
//			"eventValue": "<?//= $resp['amount'] ?>//",
			"eventLabel": "success, 3GDP, mobile",
			"ecommerce": {
				"purchase": {
					"actionField": {
						"id": "<?= $ID ?>",
						"affiliation": "site",
						//"revenue": "<?= $resp['amount'] ?>",
						"coupon": ''
					},
					"products": [{
						"name": "Deposit",
						"category": "Deposit",
						"price":  "<?= $resp['amount'] ?>",
						"quantity": 1
					}]
				}
			}
		});
		</script>
		<?php }

	} else {
		$class = 'error';
        ?>
        <script>
            dataLayer.push({
                "event": "transaction",
                "eventCategory": "deposit",
                "eventAction": "purchase<?= $FIRST_DEPOSIT ? ' first time':'' ?>",
                "eventValue": "<?= $resp['amount'] ?>",
                "eventLabel": "error - {<?= $resp["result"]?>}"
            });
        </script>
        <?php
	}
	echo "<div class='".$class."-msg'>".$resp['message']."</div><br/>";

	?>
	<script> parent.balReload() </script>
	<p><?= CLICK2CONTINUE ?></p>
	<button class="green-btn padding10" style="width:auto;margin-top:10px" onclick="this.disabled=true;parent.location.href='/vcheckout'">
		Continue &nbsp;<i class="fa fa-arrow-circle-right"></i>
	</button>
</section>
