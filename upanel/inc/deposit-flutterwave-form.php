<?php
//onfocus="this.type=\'number\'" onblur="this.type=\'password\'"

/**
* deposit-flutterwave-form.php - FlutterWave Form, mobile version
* used in /app/src/payment/PFlutterwave.php
*
* check a constant VERVE_MODE - show additional fields etc
*
* @author     Sergey V Musenko <sergey@musenko.com>
* @date       2017-01-12 22:29:09 EET
* @version    0.2
*/
$fixedRate = 26;
$fixedRateUsers = [ 196, 356 ]; // set NGN rate=$fixedRate for these users

echo '<!DOCTYPE html><html lang="en"><head>';
require(SITE_DIR.'/inc/header_head.php');

$_min = DEPOSIT_MIN;
$_max = DEPOSIT_MAX;
$_len = strlen("$_max");

if( $code=='NG' ) { // Nigeria - must pay in NGN
	$currency = 'NGN';
	if( in_array($oUser->getID(),$fixedRateUsers) ) $RATE = $fixedRate;
	else $RATE = FW_ADDPERCENT * toLocalCurrency('USD','NGN',1); // official rate + %ADDPERCENT

} else {
	$code     = 'NG'; // force NG country
	$currency = 'USD';
	$RATE     = 1;    // pure USD
}

$_SESSION['payCountryCode'] = [ $code, $currency ];
?>

<style>html,body { margin:0; padding:0; max-height:350px; overflow:hidden;} </style>
</head><body>

<div id="loading-overlay" style="display:none"></div>

<form method="post" id="flutterwaveF"
	onsubmit="parent.INPROGRESS=true;$('#loading-overlay').show();markStartPay();<?= VERVE_MODE ? 'parent.VerveWarning();':'' ?>return submitDisable('flutterwaveFbtn')">

<input type="hidden" name="country"  value="<?= $code ?>"/>
<input type="hidden" name="currency" value="<?= $currency ?>"/>
<input type="hidden" name="fw_rate" id="fw_rate" value="<?= $RATE ?>"/>
<input type="hidden" name="amount"  id="amount"  value="0"/>
<input type="hidden" name="amountV" id="amountV" value="0"/>

<table class="table table-striped">
    <tbody style="font-weight: 500;background: #f2f8fb;color: #333;">
        <tr class="payment_flutterwave">
			<td><label for="card_no" class="label_fields">Card number&nbsp;</label></td>
			<td width="100%">
				<input id="card_no" type="number" name="card_no" class="form-control" required="required" maxlength="19" autocomplete="off"
					placeholder="<?= VERVE_MODE ? '5060':'4225' ?> 0000 0000 0000"
					onkeydown="var keyCode=('which' in event) ? event.which:event.keyCode; return !(keyCode==69||keyCode==101);"
					oninput="this.setCustomValidity('');maxLengthCheck(this)"
					onblur="if(this.value.length<15)this.setCustomValidity('Card Number must be 15 digits at least')">
			</td>
		</tr><tr>
			<td><label for="expiry_month" class="label_fields">Expiry Month</label></td>
			<td style="padding-left:0;padding-right:0">
				<table width="100%" cellpadding="0" cellspacing="0">
				<tbody><tr>
					<td width="40%" class="nopadding">
						<select name="expiry_month" id="expiry-month" class="form-control">
						<?php
							$_i = date('n');
							for( $i=1; $i<=12; $i++ ) echo "<option".($i==$_i ? ' selected':'').">$i</option>";
						/* <input id="expiry_month" type="number" name="expiry_month" placeholder="<?= date('m') ?>" class="form-control" required="required" maxlength="2" autocomplete="off" oninput="maxLengthCheck(this)"> */
						?>
						</select>
					</td>
					<td width="20%"><label for="expiry_year" class="label_fields" style="float:right">Expiry&nbsp;<br>Year</label></td>
					<td width="40%" class="nopadding">
						<select name="expiry_year" id="expiry-year" class="form-control">
						<?php
							$_i = date('Y');
							for( $i=$_i; $i<=$_i+7; $i++ ) echo "<option".($i==$_i ? ' selected':'').">$i</option>";
						/* <input id="expiry_year" type="number" name="expiry_year" placeholder="<?= 1+date('y') ?>" class="form-control" required="required" maxlength="2" autocomplete="off" oninput="maxLengthCheck(this)"> */
						?>
						</select>
					</td>
				</tr></tbody>
				</table>
			</td>
		</tr><tr title="A Security Code on the back of the card">
			<td>
				<div style=" position:relative">
					<span class="cvv" onclick="$('.modal-backdrop').show();$('#cvvBanner').fadeToggle();cvvTO=setTimeout('cvvHide()',2000)">?</span>
					<label for="cvv" class="label_fields">CVV</label>
				</div>
			</td><td style="padding-left:0;padding-right:0">
				<table width="100%" cellpadding="0" cellspacing="0">
				<tbody><tr>
					<td width="40%" class="nopadding">
						<div class="input-secret">
							<div>&bull;&bull;&bull;</div>
							<input id="cvv" type="number" name="cvv" placeholder="&bull;&bull;&bull;" class="form-control" required="required" maxlength="3" autocomplete="off"
								onkeydown="var keyCode=('which' in event) ? event.which:event.keyCode; return !(keyCode==69||keyCode==101);"
								onfocus="this.style.opacity='1';$(this).next().fadeOut()"
								onblur="if(this.value.length<this.maxLength)this.value='';this.style.opacity='0'; $(this).prev().css({'color':(!this.value.length ? '#aaa':'#555')})"
								oninput="this.setCustomValidity('');maxLengthCheck(this)"
								oninvalid="this.setCustomValidity('CVV must be 3 digits')">
							<img src="<?= $site->baseURL( '/assets/img/what-is-cvv.png' ) ?>" alt="What is CVV" class="cvv" id="cvvBanner" onclick='cvvHide()'/>
						</div>
					</td>

					<?php if( VERVE_MODE ) { ?>
					<td width="20%" class="nopadding" title="ATM PIN Code, 4 digits">
						<label for="card-pin" style="float:right">PIN&nbsp;</label></td>
					<td width="40%" class="nopadding">
						<div class="input-secret pin">
							<div>&bull;&bull;&bull;&bull;</div>
							<input id="card-pin" type="number" name="pin" placeholder="&bull;&bull;&bull;&bull;" class="form-control" required="required" maxlength="4" autocomplete="off"
								onkeydown="var keyCode=('which' in event) ? event.which:event.keyCode; return !(keyCode==69||keyCode==101);"
								onfocus="this.style.opacity='1'"
								onblur="if(this.value.length<this.maxLength)this.value='';this.style.opacity='0'; $(this).prev().css({'color':(!this.value.length ? '#aaa':'#555')});"
								oninput="this.setCustomValidity('');maxLengthCheck(this)"
								oninvalid="this.setCustomValidity('PIN must be 4 digits')">
						</div>
					</td>
					<?php } else { ?>
					<td width="30%"></td><td width="30%"></td>
					<?php } ?>

				</tr></tbody>
				</table>

			</td>
		<tr>
			<td colspan="2" align="center">
				<button type="submit" class="green-btn padding10" id="flutterwaveFbtn" style="width:auto;margin-top:10px;padding-left:20px">
					CONTINUE &nbsp; <i class="fa fa-arrow-circle-right" style="margin-left: -5px;margin-right: 8px;" aria-hidden="true" title="Deposit funds"></i>
				</button>
				<div style="width:240px; margin:10px auto; font-size:13px">
					<img src="img/lock-03.png" alt="lock" style="float:left" title="Payment from <?= $code ?> in <?= $currency ?>">
					All transactions are guaranteed,<br/>safe and secured.
				</div>
				<span class="transaction_note">*This transaction will be added to your account in <span class="symbol" data-symbol="USD"></span></span>
            </td>
        </tr>
    </tbody>
</table>
</form>

<div class="modal-backdrop in" style="display:none"></div>

<link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
<script type="text/javascript" src="<?=BASE_URL?>assets/js/jquery.validate.js?v=<?= JSVERSION ?>"></script>
<script src="js/intlTelInput.min.js?v=<?= JSVERSION ?>"></script>
<script type="text/javascript" src="js/common.min.js?v=<?= JSVERSION ?>"></script>
<script>
$(document).ready(function () {
	$("#loading-overlay").hide();
});
var cvvTO=false, amountV=0;
$("#flutterwaveFbtn").click(function (e) {
	var _v=parseFloat(parent.AMOUNT);
	var _f=$("#flutterwaveF")[0];
	var _r=parseFloat(_f.fw_rate.value);
	amountV=_f.amountV.value=_v;
	_f.amount.value=Math.round(_r * _v * 100)/100;
});
function cvvHide() {
	if(cvvTO) clearTimeout(cvvTO);
	$(".modal-backdrop").fadeOut();
	$('#cvvBanner').hide();
}
$(".modal-backdrop").click(function(e) { cvvHide(); $("#cvv").focus(); });
function markStartPay() {
	// mark "user started a payment"
	$.ajax({
		url: "/ajax/index.php",
		type: "POST",
		data: { action: "payment-start", method: "FW-cc", amount: amountV }
	});
}
</script>

</body>
</html>
