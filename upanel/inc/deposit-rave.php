<?php
$_min = DEPOSIT_MIN;
$_max = DEPOSIT_MAX;
$_len = strlen("$_max");

$trxref   = "rave".time().rand(100,999); // our local transaction id, not in use indeed

//$country  = $site->country->code;
//$currency = $site->country->currencyCode;
$country  = 'NG';
$currency = 'NGN';
$_SESSION['payCountryCode'] = [ $country, $currency ]; // in use in User::updateBalance()
$RATE = RAVEPAY_ADDPERCENT*toLocalCurrency('USD',$currency,1); // official rate
//echo $country.', '.$currency.', rate= '.$RATE;
if( $oUser->getID()==356 ) { // testing user
	$_min = 0.1;
	$RATE = 10;
}

$email = str_replace( '"', '\"', $oUser->getEmail() );
?>
<style> html,body {max-height:180px; overflow:hidden;} </style>
<div style='background: #f2f8fb' id="wirecardF">
	<form method="post" onsubmit="return false;">
		<input type="hidden" id="wc_amount" name='amount'>

		<div class='payment_wirecard'>
			<div class="row">
				<div style="text-align:center;padding-top:10px">
					<button type="submit" class="green-btn padding10" id="raveFbtn" style='width:auto;padding-left:20px'>
						CONTINUE &nbsp; <i class="fa fa-arrow-circle-right" style="margin-left: -5px;margin-right: 8px;" aria-hidden="true" title="Deposit funds"></i>
					</button>
					<div style="width:240px; margin:10px auto; font-size:13px">
						<img src="img/lock-03.png" alt="lock" style="float:left" title="Payment from '.$code.' in '.$currency.'">
						All transactions are guaranteed,<br/>safe and secured.
					</div>
					<span class="transaction_note">* This transaction will be added to your account in <span class="symbol" data-symbol="USD"></span></span>
				</div>
			</div>
			<div style="clear: both"></div>
		</div>
	</form>
</div>

<script>
document.addEventListener("DOMContentLoaded", function(event) {
	document.getElementById("raveFbtn").addEventListener("click", function() {
		var retURL='<?= $site->baseURLm("iframe/rave-success")?>';
		var currency="<?= $currency ?>",
			amountUSD = parseFloat(parent.AMOUNT),
			amount = Math.round(<?= $RATE ?>*amountUSD * 100)/100,
			chargeResponse = "",
			trxref = "<?= $trxref ?>",        // your transaction ref here
			pubkey = "<?= RAVEPAY_PUBKEY ?>"; // public keys generated on your dashboard here
		if(isNaN(amountUSD) || amountUSD<<?= $_min ?> || amountUSD><?= $_max ?>) {
			alert("Please enter a valid amount!");
			return false;
		}
		// mark "user started a payment"
		$.ajax({
			url: '/ajax/index.php',
			type: 'POST',
			data: { action: 'payment-start', method: 'RavePay', amount: amountUSD }
		});
		// start RAVE!
		parent.getpaidSetup({
			PBFPubKey: pubkey,
			txref: trxref,
			payment_method: "account", // "both", "card" -accept only card payments for USD and Ghana
			amount: amount,
			currency: currency,
			country: "<?= $country ?>",
			custom_logo: "https://www.24lottos.com/assets/img/logo64x64.png",
			custom_title: "24Lottos Deposit",
			custom_description: "",
			customer_email: "<?= $email ?>",
			onclose: function(response) {
				location.href=retURL;
			},
			callback: function(response) {
				// console.log("This is the response returned after a charge", response);
				// redirect to a success page; flw_ref = response.tx.flwRef;
				if(response.tx) {
					var url=retURL+
						'?user=<?= $oUser->getID() ?>'+
						'&resp='+response.tx.chargeResponseCode+
						'&ref='+response.tx.flwRef+
						'&amount='+amount+
						'&currency='+currency+
						'&amountUSD='+amountUSD;
					location.href=url;
				}
			}
		});
	});
});
</script>
