<?php
$_min = DEPOSIT_MIN;
$_max = DEPOSIT_MAX;
$_len = strlen("$_max");

$currency = 'USD';
$currSign = '$';
$cntr = getCountryCode();
$_SESSION['payCountryCode'] = [ $cntr->code, $currency ];
?>
<style> html,body {max-height:180px; overflow:hidden;} </style>
<div style='background:#f2f8fb'>
	<form action="<?= $site->baseURLm( 'account/paymentProcessor' ); ?>" target="_self" method="post"
		id="mpesaF" onsubmit="return false;">
		<input type="hidden" name="action" value="create-3gdp-token">
		<input type="hidden" name="currency" value="<?= $currency ?>"/>
		<input type="hidden" name="currSign" value="<?= $currSign ?>"/>
		<input type="hidden" name="amount" id="3gdp_amount" value='0'/>

		<div class="row">
			<div style="text-align:center;padding-top:10px">
				<button type="submit" class="green-btn padding10" id="mpesaFbtn" style='width:auto;padding-left:20px'>
					CONTINUE &nbsp; <i class="fa fa-arrow-circle-right" style="margin-left: -5px;margin-right: 8px;" aria-hidden="true" title="Deposit funds"></i>
				</button>
				<div style="width:240px; margin:10px auto; font-size:13px">
					<img src="img/lock-03.png" alt="lock" style="float:left" title="Payment from '.$code.' in '.$currency.'">
					All transactions are guaranteed,<br/>safe and secured.
				</div>
				<span class="transaction_note">* This transaction will be added to your account in <span class="symbol" data-symbol="USD"></span></span>
			</div>
		</div>
	</form>

	<div id="3gModal" class="modal fade">
		<div class="modal-backdrop in"></div>
		<div class="modal-dialog" style="top:80px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Your Payment is in progress!</h4>
				</div>
				<div class="modal-body">
					<h5>Please do not<br>
					refresh the page or press Back button of your browser<br>
					while payment is being processed!</h5>
					<button type="button" class="btn2 btn-success pull-left" data-dismiss="modal" aria-hidden="true">I got it!</button>
				</div>
			</div>
		</div>
	</div>

	<div style="clear: both"></div>
</div>

<script>
    $(document).ready(function() {
        $("#mpesaFbtn").click(function (e) {
            e.preventDefault();
            var amount=parseFloat(parent.AMOUNT);
            if(isNaN(amount) || amount<<?= $_min?> || amount><?= $_max?>) {
                alert("Please enter a valid amount!");
                return false;
            }
            parent.INPROGRESS=true;
            $('#3gdp_amount').val(amount);
            submitDisable('mpesaFbtn');
            $('#mpesaFbtn').val('Redirecting...');
            $('#loading-overlay').show();
            //$("#3gModal").fadeIn();
            // mark "user started a payment"
            $.ajax({
                url: '/ajax/index.php',
                type: 'POST',
                data: { action: 'payment-start', method: '3gdp', amount: amount }
            });
            // start payment process
            $.ajax({
                url: '<?= $site->baseURLm( "ajax/index.php" ) ?>',
                type: "POST",
                cache: false,
                dataType: 'json',
                data: $('#mpesaF').serialize()
            }).done(function (data) {
                if (data.result == "success") {
                    location.replace(data.pay_url);
                } else {
                    $('#loading-overlay').hide();
                    //$("#3gModal").fadeOut();
                    parent.INPROGRESS=false;
                    alert(data.message);
                }
            }).fail(function () {
                $('#loading-overlay').hide();
                //$("#3gModal").fadeOut();
                parent.INPROGRESS=false;
                alert("'Error Occured! Please try again later!'");
            });
        });
        $('.modal-backdrop').click(function(){ $('#3gModal').fadeOut(); })
        $('#3gModal .close').click(function(){ $('#3gModal').fadeOut(); })
        $('#3gModal .btn2').click(function(){ $('#3gModal').fadeOut(); })
    });
</script>
