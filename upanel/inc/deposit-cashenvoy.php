<?php
// see CashEnvoy parms defined in /config/constants.php
$ce_merchantid = CASHENVOY_MTID; // $oGen->getCashEnvoyAccount();
$ce_transref = time();
$ce_customerid = $oUser->getID();
$ce_memo = "Amount Deposit in 24Lotto Wallet by ".$oUser->getName();
$ce_notifyurl = $site->baseURLm( CASHENVOY_CALLBACK );
// a signature generates with Ajax call, see below
$_min = DEPOSIT_MIN;
$_max = DEPOSIT_MAX;
$_len = strlen("$_max");
$RATE = CASHENVOY_ADDPERCENT * toLocalCurrency('USD','NGN',1); // official rate + 15%
if( strtolower(trim($oUser->getName()))=='test' ) $RATE = 26; // testing account

$currency = 'NGN';
$cntr = getCountryCode();
$_SESSION['payCountryCode'] = [ $cntr->code, $currency ];
?>

<style> html,body {max-height:180px; overflow:hidden;} </style>
<div style='background: #f2f8fb'>
	<form action="<?= CASHENVOY_URL ?>?cmd=cepay" target="_self" method="post" id="cashenvoyF">
		<input type="hidden" name="ce_merchantid" value="<?= $ce_merchantid ?>"/>
		<input type="hidden" name="ce_transref" value="<?= $ce_transref ?>"/>
		<input type="hidden" name="ce_customerid" value="<?= $ce_customerid ?>"/>
		<input type="hidden" name="ce_memo" value="<?= $ce_memo ?>"/>
		<input type="hidden" name="ce_notifyurl" value="<?= $ce_notifyurl ?>"/>
		<input type="hidden" name="ce_window" value="self"/>
		<input type="hidden" name="ce_signature" id="ce_signature" value=""/>
		<input type="hidden" name="ce_rate"    id="ce_rate" value='<?= $RATE ?>'/>
		<input type="hidden" name="ce_amount"  id="ce_amount" value='0'/>
		<input type="hidden" name="ce_amountV" id="ce_amountV" value='0'/>
		<input type="hidden" name="action" value="submit">

		<div class="row">
			<div style="text-align:center;padding-top:10px">
				<button type="submit" class="green-btn padding10" id="cashenvoyFbtn" style='width:auto;padding-left:20px'>
					CONTINUE &nbsp; <i class="fa fa-arrow-circle-right" style="margin-left: -5px;margin-right: 8px;" aria-hidden="true" title="Deposit funds"></i>
				</button>
				<div style="width:240px; margin:10px auto; font-size:13px">
					<img src="img/lock-03.png" alt="lock" style="float:left" title="Payment from '.$code.' in '.$currency.'">
					All transactions are guaranteed,<br/>safe and secured.
				</div>
				<span class="transaction_note">* This transaction will be added to your account in <span class="symbol" data-symbol="USD"></span></span>
			</div>
		</div>
	</form>
</div>

<script>
    $(document).ready(function() {
        $("#cashenvoyFbtn").click(function (e) {
            e.preventDefault();
            var amountUSD=parseFloat(parent.AMOUNT);
            if(isNaN(amountUSD) || amountUSD<<?= $_min?> || amountUSD><?= $_max?>) {
                alert("Please enter a valid amount!");
                return false;
            }
            parent.INPROGRESS=true;
            $('#ce_amountV').val(amountUSD);
            submitDisable('cashenvoyFbtn');
            $('#loading-overlay').show();
            // check input value
            var _r = parseFloat($('#ce_rate').val());
			amount = Math.round(amountUSD * _r * 100)/100;
            // get signature
            $('#ce_amount').val(amount);
            var ce_signature = document.getElementById('ce_signature');
            if( ce_signature && amount>0 ) {
                // mark "user started a payment"
                $.ajax({
                    url: '/ajax/index.php',
                    type: 'POST',
                    data: { action: 'payment-start', method: 'CashEnvoy', amount: amountUSD }
                });
                // start payment process
                $.ajax({
                    url: '<?= $site->baseURLm( "ajax/index.php" ) ?>',
                    type: 'POST',
                    data: {
                        action: 'cashenvoy_signature',
                        transref: '<?= $ce_transref ?>',
                        amountUSD: amountUSD,
                        amount: amount
                    },
                    success: function (data) {
                        ce_signature.value = data;
                        $('#cashenvoyF').submit();
                    },
                    error: function (jqXHR, exception) {
                        parent.INPROGRESS=false;
                        $('#loading-overlay').hide();
                        alert("'Error Occured! Please try again later!'");
                    }
                });
            }
            return false;
        });
    });
</script>
