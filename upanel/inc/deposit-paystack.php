<?php
$_min = DEPOSIT_MIN;
$_max = DEPOSIT_MAX;
$_len = strlen("$_max");

//$country  = $site->country->code;
//$currency = $site->country->currencyCode;
$country  = 'NG';
$currency = 'NGN';
$_SESSION['payCountryCode'] = [ $country, $currency ]; // in use in User::updateBalance()
$RATE = PAYSTACK_ADDPERCENT * toLocalCurrency('USD',$currency,1); // official rate
//echo $country.', '.$currency.', rate= '.$RATE;
if( $oUser->getID()==356 ) { // testing user
	$_min = 0.1;
	$RATE = 26;
}

$email = str_replace( '"', '\"', $oUser->getEmail() );
$mode  = isset($_GET['mode']) ? $_GET['mode'] : 'card'; // default is 'card'
$methodmode = $mode=='bank' ? '-bank':'';

?>
<style> html,body {max-height:180px; overflow:hidden;} </style>
<div style='background: #f2f8fb' id="wirecardF">
	<form method="post" onsubmit="return false;">
		<input type="hidden" id="ps_amount" name='amount'>

		<div class='payment_wirecard'>
			<div class="row">
				<div style="text-align:center;padding-top:10px">
					<button type="submit" class="green-btn padding10" id="paystackFbtn" style='width:auto;padding-left:20px'>
						CONTINUE &nbsp; <i class="fa fa-arrow-circle-right" style="margin-left: -5px;margin-right: 8px;" aria-hidden="true" title="Deposit funds"></i>
					</button>
					<div style="width:240px; margin:10px auto; font-size:13px">
						<img src="img/lock-03.png" alt="lock" style="float:left" title="Payment from '.$code.' in '.$currency.'">
						All transactions are guaranteed,<br/>safe and secured.
					</div>
					<span class="transaction_note">* This transaction will be added to your account in <span class="symbol" data-symbol="USD"></span></span>
				</div>
			</div>
			<div style="clear: both"></div>
		</div>
	</form>
</div>

<script>
	$("#paystackFbtn").click(function (e) {
		e.preventDefault();
		var email="<?= $email ?>", mode='<?= $mode ?>',
			amountUSD=parseFloat(parent.AMOUNT),
			amount = Math.round(<?= $RATE ?>*amountUSD * 100)/100;
		if(isNaN(amountUSD) || amountUSD<<?= $_min?> || amountUSD><?= $_max?>) {
			alert("Please enter a valid amount!");
			return false;
		}
		parent.INPROGRESS=true;
		$('#ps_amount').val(amount);
		submitDisable('wirecardFbtn');
		$('#loading-overlay').show();
		// mark "user started a payment"
		$.ajax({
			url: '/ajax/index.php',
			type: 'POST',
			data: { action:'payment-start', method:'PayStack<?= $methodmode ?>', amount:amountUSD }
		});
		// start payment process
		$.ajax({
			url: '/ajax/index.php',
			type: "POST",
			cache: false,
			dataType: 'json',
			data: { action:'process-paystack', mode:mode, email:email, amount:amount, amountUSD:amountUSD }
		}).done(function (data) {
			if(data.result == "success") {
				location.replace(data.pay_url);
			} else {
				$('#loading-overlay').hide();
				alert(data.message);
				parent.INPROGRESS=false;
			}
		}).fail(function (data) {
			$('#loading-overlay').hide();
			alert("'Error Occured! Please try again later!'");
			parent.INPROGRESS=false;
		});
	});
</script>
