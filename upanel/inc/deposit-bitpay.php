<?php
$_min = DEPOSIT_MIN;
$_max = DEPOSIT_MAX;
$_len = strlen("$_max");

if( $oUser->getID()==356 ) $_min = 0.1;

$cntr = getCountryCode();
$_SESSION['payCountryCode'] = [ $cntr->code, $cntr->currencyCode ];
file_get_contents("php://input");
?>
<style> html,body {max-height:180px; overflow:hidden;} </style>
<div style='background: #f2f8fb'>
	<form method="post" onsubmit="return false;">
		<input type="hidden" id="bitpay_amount" name='amount'>

		<div class='payment_bitpay'>
			<div class="row">
				<div style="text-align:center;padding-top:10px">
					<button type="submit" class="green-btn padding10" id="bitpayFbtn" style='width:auto;padding-left:20px'>
						CONTINUE &nbsp; <i class="fa fa-arrow-circle-right" style="margin-left: -5px;margin-right: 8px;" aria-hidden="true" title="Deposit funds"></i>
					</button>
					<div style="width:240px; margin:10px auto; font-size:13px">
						<img src="img/lock-03.png" alt="lock" style="float:left" title="Payment from '.$code.' in '.$currency.'">
						All transactions are guaranteed,<br/>safe and secured.
					</div>
					<span class="transaction_note">* This transaction will be added to your account in <span class="symbol" data-symbol="USD"></span></span>
				</div>
			</div>
			<div style="clear: both"></div>
		</div>
	</form>
</div>

<script>
    function hideBitpayDialog() {
//        console.log( 'hideBitpayDialog' );
        $('#loading-overlay').hide();
        location.replace( '<?= BITPAY_AFTER_CLOSE_URL ?>' );
    }

    $("#bitpayFbtn").click(function (e) {
//        e.preventDefault();
        amount=parseFloat(parent.AMOUNT);
        if(isNaN(amount) || amount<<?= $_min?> || amount><?= $_max?>) {
            alert("Please enter a valid amount!");
            return false;
        }
        parent.INPROGRESS=true;
        $('#bitpay_amount').val(amount);
        submitDisable('bitpayFbtn');
        $('#loading-overlay').show();

        // mark "user started a payment"
        $.ajax({
            url: '/ajax/index.php',
            type: 'POST',
            data: { action: 'payment-start', method: 'Bitpay', amount: amount }
        });

		$.ajax({
			url: '<?= $site->baseURLM( "ajax/index.php" ) ?>',
			type: 'POST',
			data: {
				action: 'process-bitpay',
				amount: amount
			},
			success: function (data) {
				var response = $.parseJSON(data);
				console.log(response.invoiceID);

				if( response.error == false ) {
//					location.replace( response.invoiceURL );
					parent.showBitpayDialog( response.invoiceID );
				} else {
					$('#loading-overlay').hide();
					alert( responce.error );
				}
			},
			error: function (jqXHR, exception) {
				parent.INPROGRESS=false;
				$('#loading-overlay').hide();
				alert("'Error Occured! Please try again later!'");
			}
		});
	});
</script>
