<?php
/**
* deposit-flutterwave-validate.php - FlutterWave Validateion for PIN, BVN
* used in /app/src/payment/PFlutterwave.php
*
* form submits:
*	code     '02'
*	ref      trans_id
*	otp      OTP pass
*	cardType '' optional
*
* @author     Sergey V Musenko <sergey@musenko.com>
* @date       2017-01-12 22:29:09 EET
* @version    0.2
*/


echo '<!DOCTYPE html><html lang="en"><head>';
require(SITE_DIR.'/inc/header_head.php');
$_len = 10;
echo '
<style>
	body { margin: 0; padding: 0; }
</style>
</head><body>

<form method="post" action="'.FW_RESPONSE_URL.'"
	onsubmit="return submitDisable(\'flutterwaveFbtn\')">
<input type="hidden" name="code" value="'.$code.'"/>
<input type="hidden" name="ref" value="'.$trans_id.'"/>
<input type="hidden" name="cardType" value=""/>

<table class="table table-striped" id="flutterwaveF">
	<thead>
		<tr>
			<th colspan="2"><div class="blue-background" style="font-weight:normal; text-align:left">'.$message.'</div></th>
		</tr>
	</thead>

    <tbody style="font-weight: 500;background: #f2f8fb;color: #333;">
		<tr class="payment_flutterwave">
			<td height="60">
				<input type="text" name="otp" class="form-control" autocomplete="off" autofocus
					oninput="maxLengthCheck(this)"
					maxlength="'.$_len.'" required="required">
			</td>
			<td>
				<button type="submit" class="green-btn padding10" id="flutterwaveFbtn" style="width:120px;height:35px"><i class="fa fa-arrow-circle-up" aria-hidden="true" title="Deposit funds"></i> Confirm</button>
			</td>
		</tr>
	</tbody>
</table>
</form>
<script type="text/javascript" src="<?=BASE_URL?>assets/js/jquery.validate.js?v='.JSVERSION.'"></script>
<script src="js/intlTelInput.min.js?v='.JSVERSION.'"></script>
<script type="text/javascript" src="js/common.min.js?v='.JSVERSION.'"></script>
</body></html>';
