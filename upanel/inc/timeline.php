<div class="fa fa-pencil editTimelineImage link" data-link="<?= $site->baseURLm( 'account/timeline' ) ?>"
	 style="display:none"></div>
<div class="fa fa-pencil editProfileImage link" data-link="<?= $site->baseURLm( 'account/profile' ) ?>"
	 style="display:none"></div>
<div class="timeLineWelcomeMessage">
	Welcome<br/><?= $oUser->getName() == 'Anonymous' ? 'to 24Lottos.com':ucfirst( $oUser->getName() ) ?></div>
<div class="profileImageContainer">
	<div class="profileImage">
		<img src="<?= $oUser->getProfilePic() ?>"/>
	</div>
</div>

<div style="max-height:170px; overflow:hidden;">
	<img src="<?= $site->baseURL( "images/{$oUser->getTimelinePic()}" ) ?>" class="img-responsive  timeline" style="width:100%"/>
</div>
