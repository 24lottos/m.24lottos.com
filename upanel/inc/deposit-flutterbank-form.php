<?php
/**
* deposit-flutterbank-form.php - FlutterWave Account Flow Form
* used in /app/src/payment/AFlutterwave.php
*
* @author     Sergey V Musenko <sergey@musenko.com>
* @date       2017-03-05
* @version    0.1
*/

$fixedRate = 26;
$fixedRateUsers = [ 196, 356 ]; // set NGN rate=$fixedRate for these users


echo '<!DOCTYPE html><html lang="en"><head>';
require(SITE_DIR.'/inc/header_head.php');

$c_min = DEPOSIT_MIN;
$c_max = DEPOSIT_MAX;
$c_len = strlen("$c_max");

$code     = 'NG';
$currency = 'NGN';
$RATE     = FW_ADDPERCENT * toLocalCurrency('USD','NGN',1); // official rate + %ADDPERCENT

$_SESSION['payCountryCode'] = [ $code, $currency ];
?>

<style>html,body { margin:0; padding:0; max-height:330px; overflow:hidden;} </style>
</head><body>

<div id="loading-overlay" style="display:none"></div>

<form id="flutterbankF" method="post"
	onsubmit="parent.INPROGRESS=true;$('#loading-overlay').show();markStartPay();return submitDisable('flutterbankFbtn')">
<input type="hidden" name="country"  value="<?= $code ?>"/>
<input type="hidden" name="currency" value="<?= $currency ?>"/>
<input type="hidden" name="fw_rate"  value="<?= $RATE ?>"/>
<input type="hidden" name="amount"  id="amount"  value="0"/>
<input type="hidden" name="amountV" id="amountV" value="0"/>

<div class="payment_flutterwave">
	<table width="100%" class="credit-card-details">
	<tbody>
		<tr>
			<td colspan="2">
				<label for="bankCode">Select your Bank</label>
				<select name="bankCode" required="required" class="form-control" style="width:100%"><?= $bankSelect ?></select>
			</td>
		</tr><tr>
			<td>
				<label for="accountNumber">Account Number</label>
				<input type="number" name="accountNumber" id="accountNumber" placeholder="10 numbers" maxlength="11" class="form-control" required="required" autocomplete="off"
					oninput="maxLengthCheck(this)">
			</td>
			<td title="Please select 4 to 6 digit security PIN, it will be mapped to the account">
				<label for="passCode">Pass Code</label>
				<input  type="password" name="passCode" id="passCode" placeholder="4 to 6 numbers" maxlength="6" min="1000" max="999999" class="form-control" required="required" autocomplete="off"
					onfocus="if(!$.browser.mozilla)this.type='number'"
					onblur="this.value=this.value.replace('e','');if(this.value.length<this.min.length||this.value.length>this.max.length)this.value='';if(!$.browser.mozilla)this.type='password';"
					oninput="maxLengthCheck(this)">
			</td>
		</tr>
		<tr>
			<td width="25%" nowrap>
				<label for="firstName">First Name</label>
				<input type="text" name="firstName" id="firstName" placeholder="" maxlength="64" class="form-control" required="required" autocomplete="off"
					oninput="maxLengthCheck(this)">
			</td>
			<td width="25%" nowrap>
				<label for="lastName">Last Name</label>
				<input type="text" name="lastName" id="lastName" placeholder="" maxlength="64" class="form-control" required="required" autocomplete="off"
					oninput="maxLengthCheck(this)">
			</td>
		</tr><tr>
			<td width="25%" nowrap>
				<label for="email">Email</label>
				<input type="email" name="email" id="email" placeholder="" maxlength="96" class="form-control" required="required" autocomplete="off"
					oninput="maxLengthCheck(this)" value="<?= $userEmail ?>">
			</td>
			<td width="25%" nowrap>
				<label for="phoneNumber">Phone Number</label>
				<input type="tel" name="phoneNumber" id="phoneNumber" placeholder="" maxlength="15" pattern="[0-9+ \-]{8,15}" class="form-control" required="required" autocomplete="off"
					oninput="maxLengthCheck(this)">
			</td>
		</tr><tr>
			<td colspan="2" align="center">
				<button type="submit" class="green-btn padding10" id="flutterbankFbtn" style="width:auto;margin-top:10px;padding-left:20px">
					CONTINUE &nbsp; <i class="fa fa-arrow-circle-right" style="margin-left: -5px;margin-right: 8px;" aria-hidden="true" title="Deposit funds"></i>
				</button>
				<div style="width:240px; margin:10px auto; font-size:13px">
					<img src="img/lock-03.png" alt="lock" style="float:left" title="Payment from <?= $code ?> in <?= $currency ?>">
					All transactions are guaranteed,<br/>safe and secured.
				</div>
				<span class="transaction_note">*This transaction will be added to your account in <span class="symbol" data-symbol="USD"></span></span>
            </td>
        </tr>
	</tbody>
	</table>
</div>
</form>

<link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
<script type="text/javascript" src="<?=BASE_URL?>assets/js/jquery.validate.js?v=<?= JSVERSION ?>"></script>
<script src="js/intlTelInput.min.js?v=<?= JSVERSION ?>"></script>
<script type="text/javascript" src="js/common.min.js?v=<?= JSVERSION ?>"></script>
<script>
var amountV=0;
$("#flutterbankFbtn").click(function (e) {
	var _v=parseFloat(parent.AMOUNT);
	var _f=$("#flutterbankF")[0];
	var _r=parseFloat(_f.fw_rate.value);
	amountV=_f.amountV.value=_v;
	_f.amount.value=_r * _v;
});
function markStartPay() {
	// mark "user started a payment"
	$.ajax({
		url: "/ajax/index.php",
		type: "POST",
		data: { action: "payment-start", method: "FW-bank", amount: amountV }
	});
}
</script>
</body>
</html>
