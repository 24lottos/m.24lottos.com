<?php
/**
* deposit-flutterwave-form.php - FlutterWave Response Message, mobile version
* used in /app/src/payment/PFlutterwave.php
*
* @author     Sergey V Musenko <sergey@musenko.com>
* @date       2017-01-12 22:29:09 EET
* @version    0.2
*/
echo '<!DOCTYPE html><html lang="en"><head>
<meta http-equiv="expires" content="Sun, 01 Jan 2017 00:00:00 GMT"/>';
require(SITE_DIR.'/inc/header_head.php');

$output = "
<style>
	.msg{border-radius:5px;padding:5px;text-align:left;font-family:Lato,arial;font-size:20px;font-weight:300}
	.msg.warning{background-color:#fbe3b4;border:1px solid #cd8806;color:#cd8806}
</style>".
( GAENABLED ?
"<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';
j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TRKHWV');</script>
<!-- End Google Tag Manager -->" 
:'' ).
"</head><body>".
( GAENABLED ?
"<!-- Google Tag Manager (noscript) -->
<noscript><iframe src='https://www.googletagmanager.com/ns.html?id=GTM-TRKHWV' height='0' width='0' style='display:none;visibility:hidden'></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->" 
:'' );

if( $status == 'error' ) { // Error ---
	echo $output.( GAENABLED ?
'<script>
	dataLayer.push({
		"event": "transaction",
		"eventCategory": "deposit",
		"eventAction": "purchase'.(isset($_SESSION['FIRST_DEPOSIT']) ? ' first time':'' ).'",
		"eventLabel": "error - {'.$depositData.'}"
	});
</script>'
:'' ).'
<div class="msg warning" style="margin:20px 0;font-size:16px;color:#333">
	<div class="fa fa-warning"></div>
	Error: '.$message.'
</div>';

	$country_select = '<option>--</option>';
	$geoIPCountryName = strtolower(getCountryCode()->code);
	$oGen = $_db->query("SELECT countryName as name, countryCode as code FROM callingcodes ORDER BY countryName");
	while( $oGen->Record=$oGen->fetch( PDO::FETCH_OBJ ) ) {
		$_name = $oGen->Record->name;
		$_code = strtolower($oGen->Record->code);
		$_code = $_code=='gb' ? 'uk':$_code;
		$selected =  $geoIPCountryName==$_code ? ' selected="selected"':'';
		$country_select .= "<option value='$_code'$selected> $_name </option>";
	}
	include(BASE_PATH.'inc/deposit-country.php');


} else { // success ---
	echo $output.( GAENABLED ?
'<script>
dataLayer.push({
	"event": "transaction",
	"eventCategory":"deposit",
	"eventAction": "purchase'.(isset($_SESSION['FIRST_DEPOSIT']) ? ' first time':'' ).'",
//	"eventValue": "'.$depositData['amount'].'",
	"eventLabel":"success, FlutterWave, mobile",
	"ecommerce": {
		"purchase": {
			"actionField": {
				"id": "'.$depositData['payment_id'].'",
				"affiliation": "site",
				//"revenue": "'.$depositData['amount'].'",
				"coupon": ""
			},
			"products": [{
				"name": "Deposit",
				"category": "Deposit",
				"quantity": 1,
				"price": "'.$depositData['amount'].'"
			}]
		}
	}
});
</script>'
:'' ).'
<div class="msg success" style="margin:20px 0;font-size:16px;color:#333">
	<div class="fa fa-check-square"></div>
	'.$message.'
</div>
<script> parent.balReload() </script>
<p>'.CLICK2CONTINUE.'</p>
<p><button class="green-btn padding10" style="width:auto;margin-top:10px" onclick="this.disabled=true;parent.location.href=\'/vcheckout\'">
	Continue &nbsp;<i class="fa fa-arrow-circle-right"></i>
</button></p>';
}

?>

<link rel="stylesheet" type="text/css" href="<?= $site->baseURLM('css/font-awesome.min.css?v='.CSSVERSION) ?>">
</body></html>
