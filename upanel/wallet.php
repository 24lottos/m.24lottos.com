<section>
	<!--div class="banner"></div-->
	<div class="row">
		<h5 class="blue pull-left nomargin">Wallet</h5>
		<h5 class="blue pull-right nomargin">Current Balance: $<?= number_format($oUser->getCurrentBalance(),2) ?></h5>
	</div>
    <div class="row">
        <?php if( $oUser->getCurrentBalance() ): ?>
        <div class="col-sm-6" style="width: 50%;float: left;text-align: right;padding-right: 20px;">
            <button class="green-btn padding10 link" data-link="<?= $site->baseURLm( 'account/deposit' ) ?>">
                Deposit &nbsp;<i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
            </button>
        </div>
        <div class="col-sm-6" style="width: 50%;float: left;padding-left: 20px;">
            <button class="blue-btn withdraw-btn padding10 link" style='width:auto; border: none;' data-link="<?= $site->baseURLm( 'account/withdraw' ) ?>">
                Withdraw &nbsp;<i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
            </button>
        </div>
        <?php else: ?>
            <div class="col-sm-12" style="text-align: center;">
                <button class="green-btn padding10 link" data-link="<?= $site->baseURLm( 'account/deposit' ) ?>">
                    Deposit &nbsp;<i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
                </button>
            </div>
        <?php endif; ?>
        <div style="clear:both"></div>
    </div>
    <div class="row">
        <div class="centered" id="trustWallet">
            <img src="<?= $site->baseURL( 'images/lock-03.png' ) ?>" alt="lock">
            All transactions are guaranteed, <br>safe and secured.
        </div>
        <div class="centered" id="trustWalletIcons">
            <img alt="Trust & Safety 24Lottos.com" src="<?= $site->baseURL( 'assets/img/trustnsafety1.png' ) ?>">
        </div>
    </div>
</section>
<style>
    #trustWallet {
        margin-top: 20px;
        font-weight: bold;
        color: #32599e;
    }
    #trustWallet img{
        margin: -5px 5px 0 0;
        vertical-align: middle;
    }
    #trustWalletIcons {
        margin: 25px 0;
        height: 87px;
        padding-top: 10px;
    }
</style>