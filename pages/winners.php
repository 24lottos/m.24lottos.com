<?php
$_SESSION['firs_winners_view'] = true;
?>
<script>var numRows = 10;</script>
<style>
    .pagination ul { box-shadow: none !important; font-size: 12px; margin-left: 0!important; margin-right: 0!important;  padding: 0!important;}
    #winnerList { min-width: 304px; width: 100% !important }
    #winnerList th, #winnerList td { text-align: left; padding: 5px 0; font-size: 12px; }
    #winnerList td { font-size: 14px; }
    /*#winnerList td:last-child { text-align: right; padding-right:26px; }*/
    .pagination ul > li > a, .pagination ul > li > span {
        padding: 4px 8px !important;
    }
    @media screen and (min-width: 414px){
        #winnerList th, #winnerList td { font-size: 15px; }
        .pagination ul { font-size: 15px; }
    }
    @media screen and (min-width: 480px){
        #winnerList th, #winnerList td { font-size: 16px; }
        .pagination ul { font-size: 16px; }
    }
</style>
<section id="msection">
	<!-- blue header Html Here -->
	<article>
		<div class="blue-header">
			<div class="center">
				<span class="white font11">Hall of Fame - Lottery Winners</span>
			</div>
		</div>
		<!-- blue header Html End Here -->
		<!-- Table  str Html Here -->
		<div class="table winner nomargin">
			<div class="table-responsive" style='width:100%;overflow-x:auto'>
				<br>
				<table class="table table-striped" id="winnerList" cellspacing="0">
					<thead>
					<tr>
						<th style="background-color:#3a599c !important;color:#fff">Lottery</th>
						<th style="background-color:#3a599c !important;color:#fff">Date</th>
						<th style="background-color:#3a599c !important;color:#fff">Winner</th>
						<th style="background-color:#3a599c !important;color:#fff">Amount</th>
					</tr>
					</thead>
					<tbody style="font-weight: 500;color: #333;">
					</tbody>
				</table>
			</div>
		</div>
	</article>
	<!-- Table  str Html Here -->
</section>