<h5><strong>Latest Lottery Winners</strong></h5>
<div class="table winner nomargin">
    <div class="table-responsive" style="width:100%;overflow-x:auto">
        <table class="table table-striped table_ticket yourLotteryPicks">
            <thead>
            <tr>
                <th class="centered">Lottery</th>
                <th class="centered">Winner</th>
                <th width='5%'>Amount&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $oLottery = new Lottery;
            $oLottery->winners( 0, 10 );
            while( $oLottery->nextRecord() ):?>
                <tr>
                    <td><?= $oLottery->Record->gameName ?></td>
                    <td><?= shortName( ucwords( substr( $oLottery->Record->email, 0, strpos( $oLottery->Record->email, "@" ) ) ), 2, 3 ) ?></td>
                    <td><?= "$".$oLottery->Record->winningAmtInUSD ?>&nbsp;</td>
                </tr>
            <?php endwhile; ?>
            </tbody>
        </table>
    </div>
</div>
<style>
    .table_ticket tr td {
        padding: 5px 2px;
    }
</style>