<?php

$oPromo = new Promotion;
$oPromo->view( "WHERE status>0 order by `order`" );
?>
<div class="row">
	<div class="col-lg-8">
		<article class='promolist'>
			<h5 class="pageHeading"><strong>Special Offers For You</strong></h5>
<?php
if( $oPromo->numRows() ) {
	while( $oPromo->nextRecord() ) {
	 	$url = $oPromo->Record->url;
	 	$hideBtn = strstr($url,'lottery/') ? 'hidden':'';
	 	if( !empty($url) ) { ?>
			<div class='promoitem' onclick="location.href='<?= $url ?>'">
		<?php } else { ?>
			<div class='promoitem' style='opacity:.5'>
		<?php }?>
				<img src="<?= $site->baseURL( "images/promotion/{$oPromo->Record->logo}" ) ?>"
					title="<?= $oPromo->Record->promotionName ?>" alt="<?= $oPromo->Record->promotionName ?>" />
				<h3><?= $oPromo->Record->promotionName ?></h3>
				<?= $oPromo->Record->description ?>
				<div align='center'>
				<?php /*
				<button type="button" class="btn2 btn-green <?= $hideBtn ?>"
					id="playLottery">Play Now &nbsp;<i class="fa fa-arrow-circle-right"></i>
				</button>
				*/ ?>
				</div>
			</div>
			<div class='clearfix'></div>
	<?php
	}

} else { ?>
	<p>No promotion offers found at the moment. Please come back later.</p>
<?php } ?>

		</article>
	</div>

</div>


