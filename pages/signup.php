<?php

use L24\HybridAuth\HybridUser;

if ($oUser->loginCheck()) {
	header('location:' . $site->baseURLm('account/dashboard'));
	exit();
}
$pwdPlaceHdr = 'Password, 6-30 characters';
?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<style>
	footer { display: none; }
	.rc-anchor-logo-portrait { margin-left: 0; 	}
	.rc-anchor-normal { width: 280px !important; }
</style>

<section id="msection" class="signup">
	<div class="signup-header" style='border:none;margin-top:0'>
		<i class="fa fa-sign-in" aria-hidden="true"></i>
		<h1>Open Your FREE Account</h1>
	</div>
	<div style='text-align: center; clear:both; border-top:1px solid #d6d6d6; height:10px;'></div>
<?php
if( FBGP_LOGIN ) { /* -- Social Buttons Login -- */?>
	<div class="social-login">
		<a href="<?= $site->baseURLm("signup/facebook") ?>" class="facebook_button">
			<i class="fa fa-facebook"></i> Facebook
		</a>
		<a href="<?= $site->baseURLm("signup/google") ?>" class="google_button">
			<i class="fa fa-google-plus"></i> Google+
		</a>
		<div class="clearfix"></div>
	</div>
<?php }

$showTokenBox = false;
if (isset($_POST['btnAccountAdd']) || isset($_GET['social'])) { // registration!
	echo '<br/><br/>';
	$registration_error = null; // assume OK

	if( isset($_GET['social']) ) { //Social registration
		$provider = $_GET['social'];
		try {
			switch ($provider) {
				case 'facebook':
					$provider = HybridUser::FACEBOOK;
					break;
				case 'google':
					$provider = HybridUser::GOOGLE;
					break;
//				case 'twitter':
//					$provider = HybridUser::TWITTER;
//					break;
				default:
					throw  new Exception("Unsupported social network");
			}
			$u = new HybridUser();
			$adapter = $u->authenticate($provider);
			if( !$adapter->isUserConnected() ) {
				throw new Exception("Could not register with " . $provider);
			}
			$uid = $u->map_user($provider);
			if( null !== $uid ) {
				//user already registered, do login and redirect
				$_SESSION['showSignupGreeting'] = true;
				header('Location: /login/'.$_GET['social']);
				exit();
			}
			//mimic usual registration
			if( null === $profile = $u->getUserProfile($provider) ) {
				throw new Exception("Could not access $provider profile");
			};
			if( empty($profile->email) ) {
				//logoff from provider so data will not be cached
				throw new Exception("Email is required, please confirm email in $provider profile");
			}
			$u->fill_signup($profile);
			if( false !== $registration_error = $oUser->register() ) {
				throw new Exception($registration_error);
			}
			$u->save_profile($provider, $oUser->Record->id, false);

		} catch (\Exception $e) {
			$registration_error = $e->getMessage();
			$u->logout($provider);
			session_destroy();
		}

	} else { // email registration
		/*
		$recaptcha_request_data = array( 'secret' => RECAPTCHA_SECRET_KEY, 'response' => $_POST['g-recaptcha-response'] );
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, RECAPTCHA_API_URL);
		curl_setopt($ch,CURLOPT_CUSTOMREQUEST,'POST');
		// curl_setopt($ch,CURLOPT_HTTPHEADER, array('SG-API-KEY: ' .$this->sg_api_key, 'Content-type: application/json'));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $recaptcha_request_data);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		$recaptcha_response = json_decode( $response );
		if ( $recaptcha_response->success && $recaptcha_response->hostname == $_SERVER['SERVER_NAME'] ) {
		  $_POST['login_user'] = $_POST['email'];
		  $_POST['login_pass'] = $_POST['password'];
		  $_POST['name'] = ucwords($_POST['name']);
		  $_POST['gender'] = ucfirst($_POST['gender']);
		  $registration_error = $oUser->register();
		} else {
		  $registration_error = 'Incorect reCaptcha value!';
		}
		*/
		$_POST['login_user'] = $_POST['email'];
		$_POST['login_pass'] = $_POST['password'];
		$_POST['name'] = ucwords($_POST['name']);
		$_POST['gender'] = ucfirst($_POST['gender']);
		$registration_error = $oUser->register();
	}

	if ($registration_error) {
		echo '<div class="alert register-error"><span>' . $registration_error . '</span></div>';

	} else {
		switch ($_SESSION['registration_status']) {
			case 'user_inactive':
				echo '<div class="msg warning">' . USER_INACTIVE . '</div>';
				if (GAENABLED) { ?>
				<script>
					dataLayer.push({
						'event': 'lottos',
						'eventCategory': 'authorization',
						'eventAction': 'sign up',
						'eventLabel': 'fail'
					});
				</script>
				<?php }
				break;

			case 'registration_successful':
				//  $oUser->registerationSMS($oUser);
				//echo '<div class="msg success">'.REGISTRATION_SUCCESSFUL.'</div><br />';
				$oUser->setUserToVerify($_POST['email']);
				$oUser->welcomeSMS($oUser);
				if ($oUser->login(true, false, true)) {
					// affiliate-pro - run it from a last line!
					if (AFFPRO_PATH) {
						$_include = AFFPRO_PATH . AFFPRO_TRACK_REGIS;
						if (is_file($_include)) @include_once($_include);
					}
					$_SESSION['showSignupGreeting'] = true;
					$_SESSION['loginStatus'] = 'signup';
					setcookie( 'FILLPROFILEMSG', '1', time() + 365*24*3600, '/' ); // set cookie to show msg in /account
					if ((new Bet)->isCartFull()) {
						$_SESSION['NO_ENOUGH_MONEY'] = 1;
						header('Location: /account/deposit'); // '/account/tickets'
					} else header('Location: /');
					exit;
				}
				break;

			case 'default':
				echo '<div class="msg danger">' . ERROR_MSG . '</div>';
				if (GAENABLED) { ?>
				<script>
					dataLayer.push({
						'event': 'lottos',
						'eventCategory': 'authorization',
						'eventAction': 'sign up',
						'eventLabel': 'fail'
					});
				</script>
				<?php }
				break;
		}
		$showTokenBox = true;
	}
}

$error_message = '';
if (isset($_POST['btnAccountToken'])) {
	$result = $oUser->activate(true);
	if ($result) {
		echo '<div class="error-msg"><span>' . $result . '</span></div> ';
		$showTokenBox = true;
	} else {
		header('location:' . $site->baseURLm('login'));
	}
}
if ($oUser->loginCheck()) {
	header('location:' . $site->baseURLm('account/dashboard'));
}
if ($showTokenBox) echo '<br />';
else { ?>
	<div class="msg warning" id='form_msg'
		 style="display:none;max-width:290px;margin:0 auto;font-size:14px;color:#333"
		 onclick='this.style.display="none"'>
		<div class="fa fa-warning"></div>&nbsp; <span></span>
	</div>
	<form role="form" method="post" id="frmAccountAdd"
		  onsubmit="return checkCountry();">
		<input type="hidden" name="name" value=""/>
		<input type="hidden" name="gender" value=""/>
		<input type="hidden" name="preferred_currency" value="<?= $site->country->currencyCode ?>"/>
		<input type="hidden" name="countrycode" value="<?= strtolower($site->country->code) ?>"/>
		<input type="hidden" name="country" value="<?= $site->country->name ?>"/>
		<input type="hidden" name="session" value="<?= session_id(); ?>"/>
		<div class="form-group">
			<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
			<div class='m40'>
				<input type="email" class="form-control required" name="email" id="email" maxlength="96"
					   onblur="validateSignup(this)"
					   placeholder="Email"
					   pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"/>
			</div>
		</div>
		<div class="form-group">
			<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
			<div class='m40'>
				<input type="password" class="form-control required" name="password" id="password" maxlength="30"
					   onblur="validateSignup(this)"
					   placeholder="Password 6-30 characters">
			</div>
		</div>
		<div class="form-group">
			<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
			<div class='m40'>
				<input type="password" class="form-control required" name="repassword" id="password2" maxlength="30"
					   onblur="validateSignup(this)"
					   placeholder="Confirm Password">
			</div>
		</div>
		<!--
		<div class="row-fluid form-group" style="width: 290px;">
			<div class="input-wrap" style="width: 156px; margin: 0 auto;">
				<div class="g-recaptcha" data-sitekey="<?/*= RECAPTCHA_KEY */?>" data-size="compact" style="margin: 0 auto;"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		-->
		<div class="form-group signup">
			<input type="hidden" name="btnAccountAdd" value="dummy"/>
			<button type="submit" name="btnAccountAdd" class="btn btn-green" id="btnAccountAdd"
					onclick="validateSignup(this)"
					style='width:242px;border:none;height:38px;line-height:38px;padding:0'>
				Sign Up &nbsp;<i class="fa fa-arrow-circle-right"></i>
			</button>
		</div>
	</form>
<?php } ?>
	<div class="resend">
		<?php if ($showTokenBox) { ?>
			<br/><h6><span id="register-resend-token">Not received the Verification Link? <br/> <a
							href="javascript:resendActivationToken();">Click here to resend verification link.</a></span>
			</h6><br/>
		<?php } ?>
		<h6>Already registered? <a href="<?= $site->baseURLm('login') ?>">Click here</a> to log in</h6>
		<br/>
		<div style="max-width:320px; margin:0 auto; padding-bottom:20px; text-align:center; font-size:13px; color:#999">
			By clicking Sign Up you agree that you have read and accepted the
			<a href="<?= $site->baseURLm('terms-and-conditions') ?>" class="orange">Terms Of Use</a>
			and <a href="<?= $site->baseURLm('privacy-policy') ?>" class="orange">Privacy Policy</a>
		</div>
	</div>
</section>

<script type="text/javascript">
var view_name = "signupm";
function validateSignup(obj, event) {
	$('#form_msg').fadeOut();
	var f = obj.form, fld = obj.name, msg = false;
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if(fld=='email') {
		if(f.email.value.length>0 && !re.test(f.email.value)) { msg='Please type correct Email Address'; obj.select(); }
	} else if(fld=='password') {
		if(f.password.value.length>0 && f.password.value.length<6) { msg='Please type <?= $pwdPlaceHdr ?>'; obj.select(); }
		else if(f.repassword.value.length>0 && f.password.value!=f.repassword.value) { f.repassword.value=''; msg='Please confirm your Password'; f.repassword.focus(); }
	} else if(fld=='repassword') {
		if(!f.password.value.length && f.repassword.value.length) { f.repassword.value=''; msg='Please type a Password'; f.password.focus(); }
		if(f.repassword.value.length>0 && f.password.value!=f.repassword.value) { msg='Password does not match'; obj.select(); }
//	} else if( $('#g-recaptcha-response').val() == '' ) {
//		msg = 'Please check the reCaptcha!';
	} else if( re.test(f.email.value) && f.password.value.length>=6 && f.password.value==f.repassword.value ) {
		f.submit();
	} else {
		return false;
		//msg='Please fill all fieds!';
	}
	if( msg) {
		$('#form_msg span').html(msg);
		$('#form_msg').fadeIn();

		event = event || window.event;
		if ( event.preventDefault() ) {
			event.preventDefault();
		} else {
			event.returnValue = false;
		}
	} else {
		$('#form_msg').fadeOut();
		return true;
	}
}
function checkCountry() {
	if (visitorCountry == 'US') { // see footer
		alert("We do not support users from USA at the moment!");
		return false;
	}
}
</script>
<style>
	.rc-anchor-logo-portrait { margin-left: 0; }
	.rc-anchor-normal { width: 280px !important; }
</style>
