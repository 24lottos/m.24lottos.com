<?php
$o = new Lottery;
$tLotteries = $o->fetchLotteryDetails(true);

?>
<section id="msection">
	<!-- blue header Html Here -->
	<article itemscope itemtype="http://schema.org/ItemList">
		<link itemprop="url" href="https://m.24lottos.com/results">
		<div class="blue-header">
			<div class="center">
				<h1 class="white font11" itemprop="name">Lottery Results</h1>
			</div>
		</div>
		<!-- blue header Html End Here -->
		<!-- Table  str Html Here -->
		<div class="table result">
			<div>
				<div class="table-heading Lottary">Lottery</div>
				<div class="table-heading Date">Date</div>
				<div class="table-heading Result">Result</div>
			</div>
			<?php
			$count = 0;
			?>
			<?php
			$oLotteryResultsArchive = new Lottery();
			$qLotteryResultsArchive = "
				SELECT lotto_rrc_last_results.*
				  FROM lotto_rrc_last_results
				  ORDER BY lotto_rrc_last_results.datetime DESC";
			try {
				$oLotteryResultsArchive->query($qLotteryResultsArchive);
			} catch (ErrorException $message) {}

			$has_archive = ( $oLotteryResultsArchive->numRows() && !isset($message) ) ? true : false;
			if ( $has_archive ) {
				global $lotteries_names_matching;
				while( $oLotteryResultsArchive->nextRecord() ):
					$GameName = $lotteries_names_matching[$oLotteryResultsArchive->Record->lotteryname];

					if( !array_key_exists( $GameName, $tLotteries ) ) continue;

					$_url = $tLotteries[$GameName]['alias'];
					?>
					<div class="table-inner" id="play_it_<?= $count ?>" onclick='location.href="<?= $site->baseURLM( 'lottery/'.$_url .'/results' ) ?>"'>
						<div class="table-data Lottary" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<div class="logo">
								<img src="<?= $site->baseURL( 'images/lottery_logos/small_logos/'.$tLotteries[$GameName]['Small_Image_url'] ) ?>"
									 class="img-responsive" alt="<?= $tLotteries[$GameName]['DisplayName'] ?>" title="<?= $tLotteries[$GameName]['DisplayName'] ?>"/>
							</div>
							<a class="logo-dec" href="<?= $site->baseURLM( 'lottery/'.$_url .'/results' ) ?>" style="cursor:default;">
								<meta itemprop="name" content="<?=$tLotteries[$GameName]['DisplayName']?>" />
								<?= $tLotteries[$GameName]['DisplayName'] ?>
							</a>
							<meta itemprop="url" content="<?= $site->baseURLM( 'lottery/'.$_url .'/results' ) ?>" />
							<meta itemprop="position" content="<?=$count+1?>" />
						</div>
						<?php
						$winnumbers = json_decode( $oLotteryResultsArchive->Record->winnumbers );
						$current_lottery_day = new \DateTime( 'now', new \DateTimeZone( $oLotteryResultsArchive->Record->timezone ) );
						$current_lottery_day->setTime( 0, 0, 0 );

						$draw_date = new \DateTime( $oLotteryResultsArchive->Record->datetime, new \DateTimeZone( $oLotteryResultsArchive->Record->timezone ) );
						$draw_date->setTime( 0, 0, 0 );

						$diff = $current_lottery_day->diff( $draw_date );
						$diffDays = intval( $diff->format( "%R%a" ) );

						$relative_to_today = '';
						switch( $diffDays ){
							case 0: $relative_to_today = 'Today, '; break;
							case -1: $relative_to_today = 'Yesterday, '; break;
							default: $relative_to_today = ''; break;
						}

						$draw_date = date( 'd M', strtotime( $oLotteryResultsArchive->Record->datetime ) );
						?>
						<div class="table-data Date"><?= $relative_to_today .$draw_date ?></div>
						<div class="table-data Result">
							<div>
								<?php foreach( $winnumbers->numbers as $number ): ?>
									<div class="red-circle">
										<span class="white"><?= $number ?></span>
									</div>
								<?php endforeach; ?>
								<?php foreach( $winnumbers->numbers_extra as $number ): ?>
									<div class="red-circle" style="background:#666">
										<span class="white"><?= $number ?></span>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>

				<?php
					$count++;
					endwhile; ?>
				<?php
			}
			?>
		</div>
		<!-- Table  str Html Here -->
	</article>
</section>
