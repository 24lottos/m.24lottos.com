<link rel="stylesheet" type="text/css" href="css/swiper.css"/>
<section id="msection">
    <div id="how-to-menu">
        <ul>
            <li class="how-to-menu-item">
                <a href="<?= $site->baseURLM( 'how-to-play' ) ?>">How to Play</a>
            </li>
            <li>|</li>
            <li class="how-to-menu-item active">
                How to Fund
            </li>
        </ul>
    </div>
    <div class="swiper-container" style='background:#fff'>
        <div class="swiper-wrapper">
            <?php $count = 1;
            $oPage = new Page;
            $oPage->listHowToPlay( 0, 'how_to_fund' );
            while( $oPage->nextRecord() ): ?>
                <!-- slider 1 -->
                <div class="swiper-slide">
                    <div class="slider-des">
                        <div class="blue-circle"><span><?= $count++ ?></span></div>
                        <div class="howtoplayheading"><?= htmlspecialchars_decode( $oPage->Record->title ) ?></div>
                    </div>
                    <div class="slider-img">
                        <img class="img-responsive"
                             src="<?= $site->baseURL( "images/howitworks/{$oPage->Record->image_for_mobile}" ) ?>"
                             alt="<?= htmlspecialchars_decode( $oPage->Record->title ) ?>">
                    </div>
                </div>
            <?php endwhile; ?>
        </div><!-- swiper-wrapper -->

        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>

    </div>
    <div style="margin-top: 10px;text-align: center">
        <button class="btn2 btn-green swiper-button-nexte" style="font-weight: bold; width:149px;" type="button">Next</button>
    </div>
    <br/>
</section>

<script src="js/swiper.js"></script>
<script>
    var window_width = $(window).width();
    var swiper = new Swiper('.swiper-container', {
        width: window_width,
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-nexte,.swiper-button-next',
        prevButton: '.swiper-button-preve,.swiper-button-prev',
        slidesPerView: 1,
        centeredSlides: true,
        paginationClickable: true,
        loop: true
    });
</script>
