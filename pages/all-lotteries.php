<?php
$oLotteryList = new Lottery;
$order = isset($_GET['o']) ? $_GET['o']:null;
$tLotteries = $oLotteryList->fetchLotteryDetails();
$rUpcomingLotteries = $oLotteryList->upcomingLotteries();

if( GAENABLED  ) {
	$position = 0;
	$impressions = [ ];
	foreach( $rUpcomingLotteries as $t ) {
		if( !$t['NextPurchaseDeadline'] || !$t['CurrentJackpot'] ) continue;
		$impressions[] = [
			'name' => $t['GameName'],
			'brand' => "".$t['CurrentJackpot']."",
			'category' => 'numbers',
			'position' => $position++,
			'list' => 'mainPage',
		];
	}
?>
<script>
	dataLayer.push({
		'pageType': '<?=$p?>',
		'auth': '<?= $oUser->loginCheck() ? 1:0 ?>',
		'userId': '<?= $oUser->getID() ?>',
		'depositTotal': '<?= $oUser->getCurrentBalance() ?>',
		'ecommerce': {
			'currencyCode': '<?=$site->country->currencyCode?>',
			'impressions': <?= json_encode( $impressions ) ?>  ,
		}
	});
</script>
<?php } ?>

<section id="msection">
	<!-- Table  str Html Here -->

	<article>
		<div class="table winner">
			<div>
				<div class="table-heading Lottary">Game</div>
				<div class="table-heading Date">Jackpot</div>
				<div class="table-heading Winner">Date</div>
				<div class="table-heading Amount">&nbsp;</div>
			</div>
			<?php
			$count = 0;
			foreach( $rUpcomingLotteries as $t ):
				if( !$t['NextPurchaseDeadline'] || !$t['CurrentJackpot'] ) continue;
				$timeToDraw = $t['NextPurchaseDeadline'];

				if( !array_key_exists( (string)$t['GameName'], $tLotteries ) ) continue;
				?>
				<div class="table-inner" id="play_it_<?= $count ?>">
					<div class="table-data Lottary">
						<div class="logo">
							<img src="<?= $site->baseURL( 'images/lottery_logos/small_logos/'.$tLotteries[$t['GameName']]['Small_Image_url'] ) ?>" alt="img"/>
						</div>
					</div>
					<div
						class="table-data Date"><?= getCurrencySign( $t['JackpotCurrency'] ) ?> <?= currency_compressor( $t['CurrentJackpot'] ) ?></div>
					<div class="table-data Winner"
						 data-countdown="<?= date( 'Y/m/d', strtotime( $t['NextDraw'] ) ) ?>"><?= date( 'd M', strtotime( $t['NextDraw'] ) ) ?></div>
					<div class="table-data Winner">
						<button class="green-btn"><div>PLAY</div></button>
					</div>
					<div class="logo-dec">
						<?php $game = explode( '-', $tLotteries[$t['GameName']]['DisplayName'] ) ?>
							<span><?= $game[0] ?>-</span><?= $game[1] ?>
					</div>
					<script>
						$('#play_it_<?=$count?>').click(function () {
<?php if( GAENABLED ): ?>
							dataLayer.push({
								'event': 'lottos',
								'eventCategory': 'numbers',
								'eventAction': 'click',
								'eventLabel': 'play',
								'ecommerce': {
									'click': {
										'actionField': { "list": "mainPage" }, // list name
										'products': [{
											'name': '<?=$tLotteries[$t['GameName']]['DisplayName']?>', //loterry name
											'brand': '<?=$t['CurrentJackpot']?>', // Jackpot value in USD
											'category': 'numbers', //for numbers always =numbers
										}]
									}
								}
							});
<?php endif; ?>
							$('#loading-overlay').show(0);
							location.href="<?= $site->baseURLm( 'lottery/'.stringURLSafe( $t['GameName'] ) ) ?>";
						});
					</script>
				</div>
				<?php $count++; endforeach; ?>

		</div>
	</article>
	<!-- Table  str Html Here -->
</section>
