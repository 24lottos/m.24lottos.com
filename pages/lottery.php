<?php
$moreOpened = true; // more info - opened by default

$lotteryName = trim($_GET['l']);

$_Lotteries = $oLottery->upcomingLotteries( null, 'all' );
$lotteryFound = false;
foreach( $_Lotteries as $t ) {
	if( stringURLSafe($t['alias'])==$lotteryName ) {
		$sLottery = $t['GameName'];
		$sLotteryID = $t['GameId'];
		$sLotteryJackPot = $t['CurrentJackpot'];
		$sLotteryJackPotCurrency = $t['JackpotCurrency'];
		$sTimeToDraw = $t['NextPurchaseDeadline'];
		$sTimeNow    = gmdate( 'Y-m-d' );
		$isOmitted   = $t['isOmitted'];
		$sTimeToDraw = $isOmitted || $sTimeToDraw[0]==0 ? $sTimeNow : $sTimeToDraw;
		$lotteryFound = true;
	}
}
if( !$lotteryFound ) {
	header( 'location:'.$site->baseURL( "error" ) );
	exit();
}
unset($_Lotteries);
$oLottery->getDetails( $sLottery );

$jp = '<span class="symbol" data-symbol="' .getCurrencySign( $sLotteryJackPotCurrency ) .'"></span>' .number_format($sLotteryJackPot);

// hide this lottery?
$NotAvailabe = !ENVIRONMENT ? 0 : $sTimeToDraw<=$sTimeNow || !(int)$sLotteryJackPot;

//echo '<h1 style="color:#35589c; font-size: 1.5em;">Buy Official Lottery Tickets For - '.$oLottery->Record->lotteryNameModified.'</h1>';
?>
<style>
	html { padding-bottom: 60px; }
	footer { float: left; position: relative; }
</style>

<div style="display:none" class="row padding featured_lottery curlottery"
	id="featured_lottery_<?= $sLotteryID ?>"
	data-balls="<?= $oLottery->Record->Balls ?>"
	data-extraballs="<?= $oLottery->Record->Extra_Balls ?>"
	data-ballsmaxnum="<?= $oLottery->Record->Balls_max_number ?>"
	data-extraballsmaxnum="<?= $oLottery->Record->Extra_Ball_max_number ?>"
	data-price="<?= $oLottery->Record->PRICE ?>"
	data-currency="<?= $oLottery->Record->Currency ?>"
	data-pdtid="<?= $oLottery->Record->PDTID ?>"
	data-jackpot-encoded="<?= base64_encode( (double)$sLotteryJackPot ); ?>"
	data-gamename="<?= $sLottery ?>"
	data-jackpot="<?= getCurrencySign( $sLotteryJackPotCurrency ).number_format( (double)$sLotteryJackPot, 0 ); ?>"
>
	<div class="col-lg-4">
		<div class="jackpot"><span class="symbol" data-symbol="<?= getCurrencySign( $sLotteryJackPotCurrency ) ?>"></span> <?= number_format( $sLotteryJackPot ) ?></div>
		<div class="game"><a><?= $oLottery->Record->lotteryNameModified ?></a></div>
	</div>
</div>

<div class="row">
	<div class="col-lg-8">
		<article itemscope itemtype="http://schema.org/Product">
			<div class="select-box btn-blue">
				<div>
					<div id="lottery_select_name">
						<div class="logo-dec">
							<img class="logo" src="<?= $site->baseURL('images/lottery_logos/small_logos/'.$oLottery->Record->Small_Image_url)?>" alt="<?= $oLottery->Record->lotteryNameModified; ?>" /><br/>
							<h1 itemprop="name"><?= $oLottery->Record->lotteryNameModified; ?></h1>
						</div>
						<div class="jp-dec">
                            <i><span style="position:  relative;
                                            background:  none;
                                            padding-left: 0;
                                            font-size: 20px;
                                            bottom: 0;
                                            left: 0;"
                                     class="symbol" data-symbol="<?= getCurrencySign( $sLotteryJackPotCurrency ) ?>"></span> <?= number_format( $sLotteryJackPot, 0 ) ?></i>
							<span data-cntdwn="<?= $sTimeToDraw ?>"></span>
						</div>
					</div>
				</div>
			</div>
			<div style="clear: both;"></div>
		</article>

		<div id='numbersWrap' class="game_wrapper"><!-- we can keep it -->
			<?php if( $NotAvailabe ) { ?>
			<div class="not_available" onclick='location.href="<?= $site->baseURLm() ?>"'>
				<div class="not_available_msg" style='cursor:pointer'>Sorry, Not available<br/><span>Click to <u>PLAY</u> other lotteries</span></div>
			</div>
			<?php } else { ?>
			<div class="btn-section v-gradient-white">
				<button type="button" id="playLottery" <?= $NotAvailabe ? 'disabled="disabled"':'' ?>>
					<div class='shine'>PLAY &nbsp;<i class="fa fa-arrow-circle-right"></i></div>
				</button>
			</div>
			<?php } ?>

			<div class='balls_box' onclick='$(".btn3").css("opacity","1")'>
				<div class='regular_box'>
					<button type="button" class="btn3 btn-warning" id="autoPick">iForecast</button>
					<div id="regular_picks" class="col-lg-10 white"></div>
					<div style="clear: both;"></div>
					<div id="regular_balls" class="col-lg-12"></div>
					<div style="clear: both;"></div>
				</div>
				<div class='extra_box'>
					<div id="extra_picks" class="col-lg-10 white"></div>
					<div style="clear: both;"></div>
					<div id="extra_balls" class="col-lg-12"></div>
				</div>
				<div style="clear: both;"></div>
			</div>
			<div id="selected_balls" class="col-lg-5 col-lg-offset-3"></div>
			<div style="clear: both;"></div>
		</div>

		<div id="yourLotteryPicksTableContainer">
			<?php
			UI::packages_tables( 'pending', 'Confirm your Packages' );
			UI::tickets_tables( 'pending', 'Confirm your Tickets', 'mobile' );
			?>
		</div>

		<div class="row<?= $moreOpened ? ' hidden':'' ?>">
			<div class="pull-right">
				<button class="green-btn" id="viewMoreBtn">More Info</button>
			</div>
		</div>
		<div style="clear: both;"></div>
		<br/>

		<div class="row" id="viewMoreDiv"<?= $moreOpened ? '':' style="display:none"' ?>>
			<?php
			if( isset($oLottery->Record->howToPlay) ) include('inc/lottery-howtoplay.php');
			if( isset($oLottery->Record->prizeBreakdown) ) include('inc/lottery-prizesbreakdown.php');
			if( isset($oLottery->Record->fiveFactsAbout) ) include('inc/lottery-facts.php');
			if( isset($oLottery->Record->latestResults) ) include('inc/lottery-results.php');
			?>
		</div>


	</div>
</div>

<script>
$(document).ready(function () {
	$('#viewMoreBtn').on('click', function () {
		$('#viewMoreDiv').toggle();
		if ($(this).text() == 'View Less') {
			$(this).text('View Info');
		} else {
			$(this).text('View Less');
		}
	});
});
<?php if( GAENABLED ) { ?>
	dataLayer.push({
		'pageType': 'lotteryPage',
		'auth': '<?= $oUser->loginCheck() ? 1:0 ?>',
		'userId': '<?= $oUser->getID() ?>',
		'depositTotal': '<?= $oUser->getCurrentBalance() ?>',
		'ecommerce': {
			'currencyCode': '<?= $site->country->currencyCode ?>',
			'detail': {
				'actionField': {'list': 'Lottery page'},
				'products': [{
					'name': '<?= $sLottery ?>',
					'brand': '<?= $sLotteryJackPot ?>',
					'category': 'numbers',
					'position': '0'
				}]
			}
		}
	});
<?php } ?>
</script>