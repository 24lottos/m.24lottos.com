<style>
	footer { display: none; }
</style>

<div class="row">
	<div class="col-lg-8">
		<div style="clear: both;"></div>
		<div class="col-lg-12">
			<article>
				<?php
				$oPage = new Page;
				$oPage->_view( 13 );
				?>
				<h5 class="pageHeading">Buy Lottery Tickets to Worldwide Lotteries</h5>

				<div class="blue-header">
					<div class="center">
						<span class="white font11">All Lotteries on 24Lottos.com</span>
					</div>
				</div>
				<div class="table-responsive ">
					<?php
					$oLotteryList = new Lottery;
					$rUpcomingLotteries = $oLotteryList->view( "WHERE displayOnFront='Y' ORDER BY isOmitted" );
					?>
					<table class="table featured_lottery">
						<tbody>
						<?php
						while( $rUpcomingLotteries->nextRecord() ):
							$_url = $site->baseURLm( 'lottery/'.$rUpcomingLotteries->Record->alias );
						?>
							<tr class='gotolottery'
								data-name="<?= $rUpcomingLotteries->Record->lotteryName ?>"
								data-jackpot="<?= $rUpcomingLotteries->Record->jackpot ?>"
								data-url="<?= $_url ?>">
								<td>
									<a href='<?= $_url ?>'><img src="<?= $site->baseURL( 'images/lottery_logos/small_logos/'.$rUpcomingLotteries->Record->Small_Image_url ) ?>" alt="<?= $rUpcomingLotteries->Record->lotteryNameModified ?>"/></a>
								</td><td width='100%'>
									<div class="game"><a href='<?= $_url ?>'><h3><?= $rUpcomingLotteries->Record->lotteryNameModified ?></h3></a></div>
								</td><td>
									<button class="btn3 btn-play">PLAY</button>
								</td>
							</tr>
						<?php endwhile; ?>
						</tbody>
					</table>
				</div>

				<?= str_replace( '<p>{lottery-list}</p>', '', htmlspecialchars_decode( $oPage->Record->txtpagecontent ) ); ?>

			</article>
		</div>

		<div style="clear: both;"></div>
		<br/>
	</div>

</div>
<script>
var goto='';
$(document).ready(function ($) {
	$('.gotolottery').click(function () {
<?php if( GAENABLED ) { ?>
		dataLayer.push({
			'event': 'lottos',
			'eventCategory': 'numbers',
			'eventAction': 'click',
			'eventLabel': 'play',
			'ecommerce': {
				'click': {
					'actionField': { "list": "lottery" }, // list name
					'products': [{
						'name': $(this).data('name'),
						'brand': $(this).data('jackpot'),
						'category': 'numbers',
					}]
				}
			}
		});
<?php } ?>
		goto=$(this).data('url');
		setTimeout("location.href=goto",300);
	});
});
</script>
</div>
