<?php
$t = array(
	'sLottery' => decrypt( $_GET['l'] ),
	'sLotteryID' => decrypt( $_GET['i'] ),
	'sLotteryJackPot' => decrypt( $_GET['j'] ),
	'sLotteryJackPotCurrency' => decrypt( $_GET['c'] ),
	'sTimeToDraw' => decrypt( $_GET['t'] ),
);

$l = stringURLSafe( $t['sLottery'] );

$_SESSION['lottery_details'] = $t;
header( 'location:'.$site->baseURLm( "lottery/$l" ) );
