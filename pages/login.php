<?php
if( $oUser->loginCheck() ):
	header( 'location:'.BASE_URLm );
	exit();
endif; ?>
<style> .lessScroll { display: none !important; } </style>
<div class="login-page">
	<div class="col-lg-8">
		<div class="col-lg-12">
			<div class="login_page">
				<div class="col-lg-12">

					<div class="register-now">
						<h6><strong>Don&#39;t have a 24Lottos account yet?<br>
							Try it now. Its FREE!</strong></h6>
						<br>
						<button type="submit" class="btn2 btn-blue" id="loginSignup" style='width:242px;border:none;height:38px;line-height:38px;padding:0'>
							Sign Up &nbsp;<i class="fa fa-arrow-circle-right"></i>
						</button>
						<!--input type="submit" id="loginSignup" class="btn btn-blue padding10" value="Sign Up"/-->
					</div>
					<div style="clear:both"></div><br>

					<div class="col-lg-6 col-md-6">
						<div class="signup-header">
							<i class="fa fa-unlock-alt" aria-hidden="true"></i>
							<h1>Login</h1>
						</div>
						<?php if( isset($_SESSION['registration_status']) && $_SESSION['registration_status'] == 'activation_successful' ) {
							unset($_SESSION['registration_status']); ?>
							<div class="success-msg">Activation Successful! You can now Login!</div>
						<?php }
							echo (isset($_SESSION['login_error']) && isset($_SERVER['HTTP_REFERER'])) ? "<div class='msg danger margin' style='padding: 10px;'><div class='fa fa-warning'></div> ".$_SESSION['login_error']."</div>":'';
							unset($_SESSION['login_error']);
						 ?>
						<?php if (FBGP_LOGIN) { ?>
							<!-- Social Buttons Login -->
							<div class="social-login">
								<a href="<?= $site->baseURLm("login/facebook") ?>" rel="nofollow" class="facebook_button">
									<i class="fa fa-facebook"></i> Facebook
								</a>
								<a href="<?= $site->baseURLm("login/google") ?>" rel="nofollow" class="google_button">
									<i class="fa fa-google-plus"></i> Google+
								</a>
							</div>
						<?php } ?>

						<div class="col-lg-6 no-padding" style="margin-top:4px;">
							<form role="form" method="post" id="frmLogin2" action="<?= BASE_URLm ?>do-login">
								<div class="form-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
									<div class='m40'>
										<input type="email" name="login_user" id="login_user_2"
											   maxlength="96" class="form-control required"
											   value="<?php echo isset($_SESSION['logged_data']) ? $_SESSION['logged_data']:'' ?>"
											   placeholder="Email" required='required'
											   pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" />
									</div>
								</div>
								<div class="form-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
									<div class='m40'>
										<input name="login_pass" id="login_pass_2" type="password" maxlength="40"
											   class="form-control required" required='required'
											   placeholder="Password 6-30 characters" />
									</div>
								</div>
								<div class="row">
									<button type="submit" name="btnLogin2" class="btn2 btn-green" id="btnLogin2" style='border:none;height:38px;line-height:38px;padding:0'>
										Login &nbsp;<i class="fa fa-arrow-circle-right"></i>
									</button>
								</div>
								<div class="row">
									<div class="pull-right forgot-password">
										<a href="<?= BASE_URLm ?>forgot" class="pull-right"><u>Forgot your password? </u></a>
									</div>
								</div>
							</form>
						</div>
					</div>


				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<?php
UI::packages_tables( 'pending', 'Confirm your Packages' );

$oLottery = new Lottery;
$oLottery->areNumbersPicked( session_id() );
if( $oLottery->numRows() ) {
	UI::tickets_tables( 'pending', 'Pending Tickets', 'mobile' );
} else {
	include( 'login-winners.php' );
}?>

</section>
<script type="text/javascript"> var view_name = "loginm";</script>
<script>
	$(document).ready(function ($) {
		$('#loginSignup').click(function () {
			<?php if( GAENABLED ): ?>
			dataLayer.push({
				'event': 'lottos',
				'eventCategory': 'authorization',
				'eventAction': 'sign up',
				'eventLabel': 'forward' // success, fail or forvard (if click happened on main page)
			});
			<?php endif; ?>
			window.location='<?= $site->baseURLm( 'signup' ) ?>'
		});
	});
</script>
