<?php

if( !$oUser->loginCheck() ) {
	header( 'location:'.$site->baseURLm( 'login' ) );
	exit;
}

$bet = new Bet;
if( $bet->checkBalance()==false ) {
	header( 'location:'.$site->baseURLm( 'account/deposit' ) );
	exit;
}

?>
<section id="msection">
	<div class="row">
		<div class="col-lg-8">

			<div id="yourLotteryPicksTableContainer">
				<?php
				UI::packages_tables( 'pending', 'Pending Packages' );

				UI::tickets_tables( 'pending', 'Confirm your Tickets', 'mobile' );
				// get GA data
				$impressions = [ ];
				$count = 0;
				$oLottery->areNumbersPicked( session_id() );
				while( $oLottery->nextRecord() ) {
					$impressions[] = [
						'name'    => "'".$oLottery->Record->gameName."'",
						'brand'   => "'".$oLottery->Record->jackpot."'",
						'category'=> 'numbers',
						'position'=> $count,
						'list'    => 'Checkout',
					];
					$count++;
				}
				?>
			</div>

			<div class="col-lg-8">
				<h5 class="nomargin" style='color:#fff;background:#35589c'>After you checkout</h5>
				<div style="background-color:#f5f5f5;text-align:center">
					<ul style="display:inline-block;padding:5px 15px;list-style:square;text-align:left;">
						<li>You&#39;ll receive confirmation of your order.</li>
						<li>An authorized agent will buy your lottery tickets on your behalf.</li>
						<li style="font-weight:bold">A ticket purchase confirmation will be emailed to you shortly.</li>
						<li>Lottery results and winning information will be mailed to you.</li>
						<li>Any winnings are credited to your account or sent to you directly.</li>
					</ul>
					<img src="https://www.24lottos.com/images/secure_logo01.png" style="max-width:180px;margin-bottom:10px">
				</div>
			</div>

		</div>
	</div>
</section>
<script>
<?php if( GAENABLED ) { ?>
	dataLayer.push({
		'pageType': 'Checkout',
		'auth': '<?= $oUser->loginCheck() ? 1:0 ?>',
		'userId': '<?= $oUser->getID() ?>',
		'depositTotal': '<?= $oUser->getCurrentBalance() ?>',
		'event': 'checkout',
		'ecommerce': {
			'checkout': {
				'actionField': {'step': 1},
				'products': <?= json_encode( $impressions ) ?>
			}
		}
	});
<?php } ?>
</script>
