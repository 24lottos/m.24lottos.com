<?php
/**
 * Created by PhpStorm.
 * User: Nikolic
 * Date: 8/10/2016
 * Time: 8:23 AM
 * Template file for 404 page
 */

$p = 'error';
include('inc/header.php');

?>
<div id="hello">
	<div class="container whitebg">
		<?php include('inc/nav.php') ?>
		<div class="row">
			<div class="col-lg-12">
				<div style="clear: both;"></div>
				<div class="col-lg-12">
					<article>
						<div class="message_wrapper"><h2>404</h2>
							<div class="message_body">You are on a <br/> page that <br/> does not <br/> exist.
								<div>Visit our <br/> <a href="<?php echo(BASE_URLm); ?>">Home page</a></div>
							</div>
						</div>
					</article>
				</div>
				<div style="clear: both;"></div>
				<br/>
			</div>
		</div>

	</div> <!-- /container -->
</div><!-- /hello -->

<?php if( $p != 'signup' ) include('inc/footer.php'); ?>

<script>
	$(document).ready(function () {
		$("div.howToPlayBanner").hide();
		$("#f").hide();
	});
<?php if( GAENABLED ) { ?>
	dataLayer.push({
		'pageType': 'Error page',
		'erroCode': '404',
		'auth': '<?= $oUser->loginCheck() ? 1:0 ?>',
		'userId': '<?= $oUser->getID() ?>',
		'depositTotal': '<?= $oUser->getCurrentBalance() ?>'
	});
<?php } ?>
</script>
