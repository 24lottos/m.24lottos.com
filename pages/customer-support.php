<?php
$oTopics = new FaqTopics();
$oTopics->view( 'ORDER BY priority' );

$oFaq = new Faq();

$topics = '';
?>
<div id="faqs_0" style="display: none;">
	<option value="" data-answer="">Select Subject</option>
</div>
<?php
while( $oTopics->nextRecord() ):?>
	<?php $topics .= '<option value="' .$oTopics->Record->id .'">' .stripslashes( $oTopics->Record->title ) .'</option>'; ?>
	<div id="faqs_<?= $oTopics->Record->id; ?>" style="display: none;">
		<option value="" data-answer="">Select Subject</option>
		<?php
		$oFaq->view( "WHERE status = 'Enable' AND topic = " .$oTopics->Record->id ." ORDER BY priority" );
		while( $oFaq->nextRecord() ):
			$question = stripslashes( $oFaq->Record->question );
			$answer = htmlspecialchars_decode( $oFaq->Record->answer );
			?>
			<option value="<?= $question; ?>" data-answer="<?= $answer; ?>"><?= $question; ?></option>
		<?php endwhile; ?>
	</div>
<?php endwhile; ?>
<section>
	<div class="col-lg-12 no-padding" style="color:#898989; font-size:23px; padding-bottom:10px">
		<h1 style="color:#35589c; font-size:24px; font-weight:400; text-transform:uppercase; margin-bottom:15px;">
			Happy to help you 24/5
		</h1>
		<div>We are available 24/5 <a href="mailto:<?= CONTACT_EMAIL ?>" style="color:#e60030;font-size:23px;font-weight:normal;"><?= CONTACT_EMAIL ?></a></div>
		<div>Respond time in less than 24 hours. Before sending us question, please check our <a href="<?= $site->baseURL( 'faq' ) ?>" style="color:#e60030;font-size:23px;font-weight:normal;">FAQ</a> page for value information.</div>
		<div class="clearfix"></div>
	</div>

	<?php
	 if( isset($_POST['btnAddSupport']) ) { // ajax call;
		$data = new stdClass();
		$data->name = $_POST['name'];
		$data->phone = $_POST['phone'];
		$data->subject = $_POST['subject'];
		$data->email = $_POST['email'];
		$data->question = $_POST['question'];
		$oEmail = new Email;
		if( $oEmail->supportMessage($data) && $oEmail->supportMessageCustomerNotification($data) ) {
			echo MSG_SENT;
			$oSupport = new Support;
			$lastID = $oSupport->create($oSupport->clean($_POST, new Clean));
			$oSupport->advanceCreate( $oSupport->cleanSupportSlaveTable($_POST, new Clean, $lastID), 'lotto_support_answers' );
		} else {
			echo MSG_FAIL;
		} ?>
		<style> #frmAddSupport { display:none !important; } </style>
	<?php } ?>
	<div id="message"></div>
	<div class="no-padding" style='border-top:1px solid #d6d6d6;border-bottom:1px solid #d6d6d6'>
		<form method="post" id="frmAddSupport" style="margin: 10px auto;width: 290px">
			<input type="hidden" name="action" value="customer-support"/>
			<div class="form-group">
				<input type="text" name="name" id="name" class="form-control required"
					   onkeyup="validateCustomerSupport()" placeholder="Your Name">
			</div>
			<div class="form-group">
				<input type="email" name="email" id="email" class="form-control email required"
					   onkeyup="validateCustomerSupport()" placeholder="Your Email">
			</div>
			<div class="form-group">
				<input type="text" name="phone" id="intelinput" class="form-control">
				<input type="hidden" id="hiddenphone" name="hiddenphone"/>
			</div>
			<div class="form-group">
				<select class="form-control" id="topic">
					<option value="0">Select Topic</option>
					<?= $topics ?>
					<input type="hidden" id="category" name="category" value="No category"/>
				</select>
			</div>
			<div class="form-group">
				<select class="form-control" id="subjects_list">
					<option value="">Select Subject</option>
				</select>
				<input type="text" name="subject" class="form-control required" id="subject" placeholder="Or type your own..."
					   onkeyup="validateCustomerSupport()">
				<div id="answer" style="display: none;"></div>
			</div>
			<div class="form-group">
				<textarea class="form-control required" id="question" name="question" style='height:auto' rows="3"
						  onkeyup="validateCustomerSupport()" placeholder="Your Question"></textarea>
			</div>
			<div class="form-group" style='clear:both'>
				<div class="center">
					<input type="hidden" value="dummy" name="btnAddSupport"/>
					<button type="submit" class="btn-grey padding10" id="btnAddSupport" disabled="disabled"
						style="width: 180px;margin: 10px;padding: 10px;">
						Submit &nbsp;<i class="fa fa-arrow-circle-right"></i>
					</button>
				</div>
			</div>
		</form>
	</div>

	<div style="font-size:18px; font-weight: normal;text-align: center;padding-top: 15px;">
		<?php /* if( $site->country->code != "NG" ) { */ ?>
		<div style='display:inline-block'>
			Customer service opening hours are:<br/>
			Monday- Friday 9:00 AM till 16:00 PM<br/>
			Contact us via WhatsApp!
			<a href='whatsapp://send?abid=<?= preg_replace('/\s/','',CONTACT_PHONE) ?>&text=Hello!' style='color:#000'><br>
			<a href='tel://<?= CONTACT_PHONE ?>' style='color:#000'>
			<?= CONTACT_PHONE ?></a><br/>
			Or via Facebook Messenger<br><br>
			<a href="https://api.whatsapp.com/send?phone=447453493923" target="_blank" rel="nofollow"><img src='/img/whatsapp.png' alt='Contact us!' style='width:40px;'/></a>
			<a href="https://m.me/24lottos" target="_blank" rel="nofollow"><img src='/img/messenger-icon.png' alt='Contact us!' style='width:40px;'/></a>
		</div>
	</div>
	<div style="text-align: center;margin-top:20px;"><img src="<?= $site->baseURL( 'assets/img/customer-support.jpg' ) ?>" alt="Customer Support"></div>
</section>
<script>
jQuery(document).ready(function () {
	var showAnswer = true;

	$('#topic').on('change', function() {

		var topicID = this.options[this.selectedIndex].value;
		$('#subjects_list').html( $('#faqs_' + topicID).html() );
		$('#subjects_list').trigger('change');
		$('#category').val(this.options[this.selectedIndex].text);
	});

	$('#subjects_list').on('change', function() {

		var answer = $(this.options[this.selectedIndex]).data('answer');
		$('#answer').html( answer );
		$('#subject').val(this.options[this.selectedIndex].value);

		if ( this.options[this.selectedIndex].value != '' ) {
			$('#answer').slideUp( "slow" );
			$('#answer').slideDown( "slow" );
			showAnswer = false;
		}
		else if ( this.options[this.selectedIndex].value == '' && !showAnswer ) {
			$('#answer').slideUp( "slow" );
			showAnswer = true;
		}
	});

	if(visitorCountry != 'US') {
		if ($.isFunction($.fn.intlTelInput)) $("#login_user, #phone").intlTelInput({
			allowExtensions: true,
			autoFormat: true,
			initialCountry: 'auto',
			autoPlaceholder: true,
			geoIpLookup: function (callback) {
				callback(visitorCountry);
			},
			nationalMode: true,
			numberType: "MOBILE",
			excludeCountries: ['us'],
			utilsScript: "js/utils.js",
		});
	}

});
</script>