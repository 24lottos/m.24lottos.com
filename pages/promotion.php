<?php
/**
 * promotion.php - Short description
 *
 * Membership Club, lottery page
 *
 * @author			Sergey V Musenko <musenko@gmail.com>
 * @date			2017-02-08
 *
 */

// get config file
$club = trim(preg_replace( '/([^A-Za-z0-9\-])+/', '-', $_GET['l'] ));
$file = BASE_PATH.'inc/package-'.$club.'.php';
// last sold time, rand, in seconds:
$lastSoldMin = 10;
$lastSoldMax = 661;
//unset($_SESSION['lastsold']);

if( empty($club) || !is_file($file) ) { // there is no such club!
	header( 'HTTP/1.1 303 Moved Temporarily' );
	header('Location: /error');
	exit;
}
include( $file ); // get club config file

// CONFIRM button: do same as "confirm' below pending tickets - try to purchase!
if( isset($_POST['action']) ) { // save selected package and try to confirm it
	$userid = (int)$oUser->getID();
	$option = (int)$_POST['option'];
	$club = [
		'userid'     => $userid,
		'clubid'     => $mc_id,
		'clubname'   => $mc_name,
		'cluboption' => $mc_plan[$option]['title'],
		'price'      => $mc_plan[$option]['price'],
		'oldPrice'   => $mc_plan[$option]['oldprice'],
		'totalPrice' => $mc_plan[$option]['months'] * $mc_plan[$option]['price'],
		'save'       => $mc_plan[$option]['save'],
		'ticketsNum' => $mc_plan[$option]['ticketN'],
		'months'     => $mc_plan[$option]['months'],
		'days'       => $mc_plan[$option]['actDays'],
	];

	if( $userid ) { // logged in: create DB record and go to confirm
		unset($_SESSION['createMembership']);
		$oLottery->createLotteryPackage( $club );
		header('Location: /vcheckout'); // '/confirm?club=1'

	} else { // not logged in: go to login
		unset($_SESSION['createMembership']); // ONLY 1 pending package allowed
		$_SESSION['createMembership'][] = $club;
		header('Location: /login');
	}
	exit;
}
?>

<form id='package-form' method='post'>
	<input type='hidden' name='action' value='membership'/>
	<input type='hidden' name='id'     value='<?= $mc_id ?>'/>
	<input type='hidden' name='name'   value='<?= $mc_name ?>'/>
	<input type='hidden' name='option' value='<?= $mc_best ?>'/>
</form>

<div class='row'>
	<div class='col-lg-12' style='margin-bottom:-10px'>
		<article class='promotion' style='display:none'>
			<div class='promotitle'>
				<h5 class="pageHeading"><strong><?= $mc_title ?></strong></h5>
				<img src='<?= BASE_URL.$mc_icon ?>' alt='<?= $mc_name ?>' />
			</div>
			<div class='selectPlan'>

				<?php foreach( $mc_plan as $k=>$plan ) {
					$replace = [
						'{OLDPRICE}'  => number_format($plan['oldprice']*$plan['months'],2),
						'{TOTALPRICE}'=> number_format($plan['price']*$plan['months'],2),
						'{PRICE}'     => number_format($plan['price'],2),
						'{MONTHS}'    => $plan['months'],
						'{DISC}'      => $plan['save'],
					];
					$comment = str_replace( array_keys($replace), array_values($replace), $plan['comment'] );
					if( !isset( $_SESSION['lastsold'][$club][$k]) ) {
						$_SESSION['lastsold'][$club][$k] = time() - rand($lastSoldMin,$lastSoldMax);
					} else if( $_SESSION['lastsold'][$club][$k] < time()-$lastSoldMax ) {
						$_SESSION['lastsold'][$club][$k] = time() - $lastSoldMin;
					}
					$lastsold = time()-$_SESSION['lastsold'][$club][$k];
					$lastsold = ($lastsold<59 ? "$lastsold secs" : floor($lastsold/60).' mins').' ago';
					$hint = str_replace( array_keys($replace), array_values($replace), $plan['hint'] );
					?>
					<div class='plan-box<?= $k==$mc_best ? " active":"" ?>' data-n='<?= $k ?>' >
						<?= $k==$mc_best ? '<div class="mostFav">BEST SELLER</div>':'' ?>
						<div class='glyphicon glyphicon-ok'></div>
						<div class='lastsold'>LAST SOLD<br><?= $lastsold ?></div>
						<div class='plan-name'><?= $plan['title'] ?></div>
						<div class='plan-amount'>
							<div class='amount'>$<?= $plan['price'] ?></div>
							<div class='per-month'>per month</div>
						</div>
						<div class='billing-info'><?= $comment ?></div>
						<?php if( $k==$mc_best ) { ?>
						<div class='savings'><?= 'Save '.$plan['save'] ?></div>
						<?php } else { ?>
						<div class='hint'><?= $hint ?></div>
						<?php } ?>
					</div>
				<?php } ?>
				<p><small>* All amounts shown are in <b><span class="symbol" data-symbol="USD"></span></b>.</small></p>
				<?= $mc_bottom ?>
				<div style='text-align:center;padding:10px 0'>
					<button id='pckgconfirm' class='btn-orange' type='submit'>
						<div class="shine">Confirm &nbsp;<i class="fa fa-arrow-circle-right"></i></div>
					</button>
				</div>
				<div class='clearfix'></div>
			</div>
		</article>
	</div>
</div>

<script>
$(document).ready(function () {
	$('.plan-box').click(function(){
		$('.plan-box').removeClass('active');
		$(this).addClass('active');
		$('#package-form')[0].option.value=$(this).data('n');
	});
	$('#pckgconfirm').click(function(e){
		e.preventDefault();
		$('#package-form')[0].submit();
	});
	$('.mostFav').fadeIn();
	$('.promotion').fadeIn();
});
</script>
