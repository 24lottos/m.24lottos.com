<?php
if( $oUser->loginCheck() ):
	header( 'location:'.$site->baseURLm( 'account/dashboard' ) );
endif;
?>
<section id="msection">
	<h5 class="blue">Forgot your password? Don't worry. Please fill in your registered E-mail address and we'll reset a new password for you and send to your E-mail.</h5>
	<?php if( isset($_POST['btnForgetPass']) ):
		$displayemail = trim( $_POST['displayemail'] );
		if( $oUser->isEmailExist( $displayemail ) ):
			$oUser->setPassword();
			$newpass = $oUser->getPassword();
			$oUser->resetForgetPassword( $displayemail, $newpass );
			$oUser->forgetPassSMS( $oUser, $newpass );
			echo '<div class="alert alert-success"><div class="fa fa-check"></div> An E-mail has been sent to you with new password.
            <a href="'.$site->baseURL( 'login' ).'"><b>Login Now!</b></a></div>';
		else:
			echo '<div class="alert alert-success"><div class="fa fa-check"></div> User does not exist.
                <a href="'.$site->baseURL( 'signup' ).'"><b>Signup Now!</b></a></div>';
		endif;
	endif;
	?>

	<form id="frmLogin2" method="POST">
		<div class="row">
			<div class="form-group">
				<label>Your Registered E-mail address</label><div style="clear: both;"></div>
				<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
				<div class='m40'>
					<input type="email" class="form-control required" name="displayemail" id="email" required='required'>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="center">
				<input type="hidden" name="btnForgetPass" value="dummy"/>
				<button type='submit' class="btn btn-green login-btn" id="btnForget">
					Submit &nbsp;<i class="fa fa-arrow-circle-right"></i>
				</button>
			</div>
		</div>
	</form>

</section>
<br/>
