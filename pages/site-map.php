<?php
$lottery = new Lottery();
?>
<section id="msection">
	<h1 class="blue">Site Map</h1>

    <h2 class="sitemap_subtitle">Common Pages</h2>
    <div class="site-map">
        <a href="<?= $site->baseURLm() ?>">Home</a>
        <a href="<?= $site->baseURLm( 'how-to-play' ) ?>">How to Play</a>
        <a href="https://twitter.com/36lotto">Follow us on Twitter</a>
        <a href="https://www.facebook.com/36lotto">Join us on Facebook</a>
        <a href="<?= $site->baseURLm( 'terms-and-conditions' ) ?>">Terms and Conditions</a>
        <a href="<?= $site->baseURLm( 'privacy-policy' ) ?>">Privacy Policy</a>
        <a href="<?= $site->baseURLm( 'customer-support' ) ?>">Customer Support</a>
        <a href="<?= $site->baseURLm( 'faq' ) ?>">FAQ</a>
        <a href="<?= $site->baseURLm( 'lottery' ) ?>">Lotteries</a>
        <a href="<?= $site->baseURLm( 'results' ) ?>">Lottery Results</a>
        <a href="<?= $site->baseURLm( 'winners' ) ?>">Lottery Winners</a>
        <a href="<?= $site->baseURLm( 'signup' ) ?>">New Registration</a>
        <a href="<?= $site->baseURLm( 'login' ) ?>">Login to your account</a>
        <a href="<?= $site->baseURLm( 'press' ) ?>">Press</a>
    </div>

    <h2 class="sitemap_subtitle">Lotteries</h2>
    <div class="site-map">
        <?php
        $lottery->view( "WHERE displayOnFront = 'Y' ORDER BY isOmitted" );
        while( $lottery->nextRecord() ):
        ?>
            <a href="<?= $site->baseURL() .'lottery/' .$lottery->Record->alias ?>"><?= $lottery->Record->lotteryNameModified ?></a>
        <?php
        endwhile;
        ?>
    </div>

    <h2 class="sitemap_subtitle">Lottery Results</h2>
    <div class="site-map">
        <?php
        include BASE_PATH ."config/last_results_data.php";
        foreach( $results_meta_title as $k => $v ):
        ?>
            <a href="<?= $site->baseURLM() .'lottery/' .$k .'/results'?>"><?= $lottery->getDetails( $lottery->getLottoName($k) )->Record->lotteryNameModified ?> results</a>
        <?php
        endforeach;
        ?>
    </div>
</section>

<style>
    .sitemap_table a{
        color: #35589C;
        margin-left: 20px;
    }

    .sitemap_subtitle {
        color: #35589c;
        font-size: 20px;
        text-decoration: underline;
        font-weight: normal;
    }
</style>