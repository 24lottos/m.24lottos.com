<?php
$page_press = ( new Page )->_view(21);
?>
<div class="row">
    <div class="col-lg-8">
        <article>
            <h1 class="tables_heading"><?=$page_press->Record->varheader;?></h1>
            <?php echo htmlspecialchars_decode ( $page_press->Record->txtpagecontent ); ?>
        </article>
    </div>
</div>