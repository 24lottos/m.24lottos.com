<section class="terms">
	<article>
		<?php
		$oPage = new Page;
		$oPage->_view( 4 );
		?>
		<h5 class="pageHeading"><strong><?= stripslashes( $oPage->Record->varheader ) ?></strong></h5>
		<?= htmlspecialchars_decode( $oPage->Record->txtpagecontent ) ?>
	</article>
</section>
