<section class="privacy-policy" id="msection">
	<article>
		<?php
		$oPage = new Page;
		$oPage->_view( 3 );
		?>
		<h5 class="pageHeading"><strong><?= stripslashes( $oPage->Record->varheader ) ?></strong></h5>
		<?= htmlspecialchars_decode( $oPage->Record->txtpagecontent ) ?>
	</article>
</section>
