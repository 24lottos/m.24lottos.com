<?php
$lotteryName = trim($_GET['l']);

$_Lotteries = $oLottery->upcomingLotteries( null, true );
$lotteryFound = false;
foreach( $_Lotteries as $t ) {
	if( stringURLSafe($t['alias'])==$lotteryName ) {
		$sLotteryID = $t['GameId'];
		$sLottery = $t['GameName'];
		$sLotteryJackPot = $t['CurrentJackpot'];
		$sLotteryJackPotCurrency = $t['JackpotCurrency'];
		$isOmitted   = $t['isOmitted'];
		$sTimeNow	= gmdate( 'Y-m-d H:i:s' );
		$sTimeToDraw = $t['NextPurchaseDeadline'];
		$sTimeToDraw = $isOmitted || $sTimeToDraw[0]==0 ? $sTimeNow : $sTimeToDraw;
		$lotteryFound = true;
		break;
	}
}
//if( !$lotteryFound ) {
//	header( 'location:'.$site->baseURL( "error" ) );
//	exit();
//}
unset($_Lotteries);
$oLottery->getDetails( $sLottery );

$name = explode( '-', $oLottery->Record->lotteryNameModified );
if( count($name)<2 ) $name[1] = '';
$lottery_full_name = trim($name['0']) ." " .trim($name['1']);

$default_results_description = "See the official ".$lottery_full_name ." results for the last 10 draws.";
global $results_description;
?>
<?php include( 'inc/banner.php' ); ?>
<section id="msection">
	<!-- blue header Html Here -->
	<article>
		<div class="blue-header">
			<div class="center">
				<span class="white font11"><?= $lottery_full_name ?> Results</span>
			</div>
		</div>
		<!-- blue header Html End Here -->
		<div>
			<p><?= array_key_exists( $lotteryName, $results_description ) ? $results_description[$lotteryName] : $default_results_description; ?></p>
		</div>
		<!-- Table  str Html Here -->
		<table class="table table-striped bmgrid yourLotteryPicksTable">
			<thead>
			<tr>
				<th>Date</th>
				<th>Result</th>
			</tr>
			</thead>
			<tbody>
			<?php
			//			$o = new Lottery;
			$oLotteryResultsArchive = new Lottery();

			$lotteryArchiveName = strtolower( str_replace('-', '', $lotteryName ) );
			$lotteryArchiveName = ( array_key_exists( $lotteryArchiveName, $alias_archive_name) ) ? $alias_archive_name[$lotteryArchiveName] : $lotteryArchiveName;
			$qLotteryResultsArchive = "SELECT
							  lotto_rrc_results_archive.datetime,
							  lotto_rrc_results_archive.winnumbers,
							  lotto_rrc_results_archive.timezone
							FROM lotto_rrc_results_archive
							WHERE lotto_rrc_results_archive.lotteryname = '" . $lotteryArchiveName."'
							ORDER BY lotto_rrc_results_archive.datetime DESC
							LIMIT " .NUM_ROWS_ON_ARCHIVE;
			try {
				$oLotteryResultsArchive->query($qLotteryResultsArchive);
			} catch (ErrorException $message) {}

			$has_archive = ( $oLotteryResultsArchive->numRows() && !isset($message) ) ? true : false;
			if ( $has_archive ) {
				while( $oLotteryResultsArchive->nextRecord() ):
					$winnumbers = json_decode( $oLotteryResultsArchive->Record->winnumbers );

					$current_lottery_day = new \DateTime( 'now', new \DateTimeZone( $oLotteryResultsArchive->Record->timezone ) );
					$current_lottery_day->setTime( 0, 0, 0 );

					$draw_date = new \DateTime( $oLotteryResultsArchive->Record->datetime, new \DateTimeZone( $oLotteryResultsArchive->Record->timezone ) );
					$draw_date->setTime( 0, 0, 0 );

					$diff = $current_lottery_day->diff( $draw_date );
					$diffDays = intval( $diff->format( "%R%a" ) );

					$relative_to_today = '';
					switch( $diffDays ){
						case 0: $relative_to_today = 'Today, '; break;
						case -1: $relative_to_today = 'Yesterday, '; break;
						default: $relative_to_today = ''; break;
					}

					$draw_date = date( 'd M', strtotime( $oLotteryResultsArchive->Record->datetime ) );
					?>
					<tr>
						<td class="centered"><?= $relative_to_today .$draw_date ?></td>
						<td class="centered">
							<?php foreach( $winnumbers->numbers as $number ): ?>
								<div class="red-circle">
									<span class="white"><?= $number ?></span>
								</div>
							<?php endforeach; ?>
							<?php foreach( $winnumbers->numbers_extra as $number ): ?>
								<div class="red-circle" style="background:#666">
									<span class="white"><?= $number ?></span>
								</div>
							<?php endforeach; ?>
						</td>
					</tr>
					<?php
				endwhile; ?>
				<?php

			} else {
				header( 'location:'.$site->baseURLM( "lottery/" .$lotteryName ) );
				exit();
			} ?>
			</tbody>
		</table>
		<!-- Table  str Html Here -->
		<?php if ( !$isOmitted ): ?>
			<a href="<?= $site->baseURLM() ?>lottery/<?= $lotteryName ?>" id="show_lottery_page" class="btn2 btn-green pull-right w150">
				<div class="shine">PLAY &nbsp;<i class="fa fa-arrow-circle-right"></i></div>
			</a>
		<?php endif; ?>
	</article>
	<script type="application/ld+json">
	{
	  "@context": "http://schema.org",
	  "@type": "Table",
	  "about": "<?= $lottery_full_name ?>  - Latest Results"
	}
	</script>
</section>


<script>
	$(document).ready(function () {
		$('[data-countdown]').each(function () {
			var $this = $(this), finalDate = $(this).data('countdown');
			$this.countdown(finalDate, function (event) {
				$this.html(event.strftime('%D days %H:%M:%S'));
			});
		});
	});
</script>
