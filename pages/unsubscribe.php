<?php
/*
 * Usubscribe without loggin in
 * - that is why it is in /pages but not in /upanel
 * $_GET['user'] - encrypted user's email
 */

if( $oUser->getID() ) { // logged in only
	$oUser->unsubscribe();

} else { // guest mode
	$userid = 0;
	if( isset($_GET['user']) ) { // get user's email
		$eml = base64_decode(strrev(urldecode($_GET['user'])));
		if( filter_var($eml, FILTER_VALIDATE_EMAIL) ) { // email is correct formatted
			$userid = $oUser->getIdByEmail( $eml );
		}
	}

	if( !$userid ) { // user id not fount
		header( 'location: /' );
		exit;

	} else { // ok, unsubscribe him!
		$oUser->unsubscribe( $userid );
	}
}

?>
<div class="row">
	<div class="col-lg-12">
		<div>
			<h5 class="pageHeading"><strong>Are You Sure? We're Sorry to See You Leaving…</strong></h5>
			<p>We just got your request to unsubscribe you from our promotional emails.
			You won't receive them anymore. Please keep in mind that we cannot
			unsubscribe you from the email notifications regarding your scanned
			tickets and winnings. You can turn on the promotional emails in 
			<a href="/account/account"><u>your profile</u></a> any time.</p>
			<p>Thank you for staying with 24Lottos.com!</p>
		</div>
	</div>
</div>
