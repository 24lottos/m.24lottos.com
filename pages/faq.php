<section id="msection" class="faq">
	<article>
		<h1 class="blue">Frequently Asked Questions</h1>

        <?php
        $oTopics = new FaqTopics();
        $oTopics->view( "WHERE status = 'Enable'  ORDER BY priority" );

        $oFaq = new Faq;
        ?>

        <div class="dev-accordion" id="topics">
            <?php while( $oTopics->nextRecord() ): ?>
                <h2 class="panel-title"><i class="fa <?= stripslashes( $oTopics->Record->icon ) ?>" aria-hidden="true"></i><?= stripslashes( $oTopics->Record->title ) ?></h2>

                <div class="dev-accordion">
                    <?php

                    $oFaq->view( "WHERE status = 'Enable' AND topic = " .$oTopics->Record->id ." ORDER BY priority" );
                    while( $oFaq->nextRecord() ):
                        echo '<div class="panel-title">'.stripslashes( $oFaq->Record->question ).'</div><div>';
                        echo htmlspecialchars_decode( $oFaq->Record->answer ).'</div>';
                    endwhile;
                    ?>
                </div>
            <?php endwhile; ?>
        </div>
	</article>
	<div style="margin-top:50px;"></div>
</section>

<script>
    jQuery(document).ready(function () {
        setTimeout( function () {
            $( "#topics" ).accordion( "option", "active", 0 );
        }, 100 );
    });
</script>