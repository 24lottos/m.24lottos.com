<style>
	html { padding-bottom: 60px; }
</style>
<section>

	<?php include(BASE_PATHm.'inc/featured_lotteries.php'); ?>

	<div class="home_page">
		<div class="col-lg-8">
			<article id='numbersWrap'>
				<div class="header hidden">
					<div class="col-lg-6">
						<ul class="list-inline centered ">
							<li class="draw-time white" id="jackpot_draw"></li>
						</ul>
					</div>
				</div>
				<div class="green-header hidden">
					<div class="col-lg-6">
						<ul class="list-inline centered ">
							<li class="draw white" id="jackpot_price">
								Jackpot: <?= getCurrencySign( $firstLottery[0]['JackpotCurrency'] ).number_format( $firstLottery[0]['CurrentJackpot'] ) ?></li>
						</ul>
					</div>
				</div>
				<div style="clear: both;"></div>

				<div class='balls_box' onclick='$(".btn3").css("opacity","1")'>
					<div class='regular_box'>
						<button type="button" class="btn3 btn-warning" id="autoPick">iForecast</button>
						<div id="regular_picks" class="col-lg-10 white"></div>
						<div style="clear: both;"></div>
						<div id="regular_balls" class="col-lg-12"></div>
						<div style="clear: both;"></div>
					</div>
					<div class='extra_box'>
						<div id="extra_picks" class="col-lg-10 white"></div>
						<div style="clear: both;"></div>
						<div id="extra_balls" class="col-lg-12"></div>
					</div>
					<div style="clear: both;"></div>
				</div>
				<div id="selected_balls"></div>
				<div class="btn-section v-gradient-white">
					<button type="button" id="playLottery">
						<div class='shine'>PLAY &nbsp;<i class="fa fa-arrow-circle-right"></i></div>
					</button>
				</div>
				<div style="clear: both;"></div>
			</article>

			<div id="yourLotteryPicksTableContainer">
				<?php
				UI::packages_tables( 'pending', 'Confirm your Packages' );

				UI::tickets_tables( 'pending', 'Confirm your Tickets', 'mobile' );
				?>
			</div>
            <?php /*
            <div id="how-to-play-link">
                <a href="/lottery/<?= $firstLottery['0']['alias']; ?>#lottery-how-to-play">How to Play <?= $firstLottery['0']['DisplayName']; ?></a>
            </div>
            <style>
                #how-to-play-link {
                    width: 100%;
                    text-align: right;
                    margin-bottom: 15px;
                }
                #how-to-play-link a {
                    color: #35589c;
                    text-decoration: underline;
                    font-weight: bold;
                }
            </style>
            */?>
		</div>
	</div>
</section>


<?php $_GET['p'] = 'home'; ?>
