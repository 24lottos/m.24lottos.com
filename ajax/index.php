<?php
ob_start();
session_start();

require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require_once(PATH2CONFIG.'constants.php');
require_once(PATH2CONFIG.'functions.php');

$acao = ( ENVIRONMENT ) ? $_SERVER['HTTPS_ORIGIN'] : $_SERVER['HTTP_ORIGIN'];
header( "Access-Control-Allow-Origin: {$acao}" );

function __autoload( $classname ) {
	$lower = strtolower( $classname );
	include(BASE_PATH."config/class.{$lower}.php");
}
$site = new Site;
$oUser = new User;

extract( $_SESSION );
extract( $_GET );
extract( $_POST );

include("inc/$action.php");
