<?php

$oAjax = new Lottery;
$oAjax->winners( $startLimit = (--$num * PAGING_SIZE), $endLimit = PAGING_SIZE, $filter );

while( $oAjax->nextRecord() ): ?>
	<tr>
		<td><?= $oAjax->Record->lotteryname ?></td>
		<td><?= date( 'd M', strtotime( $oAjax->Record->enddatetime ) ) ?></td>
		<td><?= ucwords( strtolower( $oAjax->Record->name ) ) ?></td>
		<td><?= $oAjax->Record->currency.' '.number_format( $oAjax->Record->winningamt, 2, '.', ',' ) ?></td>
	</tr>
	<?php
endwhile;
