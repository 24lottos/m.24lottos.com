<?php

$oAjax = new Lottery;
$oAjax->allResults( $startLimit = (--$num * PAGING_SIZE), $endLimit = PAGING_SIZE, $filter );

while( $oAjax->nextRecord() ): ?>
	<tr>
		<td><?= $oAjax->Record->name ?></td>
		<td><?= date( 'd M', strtotime( $oAjax->Record->enddatetime ) ) ?></td>
		<td>
			<?php
			$wn = explode( ',', $oAjax->Record->winningnumbers );
			foreach( $wn as $number ):?>
				<div class="img-circle circle-blue centered">
					<h4><?= $number ?></h4>
				</div>
				<?php
			endforeach;
			?>
		</td>
	</tr>
	<?php
endwhile;
