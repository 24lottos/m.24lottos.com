<?php
/**
 * process-paystack.php - Init payment on PayStack
 *
 * @category   payment system
 * @author     Sergey V Musenko <info@ws123.net>
 * @copyright  2017 (c) 24Lottos
 * @date       2017-07-26
 * @version    0.1
 **/

$response = [ // will return json
	'result'  => 'error',
	'message' => 'Sorry, Paystack init error',
	'pay_url' => '',
];

// get params
$c_amount  = round( 100*$_POST['amount'], 0 ); // amount in NGN kobo (means *100)
$c_email   = trim($_POST['email']);
$amountUSD = $_POST['amountUSD']; // amount in USD
$mode      = @$_POST['mode']; // channels
$reference = "paystk".time().rand(100,999); // our local transaction id
if( !$c_amount || empty($c_email) ) {
	$response['message'] = 'Paystack: wrong Amount or empty Email';
	die( json_encode($response) );
}

chdir( dirname(dirname(__DIR__)) );
require_once( 'vendor/autoload.php' ); // Load composer autoload
require_once( PATH2CONFIG.'constants.php' );
require_once( PATH2CONFIG.'functions.php' );
require_once( PATH2CONFIG.'class.site.php' );
require_once( PATH2CONFIG.'class.database.php' );
require_once( PATH2CONFIG.'class.user.php' );

$oUser = new User();
$user_id = $oUser->getID();
if( !$user_id ) die( json_encode($response) );

$channels = $mode=='bank' ? ['bank']:['card'];
$paystack = new Yabacon\Paystack( PAYSTACK_SECKEY );
try {
	$callback = $PROTOCOL.$DOMAIN.PAYSTACK_CALLBACK;
	$tranx = $paystack->transaction->initialize([
		'callback_url'=> $callback,
		'amount'      => $c_amount,    // in kobo
		'email'       => $c_email,     // unique to customers
		'reference'   => $reference, // unique to transactions
		'channels'    => $channels,
		'metadata'    => (object)[ 'userid' => $user_id, 'cancel_action' => $callback ],
	]);
	if( $tranx->status ) { // success init
		// store transaction reference so we can query in case user never comes back
		$db = new Database();
		$db->connect();
		$data = array(
			'user_id'       => $user_id,
			'amount'        => $c_amount, // in NGN, will check it in CallBack
			'status'        => 'in_progress',
			'type'          => 'cc_debit',
			'TrackingNumber'=> $reference,
			'request_time'  => time(),
			'response'      => $amountUSD, // in USD, will check it in CallBack
		);
		$db->advanceCreate( $data, 'lotto_transactions' );
		// redirect URL
		$response['pay_url'] = $tranx->data->authorization_url;
		$response['result']  = $response['message'] = 'success';
	}

} catch(\Yabacon\Paystack\Exception\ApiException $e){
	$msg = $e->getMessage();
	if( preg_match('/\<title\>(.*?)\<\/title/mis',$msg,$ret) ) $msg = 'Paystack Request failed: '.$ret[1];
	@testWithLog( "--- Paystack Error --- UserID=$user_id, Ref=$reference, Mobile, ".$msg );
}

echo json_encode( $response );
