<?php
/*
 * Set a mark: user started payment on deposit page
 * later we will send him reminder IF he failed
 * or clear a mark on success!
 */
$user = new User;
$user->setUnfinishedPay( $_POST['method'] );
if(LOGPAYMENTSTART) testWithLog( '--- Payment Start --- UserID='.$user->getID().', Amount=$'.$_POST['amount'].', Method='.$_POST['method'] );

// to know last trying with
$_SESSION['LASTPAYTRIED'] = $_POST['method'];
