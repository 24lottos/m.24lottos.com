<?php
global $PACKAGE_DISCOUNT;
$oLottery = new Lottery;
$tLotteries = $oLottery->fetchLotteryDetails();
$oLottery->areNumbersPicked( session_id() );
$bonus_number_color_check = 0;
if( $oLottery->numRows() ) {
	$subTotal = 0;
	$products = [];
	$tickets = [];
	$userDiscount = $oUser->getDiscount();
	while( $oLottery->nextRecord() ) {
		if( isset($tLotteries[$oLottery->Record->gameName]['Balls']) ) {
			$bonus_number_color_check = $tLotteries[$oLottery->Record->gameName]['Balls'];
		} else {
			$bonus_number_color_check = 0;
		}
		$_n = str_replace("'","\\'",$oLottery->Record->gameName);
		$_j = $oLottery->Record->jackpot;
		$_p = $oLottery->Record->price;
		$_d = (int)$oLottery->Record->drawsPerWeek;
		?>
		<tr>
			<td id="p_<?= $oLottery->Record->id ?>" class="removePickedLottery"><i class="fa fa-trash-o"></i></td>
			<td class="picked_Name"><?= $tLotteries[$oLottery->Record->gameName]['DisplayName'] ?></td>
			<td><?= date( 'd M', strtotime( $oLottery->Record->playDate ) ) ?></td>
			<td>
				<div class='numbers'>
				<?php
					$wn = explode( ',', $oLottery->Record->ticketLine );
					$count = 1;
					foreach( $wn as $number ): ?>
						<div class="img-circle <?= $count > $bonus_number_color_check ? 'circle-gray':'circle-blue' ?>">
							<span><?= $number ?></span></div>
						<?php $count++;
					endforeach; ?>
				</div>
			</td>
			<td><span class="symbol" data-symbol="$"></span><?= $oLottery->Record->price ?></td>
		</tr>
		<?php
		$tickets[] = "{'name':'$_n', 'DPW':$_d, 'price':$_p}";
		$products[] = "{
			'name': '$_n',
			'brand': '".$_j."',
			'category':'numbers',
			'position':0
		}";
		$bonus_number_color_check = 0;
		$subTotal += $oLottery->Record->price;
	}
	?>
	<tr style='display:none'><td></td><td colspan='4'>
	<script>
		lessScrollPanel(1);

		var userDisc=<?= $userDiscount ?>, userDiscName='<?= str_replace("'","\\'",OGA_DISCNAME) ?>';
		var packageDisc=<?= json_encode($PACKAGE_DISCOUNT) ?>, packageDiscName='<?= str_replace("'","\\'",PACKAGE_DISCNAME) ?>';
		var Tickets=[ <?= implode(',',$tickets) ?> ];
		<?php if( GAENABLED && @$_SESSION['currPage']=='vcheckout' ) {/* GA Data layer */ ?>
		if(typeof(dataLayer)!='undefined') dataLayer.push({
			'pageType': 'Checkout',
			'auth': '<?= $oUser->getID() ? 1:0 ?>',
			'userId': '<?= $oUser->getID() ?>',
			'depositTotal': '<?= $oUser->getCurrentBalance() ?>', // total deposits by user
			'event': 'checkout',
			'ecommerce': {
				'checkout': {
					'actionField': { 'step': 1 },
					'products': [ <?= implode(',',$products) ?> ]
				}
			}
		});
		<?php } ?>
	</script>
	</td></tr>
	<?php
	unset($products,$_SESSION['currPage']);
	exit;
}

echo '';
