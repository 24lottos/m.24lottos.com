<?php
/**
* refresh-picked-packages.php - AJAX server for Packages table
*
* Load data for Packages: pending, active, previous
*
* @author     Sergey V Musenko <info@ws123.net>
* @copyright  2017 (c) 24Lottos
* @date       2017-04-21
* @version    0.1
*/


if( $mode!='active' && $mode!='previous' ) $mode = 'pending';
$tr = '';

$kitName = '';
$i = 0;
// logged in user
if( $oUser->getID() ) {
	$oLottery = new Lottery;
	$oLottery->getLotteryPackages( $oUser->getID(), $mode );
	if( $oLottery->numRows() ) { // packages founded
		$save = '';
		$dscTotal = 0;
		$subTotal = 0;
		while( $oLottery->nextRecord() ) {
			$dscPrice = abs($oLottery->Record->price - $oLottery->Record->oldPrice);
			$btn = $mode=='pending' ? '<td id="p_'.$oLottery->Record->id.'" class="removePackageLottery"><i class="fa fa-trash-o"></i></td>':'<td></td>';
			$kitName = $oLottery->Record->clubname.' Kit';
			$tr.= '
<tr>
	'.$btn.'
	<td>'.
		$oLottery->Record->clubname.' '.
		'<small>'.$oLottery->Record->months.'-months Kit '.
		'(<span class="symbol" data-symbol="$"></span>'.$oLottery->Record->price.' per month, '.
		'billed for '.$oLottery->Record->months.' months)</small>'.
	'</td>
	<td><span class="symbol" data-symbol="$"></span>'.number_format($oLottery->Record->oldPrice*$oLottery->Record->months,2).'</td>
</tr><tr>
	<td colspan="2">Discount</td>
	<td>-<span class="symbol" data-symbol="$"></span>'.number_format($oLottery->Record->oldPrice*$oLottery->Record->months-$oLottery->Record->totalPrice,2).'</td>
</tr>';
			$subTotal+= $oLottery->Record->totalPrice;
			$dscTotal+= $dscPrice*$oLottery->Record->months;
			$save = $oLottery->Record->save; // A PATCH!!!
			$i++;
		}
		// total line
		$tr.= '
<tr class="hidden">
	<td colspan="3" class="pkgSummaryHTML">
		<div class="alert success" id="has-coupon2">
			<div class="span8 pull-left">'.$save.' Off '.
			($i==1 ? $kitName:'Membership Club').
			' Bonus</div>
			<div class="span4 pull-right" id="confirm_total">Total <span class="symbol" data-symbol="$"></span>'.number_format($subTotal,2).'</div>
			<div class="clearfix"></div>
		</div>
	</td>
</tr>';
	}

// NOT logged in user
} else if( isset($_SESSION['createMembership']) && count($_SESSION['createMembership']) ) { // not logged in
/* NOT SUPPORTED!
	$save = '';
	$dscTotal = 0;
	$subTotal = 0;
	foreach( $_SESSION['createMembership'] as $id=>$club ) {
		$dscPrice = abs($club['price'] - $club['oldPrice']);
		$btn = $mode=='pending' ? '<td id="p_'.$id.'" class="removePackageLottery"><i class="fa fa-trash-o"></i></td>':'<td></td>';
		$kitName = $MembershipClub[$club['clubid']]['fullName'].' Kit';
		$tr.= '
<tr>
	'.$btn.'
	<td>'.
		$MembershipClub[$club['clubid']]['fullName'].' '.
		'<small>'.$club['months'].'-months Kit '.
		'(<span class="symbol" data-symbol="$"></span>'.$club['price'].' per month, '.
		'billed for '.$club['months'].' months)</small>'.
	'<td><span class="symbol" data-symbol="$"></span>'.number_format($club['oldPrice']*$club['months'],2).'</td>
</tr><tr>
	<td colspan="2">Discount</td>
	<td>-<span class="symbol" data-symbol="$"></span>'.number_format($dscPrice*$club['months'],2).'</td>
</tr>';
		$subTotal+= $club['totalPrice'];
		$dscTotal+= $dscPrice*$club['months'];
		$save = $club['save']; // A PATCH!!!
		$i++;
	}
	$tr.= '
<tr class="hidden">
	<td colspan="3" class="pkgSummaryHTML">
		<div class="alert success" id="has-coupon2">
			<div class="span8 pull-left">'.$save.' Off '.
			($i==1 ? $kitName:'Membership Club').
			' Bonus</div>
			<div class="span4 pull-right" id="confirm_total">Total <span class="symbol" data-symbol="$"></span>'.number_format($subTotal,2).'</div>
			<div class="clearfix"></div>
		</div>
	</td>
</tr>';
*/
}
echo $tr;
