<?php
/*
 * This is Ajax server script for calculating CashEnvoy Signature
 *
 * parms:  transref=string(time), amount=string(float)
 * return: signature as string
 */
if( session_status() == PHP_SESSION_NONE ) session_start();

// POST parms
$transref  = trim( $_POST['transref'] );
$amountUSD = (float)$_POST['amountUSD'];
$amount    = (float)$_POST['amount'];
//die('test: '.CASHENVOY_KEY." - $transref - $amount");
if( empty($transref) || $amount <= 0 ) die('error');
// save amount in session
$_SESSION['PAYCASHENVOY'] = [
	'transref'  => $transref,
	'amountUSD' => $amountUSD,
	'amount'    => $amount,
];

// log - we started payment process
// @testWithLog( '--- Payment Start --- UserID='.$_SESSION['logged_id'].', Amount=$'.$amountUSD.', Method=CashEnvoy' );

// generate CashEnvoy signature
echo hash_hmac( 'sha256', CASHENVOY_KEY.$transref.$amount, CASHENVOY_KEY, false );
