<?php
$data = new stdClass();
$data->name = $_POST['name'];
$data->phone = $_POST['phone'];
$data->subject = $_POST['subject'];
$data->email = $_POST['email'];
$data->question = $_POST['question'];
$oEmail = new Email;
if( $oEmail->supportMessage( $data ) && $oEmail->supportMessageCustomerNotification( $data ) ):
	echo MSG_SENT;
	$oSupport = new Support;
	$lastID = $oSupport->create( $oSupport->clean( $_POST, new Clean ) );
	$oSupport->advanceCreate( $oSupport->cleanSupportSlaveTable( $_POST, new Clean, $lastID ), 'lotto_support_answers' );
else:
	echo MSG_FAIL;
endif;
