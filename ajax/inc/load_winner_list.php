<?php
// finding total records
$totalRecordCount = ( new Lottery )->winnersFront()->numRows();

// total filtered records . ie. we are not including dranw result for the page which includes limit. but we are encluding entire result after search + respective paging
$totalFilteredRecords = new Lottery;
$totalFilteredRecordCount = $totalFilteredRecords->winnersFront()->numRows();

$oAjax = new Lottery;

$clause = "";
$limit = "";

if( isset($order[0]['column']) && $_SESSION['firs_winners_view'] != true ) {

    switch ( $order[0]['column'] ) {

        case 0:
            $clause .= " ORDER BY a.gameName ".$order[0]['dir'];
            $totalFilteredRecordCount = $totalFilteredRecords->winnersFront( $clause )->numRows();
            break;

        case 1:
            $clause .= " ORDER BY a.playDate ".$order[0]['dir'];
            $totalFilteredRecordCount = $totalFilteredRecords->winnersFront( $clause )->numRows();
            break;

        case 3:
            $clause .= " ORDER BY a.winningamtInUSD  ".$order[0]['dir'];
            $totalFilteredRecordCount = $totalFilteredRecords->winnersFront( $clause )->numRows();
            break;
    }
} else {
    $clause .= " ORDER BY a.playDate  desc";
    $totalFilteredRecordCount = $totalFilteredRecords->winnersFront( $clause )->numRows();
    $_SESSION['firs_winners_view'] = false;
}

if( $length > 0 ):
	$clause .= " LIMIT $start, $length";
endif;


$output = array(
	'recordsTotal' => $totalRecordCount,
	'recordsFiltered' => $totalFilteredRecordCount,
	'data' => null,
);

if( $oAjax->winnersFront( $clause )->numRows() ) {
//	$unMaxLen = 5;
	while( $oAjax->nextRecord() ) {
        $un = substr( $oAjax->Record->email, 0, strpos( $oAjax->Record->email, "@" ) );
//		if( strlen($un)>$unMaxLen ) $un = substr( $un, 0, $unMaxLen ).'&hellip;';
		$output['data'][] = [ /* General output */
			'DT_RowId'    => $oAjax->Record->id,
			'lotteryname' => $oAjax->Record->gameName,
			'enddatetime' => date( 'd M', strtotime( $oAjax->Record->playDate ) ),
			'name'        => substr( shortName( $un, 2, 3 ), 0, 8 ),
			'winningamt'  => "$".number_format( $oAjax->Record->winningamtInUSD, 2, '.', ',' ),
		];
	}
	echo json_encode( $output );
} else {
	$output = array(
		'recordsTotal' => 0,
		'recordsFiltered' => 0,
		'data' => array(
			'lotteryname' => null,
			'enddatetime' => null,
			'name' => null,
			'winningamt' => null,
		),
	);
	echo json_encode( $output );
	exit();
}
