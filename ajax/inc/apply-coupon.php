<?php

$oLottery = new Lottery;
$oLottery->removeCoupon( session_id() );
$oLottery->areNumbersPicked( session_id() );

if( $oLottery->numRows() ):
	$subTotal = 0;
	while( $oLottery->nextRecord() ):
		$subTotal += $oLottery->Record->price;
	endwhile;

	$o = new Coupon;
	if( $o->view( "WHERE couponCode = '$coupon' AND status = 'Y'" )->numRows() ):
		if( $o->singleRecord()->Record->appliedBy == 'P' ):
			$discount = ($subTotal * $o->Record->couponBenefit) / 100;
		else:
			$discount = $o->Record->couponBenefit;
		endif;
		$json['subTotal'] = round( $o->subTotal = $subTotal, 2 );
		$json['discount'] = round( $o->discount = $discount, 2 );
		$json['grandTotal'] = $o->grandTotal = round( $subTotal - $discount, 2 );
		$o->registerCouponUsage( $o );
		echo json_encode( $json );
	else:
		echo json_encode( [ 'Invalid' ] );
	endif;
endif;
