<?php
$oBitpay = new Bitpay();
if( ENVIRONMENT ) {
    require_once( '../vendor/autoload.php' );
} else {
    require_once( dirname(dirname(dirname(__DIR__))) .'/vendor/autoload.php' );
}
echo $oBitpay->setAmount()->createInvoice();
