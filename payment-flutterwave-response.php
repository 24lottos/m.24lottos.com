<?php
// prevent cache!
header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Expires: Sun, 01 Jan 2017 00:00:00 GMT');
header('Pragma: no-cache');

require_once('./config/config.php');
require_once(__DIR__.'/app/bootstrap.php');
(new \Src\Payment\PFlutterwave())->response();
