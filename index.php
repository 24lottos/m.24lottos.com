<?php
	include('inc/header.php');
?>

<div id="hello">
    <div class="container whitebg">
		<?php /*include('inc/nav.php') -- it includes in inc/header.php !!! */ ?>

    	<?php isset($p) ? include("pages/$p.php") : include("pages/home.php") ?>

    </div> <!-- /container -->
</div><!-- /hello -->

<?php

include('inc/footer.php');

// affiliate-pro - run it from a last line!
if( AFFPRO_PATH ) {
	$_include = AFFPRO_PATH.AFFPRO_TRACK_REF;
	if( is_file($_include) ) @include_once( $_include );
}
