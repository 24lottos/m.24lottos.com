/**
 * Created by Alex on 03.05.2017.
 */
function loadJS( src ) {
    var js = document.createElement('script');
    js.src = src;
    var head = document.getElementsByTagName('head')[0];
    head.appendChild(js);
}
