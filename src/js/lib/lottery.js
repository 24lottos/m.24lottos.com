var NT = {
	errorMessage: 'Error Occured! Please try again Later!',
	game_split: 0,
	balls: 0,
	gameName: '',
	type: 3,
	ballsMaxNum: 0,
	extraBallsMaxNum: 0,
	extraBalls: 0,
	jackpot: 0,
	drawdate: 0,
	price: 0,
	currency: '',
	pdtid: 0,
	pickCounter: 0,
	pickCounterExtra: 0,
	forecasted: 0,
	list: new Array(),
	listExtra: new Array(),
	_setVars: function () {
		NT.game_split = $(".curlottery").attr('id').split('_');
		NT.gameName = $("#featured_lottery_" + NT.game_split[2]).attr('data-gamename');
		NT.jackpot = $("#featured_lottery_" + NT.game_split[2]).attr('data-jackpot');
		NT.drawdate = $("#featured_lottery_" + NT.game_split[2]).attr('data-drawdate');
		NT.jackpot_encoded = $("#featured_lottery_" + NT.game_split[2]).attr('data-jackpot-encoded');
		NT.balls = parseInt($("#featured_lottery_" + NT.game_split[2]).data('balls'));
		NT.extraBalls = parseInt($("#featured_lottery_" + NT.game_split[2]).data('extraballs'));
		NT.ballsMaxNum = parseInt($("#featured_lottery_" + NT.game_split[2]).data('ballsmaxnum'));
		NT.extraBallsMaxNum = parseInt($("#featured_lottery_" + NT.game_split[2]).data('extraballsmaxnum'));
		NT.price = $("#featured_lottery_" + NT.game_split[2]).data('price');
		NT.currency = $("#featured_lottery_" + NT.game_split[2]).data('currency');
		NT.pdtid = parseInt($("#featured_lottery_" + NT.game_split[2]).data('pdtid'));
	},
	_resetVars: function (_this) {
		NT.list=new Array();
		NT.listExtra=new Array();
		NT.pickCounter=NT.pickCounterExtra=0;
		NT.game_split = $(_this).attr('id').split('_');
		NT.gameName = $(_this).attr('data-gamename');
		NT.jackpot = $(_this).attr('data-jackpot');
		NT.drawdate = $(_this).attr('data-drawdate');
		NT.jackpot_encoded = $(_this).attr('data-jackpot-encoded');
		NT.balls = parseInt($(_this).data('balls'));
		NT.extraBalls = parseInt($(_this).data('extraballs'));
		NT.ballsMaxNum = parseInt($(_this).data('ballsmaxnum'));
		NT.extraBallsMaxNum = parseInt($(_this).data('extraballsmaxnum'));
		NT.price = $(_this).data('price');
		NT.currency = $(_this).data('currency');
		NT.pdtid = parseInt($(_this).data('pdtid'));
	},
	_extraBalls: function () {
		if (NT.extraBalls) {
			$("#extra_balls").html(NT._genBallsExtra($("#featured_lottery_" + NT.game_split[2]).data('extraballsmaxnum'))).show();
		} else {
			$("#extra_balls").html('').hide();
		}
	},
	_selectedBalls: function () {
		html = '';
		for (var i = 1; i <= NT.balls; i++) {
			html += '<div id="ball_' + i + '" class="img-circle circle-active">';
			html += '<h3></h3>';
			html += '</div>';
		}
		for (i = 1; i <= NT.extraBalls; i++) {
			html += '<div id="extra_ball_' + i + '" class="img-circle circle-active bonus">';
			html += '<h3></h3>';
			html += '</div>';
		}
		$("#selected_balls").html(html);
	},

	pickTickets: function () {
		$(".dev-lottery-option").click(function () {
			if($(this).attr('id').split('_')[2]==NT.game_split[2]) return;
			NT._resetVars(this);
			NT._selectedBalls();
			$(".featured_lottery").each(function () {
				var curID = $(this).attr("id");
				$("#" + curID).removeClass('curlottery');
			});
			$(this).addClass('curlottery');
			$("#jackpot").html("Jackpot: " + NT.jackpot);
			$(".btn3").css("opacity","0");
			updateJackpotPicks();
			$("#regular_balls").html(NT._genBalls($(this).data('ballsmaxnum')));
			$('#lottery_select_name .select_game').html(NT.gameName);
			$('#lottery_select_name').html($("#featured_lottery_" + NT.game_split[2]).html());
			$('#jackpot_price').html("Jackpot: " + NT.jackpot);
			NT.countdown();
			NT._extraBalls();
			NT._selectedBalls();
		});
	},
	defaultLottery: function () {
		NT._setVars();
		$("#jackpot").html("Jackpot: " + NT.jackpot);
		updateJackpotPicks();
		$("#regular_balls").html(NT._genBalls($("#featured_lottery_" + NT.game_split[2]).data('ballsmaxnum')));
		NT._extraBalls();
		NT._selectedBalls();
	},
	_genBalls: function (until) {
		var html = '';
		for (var i = 1; i <= until; i++) {
			html += '<div id="circle_' + i + '" class="img-circle circle-no">';
			html += '<h3 class="pickNum" data-num="' + i + '">' + i + '</h3>';
			html += '</div>';
		}
		html += '<div style="clear: both;"></div>';
		return html;
	},
	_genBallsExtra: function (until) {
		var html = '';
		for (var i = 1; i <= until; i++) {
			html += '<div id="circle_' + i + '" class="img-circle circle-no">';
			html += '<h3 class="pickNumExtra" data-num="' + i + '">' + i + '</h3>';
			html += '</div>';
		}
		html += '<div style="clear: both;"></div>';
		return html;
	},
	_updateSelected: function () {
		for (var i = 1; i <= 10; i++) {
			$("#ball_" + i + " h3").html('');
		}
		for (var i = 1; i <= NT.list.length; i++) {
			$("#ball_" + i + " h3").html(NT.list[i - 1]);
		}
		for (var i = 1; i <= 5; i++) {
			$("#extra_ball_" + i + " h3").html('');
		}
		for (var i = 1; i <= NT.listExtra.length; i++) {
			$("#extra_ball_" + i + " h3").html(NT.listExtra[i - 1]);
		}

	},
	pickBalls: function () {
		$(".pickNum").live('click', function () {
			if (NT.pickCounter == NT.balls) return false;
			$(this).parent('div').addClass('numberTaken');
			if ($.inArray(parseInt($(this).data('num')), NT.list) === -1) {
				NT.list.push($(this).data('num'));
				NT.pickCounter++;
			}
			NT._updateSelected();
			updateJackpotPicks();
			NT.forecasted=0;
		});
		$(".pickNumExtra").live('click', function () {
			if (NT.pickCounterExtra == NT.extraBalls) return false;
			$(this).parent('div').addClass('numberTakenExtra');
			if ($.inArray(parseInt($(this).data('num')), NT.listExtra) === -1) {
				NT.listExtra.push($(this).data('num'));
				NT.pickCounterExtra++;
			}
			NT._updateSelected();
			updateJackpotPicks();
		});

	},
	dropBalls: function () {
		$(".numberTaken .pickNum").live('click', function () {
			$(this).parent('div').removeClass('numberTaken');
			var index = NT.list.indexOf(parseInt($(this).html()));
			NT.list.splice(index, 1);
			NT._updateSelected();
			NT.pickCounter--;
			updateJackpotPicks();
		});
		$(".numberTakenExtra .pickNumExtra").live('click', function () {
			$(this).parent('div').removeClass('numberTakenExtra');
			var index = NT.listExtra.indexOf(parseInt($(this).html()));
			NT.listExtra.splice(index, 1);
			NT._updateSelected();
			NT.pickCounterExtra--;
			updateJackpotPicks();
		});

	},
	_iFortuneLogic: function () {
		var rand_num = null;
		NT._clear();
		while (NT.pickCounter < NT.balls) {
			rand_num = Math.floor((Math.random() * NT.ballsMaxNum) + 1);
			if ($.inArray(rand_num, NT.list) === -1 && rand_num != 0 && rand_num <= NT.ballsMaxNum) {
				$('#regular_balls #circle_' + rand_num).addClass('numberTaken');
				NT.list.push(rand_num);
				NT.pickCounter++;
			} else {
				NT.pickCounter = NT.pickCounter;
			}
		}
		NT._updateSelected();
		while (NT.pickCounterExtra < NT.extraBalls) {
			rand_num = Math.floor((Math.random() * NT.extraBallsMaxNum) + 1);
			if ($.inArray(rand_num, NT.listExtra) === -1 && rand_num != 0 && rand_num <= NT.extraBallsMaxNum) {
				$('#extra_balls #circle_' + rand_num).addClass('numberTakenExtra');
				NT.listExtra.push(rand_num);
				NT.pickCounterExtra++;
			} else {
				NT.pickCounterExtra = NT.pickCounterExtra;
			}
		}
		NT._updateSelected();
		updateJackpotPicks();
		NT.forecasted=1;
	},
	iFortune: function () {
		$("#autoPick").click(function () {
			NT._iFortuneLogic();
		});
	},
	_clear: function () {
		for (var i = 1; i <= NT.ballsMaxNum; i++) {
			$('#regular_balls #circle_' + i).removeClass('numberTaken');
		}
		for (var i = 1; i <= NT.extraBallsMaxNum; i++) {
			$('#extra_balls #circle_' + i).removeClass('numberTakenExtra');
		}
		NT.list = new Array;
		NT.listExtra = new Array;
		NT.pickCounter = 0;
		NT.pickCounterExtra = 0;
		NT._selectedBalls();//refreshing selected balls again
	},
	countdown: function () {
		var diffInMinutesInUTC = new Date().getTimezoneOffset() *60 *1000; // client Time Zone, in ms
		$('[data-countdown]').each(function () {
			var $this = $(this), finalDate = new Date( new Date($this.data('countdown').replace(/-/g,'/')).getTime() - diffInMinutesInUTC );
			$this.countdown(finalDate, function (event) {
				$this.html("Time to draw: " + event.strftime('%D days %H:%M:%S'));
			});
		});
		$('[data-cntdwn]').each(function () {
			var $this = $(this), finalDate = new Date( new Date($this.data('cntdwn').replace(/-/g,'/')).getTime() - diffInMinutesInUTC );
			$this.countdown(finalDate, function (event) {
				$this.html(event.strftime('%D days %H:%M:%S'));
			});
		});
		$('#jackpot_draw').each(function () {
			var $this = $(this), finalDate = new Date( new Date(NT.drawdate.replace(/-/g,'/')).getTime() - diffInMinutesInUTC );
			$this.countdown(finalDate, function (event) {
				$this.html("Time to draw: " + event.strftime('%D days %H:%M:%S'));
			});
		});
        $('[data-lotterycountdown]').each(function() {
            var $this = $(this), finalDate = new Date( new Date($this.data('lotterycountdown').replace(/-/g,'/')).getTime() - diffInMinutesInUTC );
            $this.countdown(finalDate, function(event) {
                $this.html(event.strftime('<div class="lotto-count" style="margin-right:10px"><span>%-d</span><br> <div>days</div> </div> '
                    + '<div class="lotto-count"><span>%-H</span><br><div>hours</div></div>'
                    + '<div class="lotto-count"><span>%-M</span><br><div>minutes</div></div>'
                    + '<div class="lotto-count"><span>%-S</span><br><div>seconds</div></div>'));
            });
        });
	},
	play: {
		_checkBalls: function () {
			return NT.list.length == NT.balls ? true : false;
		},
		_checkExtraBalls: function () {
			return NT.listExtra.length == NT.extraBalls ? true : false;
		},
		_refresh: function (clear, animate) {
			$('#loading-overlay').show(0);
			$.ajax({
				url: mlotto.baseURLm + "ajax/index.php",
				type: "POST",
				cache: false,
				data: {
					'action': 'refresh-picked-numbers',
					page: $("#yourLotteryPicksTable").data('page')
				}, // all form fields
				success: function (data) {
					data=data.trim();
					if(data=='') {
						$("#has-coupon").hide();
						$("#checkout").hide();
						$("#yourLotteryPicksTable tbody").html('<tr><td colspan="4" align="left">'+NO_TICKETS+'</td><td></td></tr>');
						$("#pendTicNum").hide();
						$(".duration").hide();
						if(lessScrollPanelState) lessScrollPanel(false);
					} else {
						$("#yourLotteryPicksTable tbody").html(data);
						$("#has-coupon").show();
						$("#checkout").show();
						$(".duration").show();
						if ($("#has-coupon").length) NT.coupon.before();
						// update pendTicNum icon
						var l=$(".removePickedLottery").length;
                        if(l) {
                            // $("#pendTicNum span").removeClass('reddot-animation');
                            // $("#pendTicNum #rectangle").removeClass('ticket-animation');
                            $("#pendTicNum span").html(l);
                            $("#pendTicNum").show('slow');
                            // if(animate) {
                            //     $("#pendTicNum span").addClass('reddot-animation');
                            //     $("#pendTicNum #rectangle").addClass('ticket-animation');
                            // }
                        }
					}
					if (clear) {
						NT._clear();
					}
					$('#loading-overlay').hide(0);

				} // success
			}).fail(function () {
				alert(NT.errorMessage);
				$('#loading-overlay').hide(0);
			}); // ajax

		},
		lottery: {
			_submit: function () {
				$('#loading-overlay').show(0);
				$.ajax({
					url: mlotto.baseURLm + "ajax/index.php",
					type: "POST",
					cache: false,
					dataType: 'json',
					data: {
						'action': 'send-picked-numbers',
						'ticketLine': JSON.stringify(NT.list),
						'gameID': NT.game_split[2],
						'gameName': NT.gameName,
						'typeID': NT.type,
						'jackpot': NT.jackpot_encoded,
						'price': NT.price,
						'currency': NT.currency,
						'productID': NT.pdtid,
						'ticketLineExtra': JSON.stringify(NT.listExtra)
					} // all form fields
				}).done(function (data) {
					if (data.result == "success") {
						NT.play._refresh(true, true);
						if (typeof(dataLayer) != 'undefined') {
							//GA Data layer
							dataLayer.push({
								'event': 'addToCart',
								'eventCategory': 'numbers',
								'eventAction': 'add',
								'eventLabel': 'ticket',
								'ecommerce': {
									'currencyCode': 'USD', // ticket price currency code
									'add': {
										'products': [{
											'name': NT.gameName, //loterry name
											'price': NT.price, //ticket price
											'brand': NT.jackpot, // Jackpot value in USD
											'category': 'numbers', //for numbers always =numbers
											'variant': NT.forecasted ? 'iforecast':'default',
											'quantity': 1 // always 1
										}]
									}
								}
							}); //GA Data layer /
						}
					} else {
						alert(NT.errorMessage);
						$('#loading-overlay').hide(0);
					}
				}).fail(function () {
					alert(NT.errorMessage);
					$('#loading-overlay').hide(0);
				}); // ajax
				NT._clear();
			},
			add: function () {
				$("#playLottery").click(function () { // PLAY button click ---
					onPlay=true; // no scrollTop in updateJackpotPicks()
					if(!lessScrollPanelState) lessScrollPanel(true);
					if( NT.play._checkBalls() && NT.play._checkExtraBalls() ) {
						NT.play.lottery._submit();
					} else {
						NT._iFortuneLogic();
						NT.play.lottery._submit();
					}
				});
				NT.play._refresh(true, false);
			},
			disable_checkout_btn: function () {
				if ($('#checkout_btn').hasClass('btn-red')) {
					$('#checkout_btn').addClass('btn-gray');
					$('#checkout_btn').removeClass('btn-red');
				}
			},
			enable_checkout_btn: function () {
				if ($('#checkout_btn').hasClass('btn-gray')) {
					$('#checkout_btn').addClass('btn-red');
					$('#checkout_btn').removeClass('btn-gray');
				}
			},
			remove: function () {
				$(".removePickedLottery").live('click', function () {
					var splitID = $(this).attr('id').split('_');
					$('#loading-overlay').show(0);
					$.ajax({
						url: mlotto.baseURLm + "ajax/index.php",
						type: "POST",
						cache: false,
						data: {
							'action': 'delete-picked-numbers',
							'tempID': splitID[1],
						},
						success: function (data) {
							NT.play._refresh(false, true);
							$("#confirm_total").html('Total $0')
							if (typeof(dataLayer) != 'undefined') {
								//GA Data layer
								dataLayer.push({
									'event': 'lottos',
									'event': 'removeFromCart',
									'eventCategory': 'numbers',
									'eventAction': 'remove',
									'eventLabel': 'ticket',
									'ecommerce': {
										'remove': {
											'products': [{
												'name': NT.gameName, //loterry name
												'price': NT.price, //ticket price
												'brand': NT.jackpot, // Jackpot value in USD
												'category': 'numbers', //for numbers always =numbers
												'variant': 'default', // if user use bonus number, if no - default
												'quantity': 1 // always 1
											}]
										}
									}
								});
								//GA Data layer /
							}
						} // success
					}).fail(function () {
						alert(NT.errorMessage);
						$('#loading-overlay').hide(0);
					});
					; // ajax
				});
			}
		}
	},
	coupon: {
		before: function(o){ // SHOW tickets total!!! do not remove
			var discount='', d=0, total='', t=0, s=0, tpr=0, dpw=0;
			// use vars from refresh-picked-numbers.php:
			// DURATION, userDisc, userDiscName, packageDisc, packageDiscName
			// and Tickets[{name,DPW,price}] array
			if(typeof(Tickets)=='object' && Tickets.length) { // we've got tickets!
				if(typeof(DURATION)!='number'||DURATION<0) DURATION=0; // in weeks, 0 - single order
				// get discount
				if(DURATION) { // 1 week or more
					d=packageDisc[DURATION];
					discount=d+'% '+packageDiscName;//+' weeks:'+DURATION;
				} else { // single order
					d=userDisc;
					discount=d+'% '+userDiscName;//+' weeks:'+DURATION;
				}
				if(!d) discount='';
				// cals total with discount
				for(i  in Tickets) {
					tpr=Tickets[i]['price'];
					dpw=Tickets[i]['DPW'];
					if(DURATION) sub=tpr*dpw*DURATION; // subtotal for duration
					else sub=tpr; // subtotal for single
					Tickets[i]['disc']=sub*d/100; // discount per line
					s+=sub;
					t+=sub-Tickets[i]['disc']; // total sum
				}
				total='Total $'+moneyFmt(t);
			}
			$("#has-coupon div.span8").html(discount);
			$("#has-coupon div.span4").html(total);
		},
/*		apply: function () {
			$("#has-coupon input[type='button']").click(function () {
				var coupon = $("#has-coupon input[type='text']").val().trim();
				if (coupon != '') {
					$.ajax({
						url: baseURL + "ajax/index.php",
						type: "POST",
						cache: false,
						data: {
							action: 'apply-coupon',
							coupon: coupon
						},
						success: function (data) {
							var data = $.parseJSON(data);
							if (data[0] != 'Invalid') {
								if ($("#yourLotteryPicksTable tr").is('.last')) {
									$("#yourLotteryPicksTable tr.last").replaceWith("<tr class='last'><td colspan='5' align='right'>Discount</td><td>$" + data.discount + "</td></tr>");
								} else {
									$("#yourLotteryPicksTable tr:last").after("<tr class='last'><td colspan='5' align='right'>Discount</td><td>$" + data.discount + "</td></tr>");
								}
								$("#confirm_total").html('Total $' + data.grandTotal);
							} else {
								$("#invalidCouponModal").show();
								//alert("Invalid Coupon Applied!");
							}
						} // success
					}); // ajax
				}
			});
		}*/
	},
	init: function () {
		this.pickTickets();
		this.pickBalls();
		this.dropBalls();
		if ($('*').hasClass('curlottery')) this.defaultLottery();
		this.countdown();
		this.iFortune();
		this.play.lottery.add();
		this.play.lottery.remove();
		//this.coupon.apply();
		// less scroll panel
		$('.lessScroll .add').click(function(e){ e.preventDefault(); lessScrollPanel(false); });
		$('.lessScroll .play').click(function(e){ e.preventDefault(); $('#playLottery').trigger('click'); });
	}
}

var onPlay=false; // true means- no scrollTop in updateJackpotPicks()
function updateJackpotPicks() {
	var t="Pick ", title='top', cmltd='<i class="fa fa-check-circle"></i> Completed, click PLAY!';
	if(!NT.pickCounter) t+=NT.balls+" numbers:"; // new
	else if(NT.pickCounter>=NT.balls) { // full set
		var top_pos=0;
		if(NT.extraBalls) {
			top_pos=$('.regular_box').offset().top;
			if(!NT.pickCounterExtra) {
				t+=NT.extraBalls+" Bonus Number"+(NT.extraBalls>1?'s':'')+':';
				top_pos =$('.extra_box').offset().top;
			} else if(NT.pickCounterExtra>=NT.extraBalls)
				t=cmltd;
			else {
				t+=(NT.extraBalls-NT.pickCounterExtra)+" more Bonus Number"+(NT.extraBalls-NT.pickCounterExtra>1?'s':'')+':';
				top_pos =$('.extra_box').offset().top;
			}
			$("#extra_picks").html(t);
			if(NT.pickCounterExtra<NT.extraBalls) title='bottom';
		} else t=cmltd;
		if(!onPlay && top_pos && NT.pickCounter+NT.pickCounterExtra) {
			$("html, body").animate({scrollTop:top_pos}, 500);
		}
	} else // part only
		t+=(NT.balls-NT.pickCounter)+" more:";
	if(title=='top') { $("#regular_picks").html(t); $("#extra_picks").hide(); }
	else { $("#regular_picks").html(''); $("#extra_picks").show(); }
	onPlay=false;
}

var Package = {
	tblID: "NOTyourLotteryPackageTable",
	init: function() {
		// load data to all tables with tblID
		$("#"+this.tblID).each(function(){
			var tbl=$(this), mode=$(this).data('mode');

			$('#loading-overlay').show(0);
			$.ajax({
				url: "/ajax/index.php",//baseURL
				type: "POST",
				cache: false,
				data: {
					'action': 'refresh-picked-packages',
					'mode': mode
				},
				success: function (data) {
					data=data.trim();
					if(data=='') {
						$(".pk_"+mode+"_wrap").hide();
					} else {
						$(tbl).find('tbody').html(data);
						var Summary=$('.pkgSummaryHTML').html();
						if(Summary) $('#pkgSummary').html(Summary);
						$("#pkcheckout_"+mode).show();
						$(".pk_"+mode+"_wrap").show();
					}
				}
			}).always( function() { $('#loading-overlay').hide(0); });
		});
		this.remove();
	},
	remove: function() {
		// delete LOTTERY Package ---
		$(".removePackageLottery").live('click', function(){
			var id=$(this).attr('id').split('_')[1];
			var tr=$(this).parent('tr'), wrap=$(this).closest('div');
			$('#loading-overlay').show(0);
			$.ajax({
				url: "/ajax/index.php",//baseURL
				type: "POST",
				cache: false,
				data: {
					'action': 'delete-picked-package',
					'id': id
				},
				success: function (data) {
					if(data=='ok') {
						if($(".removePackageLottery").length==1) {
							$(wrap).fadeOut(); // hide empty table
						} else $(tr).remove();
					} else { alert('Error, sorry! '+data)}
				}
			}).always( function() { $('#loading-overlay').hide(0); });
		});
	}
};

$(document).ready(function () {
/*	// I dont know what is this for...
	$('#autoPick.yellow-btn').bind('touchstart touchend', function () {
		$(this).toggleClass('green_hover_effect');
		var top_pos = $('#circle_22').offset().top
		$("html, body").animate({scrollTop:top_pos}, 600);
	});
	$('#playLottery.btn-green').bind('touchstart touchend', function () {
		$(this).toggleClass('orange_hover_effect');
	}); */
	NT.init();
	Package.init();
});

// less scroll panel control
var lessScrollPanelState=false;
function lessScrollPanel(mode) {
	var top_pos = $('#numbersWrap').offset() ? $('#numbersWrap').offset().top:0;
	if(mode) {
		lessScrollPanelState=true;
		$('#numbersWrap').addClass('rollUp');
		$('.lessScroll').fadeIn();
		$("html, body").animate({scrollTop:top_pos}, 800);
		$("html").css('padding-bottom','0px');
		$('.v-gradient-white').hide();
	} else {
		lessScrollPanelState=false;
		onPlay=true; updateJackpotPicks(); // restore "Pick N numbers" heading
		$('#numbersWrap').removeClass('rollUp');
		$('.lessScroll').fadeOut();
		$("html, body").animate({scrollTop:top_pos}, 800);
		$("html").css('padding-bottom','60px');
		$('.v-gradient-white').show();
	}
}
