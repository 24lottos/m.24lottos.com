/*
 * classie - class helper functions
 * from bonzo https://github.com/ded/bonzo
 *
 * classie.has( elem, 'my-class' ) -> true/false
 * classie.add( elem, 'my-new-class' )
 * classie.remove( elem, 'my-unwanted-class' )
 * classie.toggle( elem, 'my-class' )
 */

/*jshint browser: true, strict: true, undef: true */

(function (window) {

	'use strict';

// class helper functions from bonzo https://github.com/ded/bonzo

	function classReg(className) {
		return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
	}

// classList support for class management
// altho to be fair, the api sucks because it won't accept multiple classes at once
	var hasClass, addClass, removeClass;

	if ('classList' in document.documentElement) {
		hasClass = function (elem, c) {
			return elem.classList.contains(c);
		};
		addClass = function (elem, c) {
			elem.classList.add(c);
		};
		removeClass = function (elem, c) {
			elem.classList.remove(c);
		};
	}
	else {
		hasClass = function (elem, c) {
			return classReg(c).test(elem.className);
		};
		addClass = function (elem, c) {
			if (!hasClass(elem, c)) {
				elem.className = elem.className + ' ' + c;
			}
		};
		removeClass = function (elem, c) {
			elem.className = elem.className.replace(classReg(c), ' ');
		};
	}

	function toggleClass(elem, c) {
		var fn = hasClass(elem, c) ? removeClass : addClass;
		fn(elem, c);
	}

	window.classie = {
		// full names
		hasClass: hasClass,
		addClass: addClass,
		removeClass: removeClass,
		toggleClass: toggleClass,
		// short names
		has: hasClass,
		add: addClass,
		remove: removeClass,
		toggle: toggleClass
	};

})(window);


var Common = {
	hLink: function () {
		$(".link").bind('click', function (e) {
			e.preventDefault();
			var Urlink = $(this).data("link");
			var target = $(this).data("target");
			if (target == '_blank') {
				window.open(Urlink, '_blank');
			} else {
				window.location = Urlink;
			}

		});
	},
	hLink2: function () {
		$(".link2").bind('click', function (e) {
			//e.preventDefault();
			var Urlink = $(this).data("link");
			window.open(Urlink, '_blank');
			;

		});
	},
	hLink3: function () {
		$("#checkout_btn").bind('click', function (e) {
			e.preventDefault();
			if ($('#checkout_btn').hasClass('btn-gray')) {
				alert("First select your numbers and click on PLAY button!");
			} else {
				var Urlink = $(this).data("link");
				window.location = Urlink;
			}

		});
	}, bannerLink: function () {
		$(".bannerLink").bind('click', function (e) {
			//e.preventDefault();
			var Urlink = $(this).data("link");
			$.ajax({
				url: "ajax/",
				type: "POST",
				cache: false,
				data: {
					'action': 'update_clicks',
					'adID': $(this).data("number")
				}, // all form fields
				success: function (data) {
					window.open(Urlink, '_blank');
				} // success
			}); // ajax
		});
	},
	changeCurrency: function () {
		$(".flag").bind('click', function (e) {
			e.preventDefault();
			var currency = $(this).attr('id');
			var loadURL = $(location).attr('href');
			$.ajax({
				type: "POST",
				dataType: "text",
				url: mlotto.baseURLm + "ajax/",
				cache: false,
				data: {
					newCurrency: currency,
					//action: "change_currency" // disabling this as dont want shortcut to change preferred currency
					// can enable it in future but not sure when
				},
				success: function (theResponse) {
					//window.location = loadURL; // enable this as well when un comment action
				}
			});	//ajax
		});
	},
	CustomerSupport: function () {
		$("#btnAddSupport").click(function () {
			$("#frmAddSupport").validate();
		});
	},
	CustomerSupportReply: function () {
		$("#btnAddReply").click(function () {
			$("#frmAddReply").validate();
		});
	},
	resendActivationMail: function () {
		$(".resendActivationMail").bind('click', function (e) {
			e.preventDefault();
			var currency = $(this).attr('id');
			var loadURL = $(location).attr('href');
			$.ajax({
				type: "POST",
				dataType: "text",
				url: mlotto.baseURLm + "ajax/",
				cache: false,
				data: {
					action: 'resend-mail',
					mail: $(this).data('resend')
				},
				success: function (theResponse) {
					$("#resendmessage").html('<div class="fa fa-check"></div>A confirmation mail has been sent, please confirm it and come back to complete the registration');
				}
			});	//ajax
		});
	},
	bmaccordian: function () {
		$("#bmaccordian_toggler").live('click', function () {
			$(this).addClass('showless').html('Click here for less lotteries');
			$(".bmaccordian").removeClass('hide');

		});

		$("#bmaccordian_toggler.showless").live('click', function () {
			$(this).removeClass('showless').html('Click here for more lotteries');
			$(".bmaccordian").addClass('hide');

		});
	},
	init: function () {
		this.hLink();
		this.hLink2();
		this.hLink3();
		this.bannerLink();
		this.changeCurrency();
		this.CustomerSupport();
		this.CustomerSupportReply();
		this.resendActivationMail();
		this.bmaccordian();
	}
}
/* classie ends */

$(document).ready(function () {
	Common.init();

	/* Menu Animation */
	var menuLeft = document.getElementById('cbp-spmenu-s1'),
		body = document.body;
	$(menuLeft).hide();
	if( $("#showLeft").length ) { // it is NOT a FlutterWave iframe ?
		showLeft.onclick = function () {
			if($('.navbar-toggle i').hasClass('fa-bars')) {
				$('.navbar-toggle i').removeClass('fa-bars');
				$('.navbar-toggle i').addClass('fa-times');
				$(menuLeft).show('fast');
			} else {
				$('.navbar-toggle i').removeClass('fa-times');
				$('.navbar-toggle i').addClass('fa-bars');
                $(menuLeft).hide('fast');
			}
			classie.toggle(this, 'active');
			classie.toggle(menuLeft, 'cbp-spmenu-open');
			disableOther('showLeft');
			$('.modal-backdrop.nav').fadeToggle();
		};
		$('.modal-backdrop.nav').click(function(){ $("#showLeft").trigger('click'); });
		$('.account-sectonbox .close').click(function(){ $("#showLeft").trigger('click'); });
		/* initialize accordion */
		$("#dev-accordionMenu").accordion({active: false, collapsible: true});
		$(".dev-accordion").accordion({collapsible: true, heightStyle: "content", active: false});
	}

	/* footer height */
	var height = $(window).height() - 114;
	/*$('#msection').css({"min-height": height + 'px'});*/


	/* animate the lottery select box on homepage */
	var lottery_dropdown=false;
	$('#lottery_select').on('click', function () {
		$('#lottery_dropdown').fadeToggle(200);
		if( !lottery_dropdown && typeof(dataLayer) != 'undefined' ) {
			if(typeof(impressions)=='undefined') impressions=[];
			else impressions.splice(0,1); // remove 1st - it is always visible
			//GA Data layer
			dataLayer.push({
				'event': 'lottos',
				'eventCategory': 'numbers',
				'eventAction': 'click',
				'eventLabel': 'more lotteries',
				'ecommerce': {
					'currencyCode': 'USD', // ticket price currency code
					'impressions': impressions // see featured_lotteries.php
				}
			}); //GA Data layer /
 		}
		lottery_dropdown=true; // count only 1 time
	});
	if ($.isFunction($.fn.intlTelInput)) $("#phone,#intelinput").intlTelInput({
		allowExtensions: true,
		autoFormat: true,
		initialCountry: 'auto',
		autoPlaceholder: true,
		geoIpLookup: function (callback) {
			callback(countryCode);
		},
		nationalMode: true,
		numberType: "MOBILE",
		//excludeCountries: ['us','il'],
		utilsScript: "js/utils.js",
	});
	// pass country code to country name
	$('#countrycode').on('change',function(){ $('#countryname').val(this.options[this.selectedIndex].text); });
	$('#countrycode').trigger('change');

	$('#loading-overlay').hide(0);
	setTimeout(function(){
		// alert(window.sidebar + ' | ' + window.external + ' | ' + ('AddFavorite' in window.external)+ ' | ' + window.opera + ' | ' + window.print);
		if (window.sidebar && window.sidebar.addPanel) {
			bookmarkM();
		} else if (window.external && ('AddFavorite' in window.external)) {
			bookmarkM();
		} else if (window.opera && window.print) {
			// bookmarkM();
		}
	}, 2000);
});

function maxLengthCheck(object) {
	if(object.maxLength>0 && object.value.length>object.maxLength) // check length
		object.value = object.value.slice(0, object.maxLength);
	if(typeof(object.min)=='undefined') return;
	var min=object.min.length>0 ? parseInt(object.min):0;
	var max=object.max.length>0 ? parseInt(object.max):0;
	var val=parseFloat(object.value);
	var msg='Sorry, Value should be ';
	if(min+max>0){ // chech value
		if(val<0.)  { alert(msg+min+' minimum'); object.value=min; }
		if(val>max) { alert(msg+max+' maximum'); object.value=max; }
	}
}

function disableOther(button) {
	if (button !== 'showLeft') {
		classie.toggle(showLeft, 'disabled');
	}
}

function bookmarkM() {

	$('#bookmark_overlay').show();

	$('#bkmoc').on('click', function () {
		$('#bookmark_overlay').hide();
	});

	$('#bookmark_btn').on('click', function () {
		if (window.sidebar && window.sidebar.addPanel) { // Mozilla Firefox Bookmark
			window.sidebar.addPanel(document.title, window.location.href, '');
		} else if (window.external && ('AddFavorite' in window.external)) { // IE Favorite
			window.external.AddFavorite(location.href, document.title);
		} else if (window.opera && window.print) { // Opera Hotlist
			this.title = document.title;
			return true;
		} else { // webkit - safari/chrome
			alert('Press ' + (navigator.userAgent.toLowerCase().indexOf('mac') != -1 ? 'Command/Cmd' : 'CTRL') + ' + D to bookmark this page.');
		}
		$('#bookmark_overlay').hide();
	});


}

function validateCustomerSupport() {
	if ($('#name').val().length > 3 && $('#email').val().length > 4 && $('#question').val().length) {
		if ($('#btnAddSupport').hasClass('btn-grey')) {
			$('#btnAddSupport').removeAttr('disabled');
			$('#btnAddSupport').removeClass('btn-grey');
			$('#btnAddSupport').addClass('btn-green');
		}
		var intlNumber = $('#phone').intlTelInput("getNumber");
		$('#hiddenphone').val(intlNumber);
	} else {
		if ($('#btnAddSupport').hasClass('btn-green')) {
			$('#btnAddSupport').attr('disabled', 'disabled');
			$('#btnAddSupport').removeClass('btn-green');
			$('#btnAddSupport').addClass('btn-grey');
		}
	}
}
$("#frmAddSupport").validate({
	// Specify the validation rules
	rules: {
		name: "required",
		phone: "required",
		email: "required email",
		subject: "required",
		question: "required"
	},
	submitHandler: function (form) {
		//form.submit();
		$.post(baseURL + "ajax/index.php",
			$('#frmAddSupport').serialize(),
			function (data) {
				$("#message").html(data);
				$("#frmAddSupport")[0].reset();
			}, "text");
	}
});

function resendActivationToken() {
	$('#register-resend-token').text('sending the new verification link. Please Wait...');
	$.ajax({
		url: baseURL + "ajax/index.php",
		type: "POST",
		cache: false,
		dataType: 'json',
		data: $('#frmAccountAdd').serialize()
	}).done(function (data) {
		if (data.result == "success") {
			alert('verification link sent to your email Successfully');
		} else {
			alert(data.message);
		}
		$('#register-resend-token').html("Not received the verification link? <a href='javascript:resendActivationToken();'>Click here to resend verification link.</a>");
	}).fail(function () {
		alert(errorMessage);
		$('#register-resend-token').html("Not received the verification link? <a href='javascript:resendActivationToken();'>Click here to resend verification link.</a>");
	}); // ajax
}

$(window).load(function () {
	$('.login-inner').hide();
	$('.submenu-button').click(function () {
		$('.login-inner').slideToggle("slow");
		$('.submenu-button').css('background-color', 'white');
        $('#show-hide-support').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
		;
	});
});
$(window).load(function () {
    $('.submenu-button-lotteries').on('click', function () {
        $('.lotteries-inner').slideToggle("slow");
        $('.submenu-button-lotteries').css('background-color', 'white');
        $('#show-hide-lotteries').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
    });
});

// prevent doble click on submit button; b - button id
function submitDisable(b) {
	var _bFW=document.getElementById(b);
	if(_bFW){
		_bFW.disabled=true;
		_bFW.style.backgroundColor="#ccc";
	}
	return true;
}

// money fromat: 0.00
function moneyFmt(amnt) {
	amnt=''+Math.round(amnt*100)/100;
	x=amnt.indexOf('.');
	if(x<0) amnt+='.00';
	else if(amnt.length-x==2) amnt+='0';
	return amnt;
}

/* Close system messages */
$(document).ready(function () {
    $('.msg').on('click', function () {
        $(this).fadeOut('slow');
    });
	if(navigator.userAgent.match(/iPhone/)) {
		$('html').addClass('iphone');
	}
});