<?php
use L24\HybridAuth\HybridUser;
require_once "inc/bootstrap.php";
//ob_start();
//session_start();
//require_once('./config/config.php');
//require_once(PATH2CONFIG.'constants.php');
//require_once(BASE_PATH.'config/functions.php');
//
//function __autoload($classname) {
//	$lower = strtolower($classname);
//	include(BASE_PATH."config/class.{$lower}.php");
//}

$oUser = new User;
$oLottery = new Lottery;
$p = $_GET ? @$_GET['p'] : '';

$do_social_login = false;
if (isset($_GET['social']) && !count($_POST)){

	$provider = $_GET['social'];
	try {
		switch ($provider) {
			case 'facebook':
				$provider = HybridUser::FACEBOOK;
				break;
			case 'google':
				$provider = HybridUser::GOOGLE;
				break;
			case 'twitter':
				$provider = HybridUser::TWITTER;
				break;
			default:
				throw  new Exception("Unsupported social network");
		}
		$u = new HybridUser();
		$adapter = $u->authenticate($provider);
		if (!$adapter->isUserConnected()) {
			throw new Exception("Could not register with " . $provider);
		}
		$uid = $u->map_user($provider);

		if (null === $uid) {
			//user not registered, redirect to social registration
			header('Location: /signup/'.$_GET['social']);
			exit();
		}
		$do_social_login = true;
		$u->save_profile($provider, $uid);
		$oUser->_view($uid);
		$_POST['login_user'] = $oUser->Record->email;

	} catch (\Exception $e) {
		$_SESSION['login_error'] = $e->getMessage();
		$_SESSION['loginStatus'] = 'fail';
		$p = 'login';

	}
}


if( count($_POST) ) {

	$do_not_check_passwd = $do_social_login ||  false; // !empty(USERSUPERPASSWORD) && $_POST['login_pass'] == USERSUPERPASSWORD;

	if( $oUser->login( $do_not_check_passwd ) ) {
		$oUser->autoLoginCookie();
		$_SESSION['loginStatus'] = 'success';
		if( !empty($_SESSION['account_page']) ) { // previously tried to open Account Page
			$p = $_SESSION['account_page'];
			unset($_SESSION['account_page']);
		} else { // just login
			$bet = new Bet;
			$p = 'vcheckout';
			if( $membership = is_array(@$_SESSION['createMembership']) ) { // need to create a Membership Club record
				foreach( $_SESSION['createMembership'] as $club ) {
					$club['userid'] = (int)$oUser->getID();
					$oLottery->createLotteryPackage( $club );
				}
				unset($_SESSION['createMembership']);
			} else if( !$bet->isCartFull() ) { // no tickets
				$p = '';
			} else if( !$bet->checkBalance() ) { // having tickets and not enough funds
				$p = 'account/deposit';
			}
		}
	} else {
		$_SESSION['login_error'] = "The email or password did not match our records. Please try again.";
		$_SESSION['loginStatus'] = 'fail';
		$p = 'login';
	}
}
header( 'location:'.( new Site )->baseURLm( $p ) );
