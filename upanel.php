<?php
include( 'inc/header.php' );
if( !$oUser->loginCheck() || !$oUser->getID() ) {
	if( !empty($p) ) $_SESSION['account_page'] = 'account/'.$p;
	header( 'location:'.$site->baseURLm('login') );
	exit;
}

$oAccount = new Account( $oUser );
$oUser->_view( $oUser->getID() );
$userROW = $oUser->Record;
?>
<style>.nav-bg {padding-bottom: 0px;-webkit-padding-after: 1px;}</style>
<div id="hello">
    <div class="container whitebg">

    	<?php isset($p) ? include("upanel/$p.php") : include("pages/home.php")?>

    </div> <!-- /container -->
</div><!-- /hello -->

<?php include(BASE_PATHm.'inc/footer.php')?>
