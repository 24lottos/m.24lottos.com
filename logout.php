<?php
ob_start();
session_start();
require_once('./config/config.php');
require_once(PATH2CONFIG.'constants.php');
require_once(BASE_PATH.'config/functions.php');

function __autoload($classname) {
    $lower = strtolower($classname);
	include(BASE_PATH."config/class.{$lower}.php");
}

$site = new Site;
$oUser = new User;
$p = $_GET ? @$_GET['p'] : NULL;

// logging out user from site
$oUser->logout();
setcookie( 'last_visit', 'true', time()-3600, "" );
session_unset();
session_destroy();
session_start();
session_regenerate_id( true );

//redirect back to site
header('location:'.$site->baseURLm());
