var VENDOR_SCRIPTS = [
	'src/vendor/**/*.js',
];

var Q = require('q'),
	gulp = require('gulp'),
	util = require('gulp-util'),
	merge = require('merge-stream'),
	concat = require('gulp-concat'),
	cssc = require('gulp-css-condense'),
	LessPluginAutoPrefix = require('less-plugin-autoprefix'),
	LessPluginCleanCSS = require('less-plugin-clean-css'),
	less = require('gulp-less'),
	modifyCssUrls = require('gulp-modify-css-urls'),
	sourcemaps = require('gulp-sourcemaps'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	plumber = require('gulp-plumber'),
	notify = require('gulp-notify'),
	watch = require('gulp-watch'),
	batch = require('gulp-batch'),
	sort = require('gulp-sort'),
//	inline_base64 = require('gulp-inline-base64'),
//	autoprefixer = require('gulp-autoprefixer'),
//	browserSync = require('browser-sync').create(),
	end = true;

var log = function (message) {
	util.log(util.colors.green(message));
};

var log_error = function (message) {
	util.log(util.colors.green(message));
	//this.emit('end');
};

var errorHandler = function (err) {
	notify.onError({
		title: "Gulp Error",
		message: "Error: <%= error.message %>",
		sound: "Bottle"
	})(err);
	this.emit('end');
	//this.emit('finish');
};

var _gulpsrc = gulp.src;
gulp.src = function () {
	return _gulpsrc.apply(gulp, arguments)
		.pipe(plumber({
			errorHandler: errorHandler
		}));
};

function clean(target) {
	var deferred = Q.defer();
	log('cleaning files: ' + target);
	gulp.src(target)
		//	.pipe(rimraf())
		.on('error', log)
		.on('finish', deferred.resolve);
	return deferred.promise;
}

////////
// tasks
////////

gulp.task('default', function () {
	// place code for your default task here
});


gulp.task('vendor-scripts', function () {
	var deferred = Q.defer();
	clean(['js/vendor.js']).then(function () {
		log('building vendor javascript');
		gulp.src(VENDOR_SCRIPTS, {base: '.'})
			.pipe(sort())
			.pipe(concat('vendor.js'))
			.pipe(gulp.dest('js/'))
			.pipe(uglify())
			.pipe(rename({suffix: '.min'}))
			.pipe(gulp.dest('js/'))
//			.pipe(browserSync.stream())
			.on('finish', deferred.resolve);
	});
	return deferred.promise;
});


gulp.task('theme-scripts', function () {
	var deferred = Q.defer();
//	clean(['js/theme.js']).then(function () {
	log('building theme javascript');
	// .js to .min.js
	gulp.src(['src/js/lib/*.js'])
		.pipe( uglify() )
		.pipe( rename({suffix: '.min'}) )
		.pipe(gulp.dest('js/'))
//		.pipe(browserSync.stream())
		.on('finish', deferred.resolve)
		.on('error', errorHandler)
	;
	//head and footer separate scripts to inline
	gulp.src(['src/js/head-inline-js/**/*.js'])
		.pipe(sort())
		.pipe(concat('head-inline.js'))
//		.pipe(gulp.dest('js/'))
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('js/'))
//		.pipe(browserSync.stream())
		.on('finish', deferred.resolve)
		.on('error', errorHandler)
	;
	//head and footer separate scripts to inline
	gulp.src(['src/js/footer-inline-js/**/*.js'])
		.pipe(sort())
		.pipe(concat('footer-inline.js'))
//		.pipe(gulp.dest('js/'))
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('js/'))
//		.pipe(browserSync.stream())
		.on('finish', deferred.resolve)
		.on('error', errorHandler)
	;
	return deferred.promise;
});


gulp.task('theme-styles', function () {
	var deferred = Q.defer();
	log('building CSS');
	//autoprefix = new LessPluginAutoPrefix({browsers: ["last 2 versions"]});
	//cleancss = new LessPluginCleanCSS({advanced: true});

	/**
	 *  head inline style
	 */
	gulp.src(['src/less/head-inline.less'])
		.pipe(sourcemaps.init().on('error', log))
		.pipe(less().on('error', log_error))
		//.pipe(inline_base64({
		//	baseDir: './css/',
		//	maxSize: 14 * 1024,
		//	debug: true
		//}))
		// .pipe(modifyCssUrls({
		//	 modify: function (url, filePath) {
		//		 //return 'vendor/' + url;
		//		 // return '/css/' + url;
		//		 return url;
		//	 },
		//	 //prepend:  PATHS['dist'].siteUrl,
		//	 // append: '?' + CSS_VERSION
		// }))
		.pipe(sourcemaps.write().on('error', log))
//		.pipe(gulp.dest('css/'))
		.pipe( cssc() )
		.pipe(rename({suffix: '.min'}).on('error', log_error))
		.pipe(gulp.dest('css/'))
//		.pipe(browserSync.stream())
		.on('finish', deferred.resolve)
		.on('error', errorHandler)
	;

	/**
	 *  theme main css
	 */
	gulp.src(['src/less/main-css.less'])
		.pipe(sourcemaps.init().on('error', log))
		.pipe(less())
		//.pipe(inline_base64({
		//	baseDir: './css/',
		//	maxSize: 14 * 1024, //5KB
		//	debug: true
		//}))
		.pipe(sourcemaps.write().on('error', log))
		.pipe(concat('main.css'))
//		.pipe(gulp.dest('css/'))
		.pipe( cssc() )
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('css/'))
//		.pipe(browserSync.stream())
		.on('finish', deferred.resolve)
		.on('error', errorHandler)
	;
	return deferred.promise;
});

gulp.task('all', ['theme-scripts', 'theme-styles'], function(){
});

gulp.task('watch', [], function () {
	console.info('Process dir:' + process.cwd()); //gulpfile.js location
//	browserSync.init({ proxy: "http://bots.kbcn.info" });
	// css
	gulp.watch( ['src/less/**/*.less'], ['theme-styles'] );
	// js
	gulp.watch(['src/js/**/*.js'], ['theme-scripts'] );
});
