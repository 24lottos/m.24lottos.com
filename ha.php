<?php
/**
 *  THis file used by Hybrid Auth to perform logins and redirects!
 * !! DO NOT EDIT OR DELETE IT !!
 */
require_once "inc/emsgd.php";
require_once "./vendor/autoload.php";
try{
	Hybrid_Endpoint::process();
}catch (Exception $e){
	echo($e->getMessage());
}