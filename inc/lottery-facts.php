<div id='interesting' class="clearfix"></div>
<h5 class="pageHeading blue-list">Interesting Facts about the <?= $oLottery->Record->lotteryNameModified ?></h5>
<img src='/img/px.gif' alt='Interesting Facts' class='icon facts'/>
<div class="col-lg-12 no-padding">
	<?= $oLottery->Record->fiveFactsAbout ?>
</div>

<?php if( !isset($moreOpened) || !$moreOpened ): ?>
<script>
$(window).load(function () {
	var q=window.location.href.replace(/.*#/,'');
	if(q=='interesting') {
		$('#viewMoreBtn').trigger("click");
		$(document.body).animate({'scrollTop':$('#interesting').offset().top}, 200);
	}
});
</script>
<?php endif; ?>
