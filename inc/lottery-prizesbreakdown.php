<h5 class="pageHeading blue-list"><?= $oLottery->Record->lotteryNameModified ?> - Prize Breakdown</h5>
<img src='/img/px.gif' alt='Prize Breakdown' class='icon prize'/>
<div class="col-lg-12 no-padding prizesbreakdown">
	<?= stripslashes( htmlspecialchars_decode( $oLottery->Record->prizeBreakdown ) ) ?>
</div>
<style>
    .prizesbreakdown table {
        width: auto!important;
        margin: 20px auto 0;
    }
</style>
