<?php
/**
 * header.php - HTML <head> part and top of the page
 */
?><!DOCTYPE html>
<html lang="en">
<head>
<?php require_once('header_head.php') ?>
</head>

<body class="cbp-spmenu-push">

<?php if( GAENABLED ) { ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TRKHWV" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<script>
<?php // page not from listed = other page
	if( $p!='home' && $p!='error' && $p!='lottery' && $p!='vcheckout' && $p!='deposit' ) { ?>
	dataLayer.push({
		'pageType': 'info',
		'auth': '<?= $oUser->loginCheck() ? 1:0 ?>',
		'userId': '<?= $oUser->getID() ?>',
		'depositTotal': '<?= $oUser->getCurrentBalance() ?>'
	});
<?php }
	if(@$_SESSION['loginStatus'] == 'signup') { ?>
	dataLayer.push({
		'event': 'lottos',
		'eventCategory': 'authorization',
		'eventAction': 'sign up',
		'eventLabel': 'success'
	});
<?php }
	else if( !empty($_SESSION['loginStatus']) ) { ?>
	dataLayer.push({
		'event': 'lottos',
		'eventCategory': 'authorization',
		'eventAction': 'logIn',
		'eventLabel': '<?= $_SESSION["loginStatus"] ?>'
	});
<?php } ?>
</script>
<?php } // if( GAENABLED ) /

unset($_SESSION['loginStatus']);

$oLottery = new Lottery;
$oLottery->areNumbersPicked( session_id() );
$pendTicNum = $oLottery->numRows();

$animate_icon = false;
if ($pendTicNum) {
    $last_visit = isset($_COOKIE['last_visit']) ? true : false;
    setcookie( 'last_visit', 'true', time()+3600, "" );
    $animate_icon = ( !isset($_SESSION['animate_icon']) || !$last_visit ) ? true : false;
    $_SESSION['animate_icon'] = false;
}
?>

<div class="overlay" id="bookmark_overlay">
	<div class="overlay_inner">
		<img src="img/close.png" class="close_overlay" id="bkmoc" alt='close'>
		<?php /* <span>Dummy text Dummy text Dummy text Dummy text</span> */ ?>
		<div class="bookmark_div">
			<img id="bookmark_btn" src="img/bookmark-button.png" alt='bookmark'>
		</div>
	</div>
</div>

<div class="container">
	<div class="main">
		<div class="section">
			<header style='text-align:center'>
				<button type="button" class="navbar-toggle" id="showLeft">
					<i class="fa fa-bars" aria-hidden="true" style="font-size:25px"></i>
				</button>

				<a href="<?= BASE_URLm ?>" class='hdrlogo'><img src="img/logo.png" alt="24lottos.com logo" title="24lottos.com logo"/></a>

				<?php if( $p!='deposit' ) { ?>
                <div class="icon-wrap link" style="<?= !$pendTicNum ? 'display:none':'' ?>" id='pendTicNum'  data-link="/vcheckout">
                    <div id="rectangle" class="<?= $animate_icon ? 'ticket-animation' : '' ?>">
                        <div class="inner-preview-box">
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                        </div>
                    </div>
                    <span id="reddot" class="<?= $animate_icon ? 'reddot-animation' : '' ?>"><?= $pendTicNum ?></span>
                </div>
                <?php } ?>
				<!--<button type="button" style="<?/*= !$pendTicNum ? 'display:none':'' */?>"
					id='pendTicNum'  class="navbar-checkout link"
					data-link="/vcheckout"><span><?/*= $pendTicNum */?></span>
				</button>-->
			</header>

            <?php include_once('nav.php') ?>
<?php
/* message right after a SignUp */
if( @$_SESSION['showSignupGreeting'] ) { ?>
			<div class="success-msg"><i class="fa fa-star"></i> <?= SIGNUPGREETING ?></div>
<?php
	unset($_SESSION['showSignupGreeting']);
}
?>
            <script>
                var validNavigation = false;
                jQuery(document).ready(function () {
                    wireUpEvents();
                });

                function endSession() {
                    // Browser or broswer tab is closed
                    // Do sth here ...
                    // console.log("endSession");
                    var date = new Date();
                    var expire = new Date(date.setHours(date.getHours() - 1));
                    // console.log('endSession - ' + expire);
                    document.cookie = "last_visit=false; path=; expires=" + expire.toUTCString();
                }

                function wireUpEvents() {
                    // Attach the event keypress to exclude the F5 refresh
                    $(document).on('keypress keydown keyup', function (e) {
                        e = e || window.event;
                        if (e.keyCode == 116) {
                            validNavigation = true;
                        }
                    });

                    // Attach the event click for all links in the page
                    $("a").on("click", function () {
                        validNavigation = true;
                        //console.log('a');
                    });

                    $("button").on("click", function () {
                        validNavigation = true;
                        //console.log('button');
                    });

                    $("#btnLogin").on("click", function () {
                        validNavigation = true;
                        //console.log('btnLogin');
                    });

                    // Attach the event submit for all forms in the page
                    $("form").on("submit", function () {
                        validNavigation = true;
                    });

                    // Attach the event click for all inputs in the page
                    $("input[type=submit]").bind("click", function () {
                        validNavigation = true;
                    });

                    /*
                    * For a list of events that triggers onbeforeunload on IE
                    * check http://msdn.microsoft.com/en-us/library/ms536907(VS.85).aspx
                    */
                    window.onbeforeunload = function() {
                        if (!validNavigation) {
                            //console.log('onbeforeunload');
                            endSession();
                        }
                        return;
                    }
                }
            </script>
