<?php
$jackpot = isset( $t['CurrentJackpot'] ) ? $t['CurrentJackpot'] : 0;

if( $p == 'lottery-archive' && $jackpot > 0 && $t['NextPurchaseDeadline'] > date( 'Y-m-d H:i:s', time() ) ) {
    $_name = $t['DisplayName'];
    $_url  = $site->baseURL( 'lottery/'.stringURLSafe( $t['alias'] ) );
    $_ico  = $site->baseURL( 'images/lottery_logos/small_logos/'.$t['Small_Image_url'] );
    $_jp   = $t['CurrentJackpot'];
    $_jpcu = getCurrencySign( $t['JackpotCurrency'] );
    $_time = $t['NextPurchaseDeadline'];
} else {
    $oLotteryList = new Lottery;
    $rUpcomingLotteries = $oLotteryList->upcomingLotteries();
    foreach( $rUpcomingLotteries as $t ):
        $timeToDraw = $t['NextPurchaseDeadline'];
        $stopt = ENVIRONMENT ? date( 'Y-m-d H:i:s', time() ) : $timeToDraw;
        if( !$timeToDraw[0] || $timeToDraw<$stopt || !$t['CurrentJackpot'] ) continue;
        $_name = $t['DisplayName'];
        $_url  = $site->baseURL( 'lottery/'.stringURLSafe( $t['alias'] ) );
        $_ico  = $site->baseURL( 'images/lottery_logos/small_logos/'.$t['Small_Image_url'] );
        $_jp   = $t['CurrentJackpot'];
        $_jpcu = getCurrencySign( $t['JackpotCurrency'] );
        $_time = $t['NextPurchaseDeadline'];
        break;
    endforeach;
}

if( $_jp>=10000000) $_jp = '<span style="font-size:145%">'.(int)($_jp/1000000).'</span> </span> <span class="symbol" data-symbol="millions"></span>';
else $_jp = '<span style="font-size:125%">'.number_format( $_jp/10, 0 ).'</span>';
?>
<div id="banner" onclick='location.href="<?= $_url ?>"'>
    <div id="background">
        <div class='lottery'>
            <a href='<?= $_url ?>' title="<?= $_name; ?>">
                <div class='htLottery'><?= $_name ?></div>
                <img src='<?= $_ico ?>' class='icon' alt='<?= $_name; ?>'/>
            </a>
            <div class='jackpot'><span class="symbol" data-symbol="<?= $_jpcu ?>"></span> <?= $_jp ?></div>
        </div>
    </div>
    <div class='lottery-time'>
        <div class='timetodraw' style="float: none !important;" data-lotterycountdown="<?= $_time ?>"></div>
        <button><div class='shine'>PLAY &nbsp;<i class="fa fa-arrow-circle-right"></i></div></button>
    </div>
    <div style="clear: both;"></div>
</div>

<style>
    #banner {
        position: relative;
        width: 96%;
        max-width: 360px;
        height: 230px;
        background: #f8f8f8;
        border: 1px solid #ddd;
        margin: 10px auto 0;
        min-height: 60px;
        padding: 0;
    }

    #banner #background {
        width: 100%;
        height: 105px;
        background: url(/img/htbanner_l_bg.png) left top no-repeat;
    }

    #banner .lottery a {
        display: block;
    }

    #banner .lottery .htLottery {
        float: left;
        margin: 10px 0 0 10px;
        font-size: 22px;
        color: #fff;
        font-weight: normal;
        width: 158px;
    }

    #banner .lottery .icon {
        float: right;
        margin: 5px 5px 0 0;
    }

    #banner .lottery .jackpot {
        clear: both;
        color: white;
        font-size: 30px;
        margin-left: 15px;
    }

    #banner .lottery-time .timetodraw {
        color: #35589c;
        font-size: 34px;
        font-weight: bold;
        width: 200px;
        height: 40px;
        margin: 10px auto 20px;
    }

    #banner .lottery-time .lotto-count {
        float: left;
        margin-left: 10px;
        text-align: center;
    }

    #banner .lottery-time .lotto-count div {
        font-size: 10px;
    }

    #banner .lottery-time button {
        display: block;
        background: #009345;
        border: 1px solid #fff;
        color: #fff;
        font-family: sans-serif,arial;
        font-weight: bold;
        font-size: 20px;
        line-height: 42px;
        padding: 0;
        width: 148px;
        margin: 0 auto;
    }
</style>
