<div class="modal-backdrop in nav"></div>
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">

<?php /*
	 if( $oUser->loginCheck() ) {
		<div class="nav-sectionbox">
			<a href="<?= BASE_URLm ?>account/dashboard" class="dashboard_link">Dashboard</a>
			<a href="<?= BASE_URLm ?>account/deposit" class="quick_deposit" onclick="confirmBtnClick('quickDeposit')">Quick Deposit</a>
		</div>
	}
	// removed 2017-04-17: <a href="<?= BASE_URLm ?>account/account"><i class="fa fa-user" aria-hidden="true" title="Account"></i> Account</a>
*/ ?>

	<div class="account-sectonbox">
		<?php
		if( $oUser->loginCheck() ) { ?>
			<a href="<?= BASE_URLm ?>account/deposit"><i class="fa fa-rocket" aria-hidden="true" title="Tickets"></i> Quick Deposit</a>
			<a href="<?= BASE_URLm ?>account/tickets"><i class="fa fa-tags" aria-hidden="true" title="Tickets"></i> Tickets</a>
			<a href="<?= BASE_URLm ?>account/wallet"><i class="fa fa-money" aria-hidden="true" title="Wallet"></i> Wallet</a>
		<?php } else { ?>
			<div class="login">
				<a href="<?= BASE_URLm ?>login">Login/Sign up<i class="fa fa-sign-in pull-right" aria-hidden="true" title="Sign-in"></i></a>
			</div>
		<?php } ?>
		<a href="/" class="white_tab"><i class="fa fa-home" aria-hidden="true" title="Home"></i> Home</a>
        <a href="javascript:void(0)" class="submenu-button-lotteries white_tab"><i class="fa fa-cogs" aria-hidden="true" title="Lotteries"></i> Lotteries <i id="show-hide-lotteries" class="fa fa-chevron-up pull-right" aria-hidden="true"></i></a>
        <div class="lotteries-inner">
            <a href="<?= BASE_URLm ?>lottery/mega-millions"><img src="<?= BASE_URLm ?>img/logo-megamillions-xs.png" alt="US Mega Millions" title="US Mega Millions" class="logo-ball" align="middle"><div class="lotto-name">US Mega Millions</div></a>
<!--            <a href="--><?//= BASE_URLm ?><!--lottery/euromillions"><img src="--><?//= BASE_URLm ?><!--img/logo-euromillions-xs.png" alt="EuroMillions" title="EuroMillions" class="logo-ball" align="middle"><div class="lotto-name">EuroMillions</div></a>-->
            <a href="<?= BASE_URLm ?>lottery/powerball"><img src="<?= BASE_URLm ?>img/logo-powerball-xs.png" alt="US Powerball" title="US Powerball" class="logo-ball" align="middle"><div class="lotto-name">US Powerball</div></a>
            <a href="<?= BASE_URLm ?>lottery" class="white_tab"><img src="<?= BASE_URLm ?>img/all-games.png" alt="See All Lotteries" title="See All Lotteries" class="see-all" align="middle"><div class="lotto-name">More Lotteries</div></a>
        </div>
		<a href="<?= BASE_URLm ?>results" class="white_tab"><i class="fa fa-bullhorn" aria-hidden="true" title="Results"></i> Results</a>
        <a href="<?= BASE_URLm ?>winners" class="white_tab"><i class="fa fa-trophy" aria-hidden="true" title="Winners"></i> Winners</a>
		<?php if( !ENVIRONMENT ) { ?>
		<a href="<?= BASE_URLm ?>promotions" class="white_tab"><i class="fa fa-rocket" aria-hidden="true" title="Lotteries"></i> Promotions</a>
		<?php } ?>
		<a href="javascript:void(0)" class="submenu-button white_tab"><i class="fa fa-cogs" aria-hidden="true" title="Copy to use cogs"></i> Support & Info <i  id="show-hide-support" class="fa fa-chevron-down pull-right" aria-hidden="true"></i></a>
		<div class="login-inner">
			<a href="<?= BASE_URLm ?>how-to-play"><i class="fa fa-question-circle" aria-hidden="true" title="How To Play"></i> How To Play</a>
			<a href="<?= BASE_URLm ?>customer-support"><i class="fa fa-medkit" aria-hidden="true" title="Customer Support"></i> Customer Support</a>
			<a href="<?= BASE_URLm ?>faq"><i class="fa fa-info-circle" aria-hidden="true" title="FAQ"></i> FAQ</a>
            <a href="<?= BASE_URLm ?>press"><i class="fa fa-newspaper-o" aria-hidden="true" title="FAQ"></i> Press</a>
		</div>

		<?php if( $oUser->loginCheck() ): ?>
			<div class="pull-left">
				<div class="pull-right admin-section" style='position:absolute'>
					<a href="<?= BASE_URLm ?>account/account"><i class="fa fa-user" style='padding-right:10px'aria-hidden="true"></i>My Account</a>
				</div>
			</div>
			<div class="pull-right admin-section">
				<a href="<?= BASE_URLm ?>logout">Logout<i class="fa fa-sign-out logout" aria-hidden="true" title="Logout"></i></a>
			</div>
		<?php endif; ?>
	</div>

</nav>
<?php
$facts = array(
    "48% of winners keep their day job",
    "83% of winners give money to family",
    "Just 1% of winners get plastic surgery",
    "99% of winners travel",
    "3% of winners move their kids to private schools",
    "68% of winners keep playing the lottery",
    "95% of winners remain married after winning",
    "51% of winners gave money to their parents",
    "90% of winners still have a best friend",
    "40% of winners increased contributions to charity",
    "100% of winners are still in a relationship",
    "58% of winner families are happier than before",
    "57% of winners gave money to their children",
    "45% of winners started their own business",
    "75% of winners moved from an apartment to a home",
    "65% of winners gave money to their siblings",
    "24% of winners now own property in foreign countries",
);
$fact = $facts[ rand( 0, count( $facts ) - 1 ) ];

if( in_array( $p, ['lottery-list', 'results', 'winners', 'tickets'] ) ) {
    ob_flush(); flush();
    $buffer = ob_get_status(true);
    $buf_size = $buffer[0]['chunk_size'];

    ob_start();
    ?>
    <div id="loading-overlay">
        <div id="fyi-random-fact">
            <div id="fyi">Did You Know?</div>
            <div id="random-fact"><?=$fact?></div>
        </div>
    </div>
    <?php
    $buf_content = ob_get_contents();
    ob_end_clean();
    echo str_pad ($buf_content, $buf_size,' ', STR_PAD_BOTH );
    ob_flush(); flush();
//    sleep(5);
}
else {
    echo '<div id="loading-overlay"></div>';
}

?>
