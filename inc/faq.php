<?php
$oFaq = new Faq;
$pTemp = isset($p) ? $p:'home';
$oFaq->view( "WHERE status = 'Enable' AND displayOn LIKE '%$pTemp%' ORDER BY priority" );
if( $oFaq->numRows() ): ?>

<div class="clearfix"></div>
<section id="msection" class="faq">
	<article>
		<h5 class="pageHeading blue-list">Frequently Asked Questions</h5>
		<div class="dev-accordion">
			<?php
			while( $oFaq->nextRecord() ):
				echo '<div class="panel-title">'.stripslashes( $oFaq->Record->question ).'</div><div>';
				echo htmlspecialchars_decode( $oFaq->Record->answer ).'</div>';
			endwhile;
			?>
		</div>
	</article>
</section>
<br/>

<?php endif;
