<?php
ob_start();
session_start();
require_once('config/constants.php');
require_once('config/functions.php');

function __autoload( $classname ) {
	$lower = strtolower( $classname );
	include("config/class.{$lower}.php");
}

$site = new Site;
$oUser = new User;
$oEmail = new Email;
$oPixel = new Pixel;
$oGen = new General;

// later modify this code with session. session is alreacy extracted at line 18 below.
if( !isset($_SESSION['currency']) ):
	if( ipToCountry() == 'GH' ):
		$_SESSION['currency'] = "GHS";
	else:
		$_SESSION['currency'] = "NGN";
	endif;
endif;

$p = $_GET ? @$_GET['p']:'results';
$sp = isset($_GET['sp']) ? @$_GET['sp']:null;
extract( $_SESSION );

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php
	$blSeo = new BaseLottery;
	$oSeoKey = new Seo();
	$oSeoDesc = new Seo();
	$oSeoKey->_view( 8 );
	$oSeoDesc->_view( 9 );

	if( $sp !== 'lotto-results' ):
		$blSeo->_view( $sp );
		$key = $blSeo->Record->metakey == '' ? $oSeoKey->Record->value:$blSeo->Record->metakey;
		$desc = $blSeo->Record->metadesc == '' ? $oSeoDesc->Record->value:$blSeo->Record->metadesc;
		$fbTitle = $blSeo->Record->lotteryName;
		$fbDesc = $blSeo->Record->description;
		$fbImage = $site->baseURL( "images/{$blSeo->Record->logo}" );
	else:
		$key = $oSeoKey->Record->value;
		$desc = $oSeoDesc->Record->value;
		$fbTitle = $site->name;
		$fbDesc = "Meet the winners of 24Lottos.com";
		$fbImage = $site->baseURL( 'assets/img/logo.png' );
	endif;
	?>
	<meta property="og:title" content="<?= $fbTitle ?>"/>
	<meta property="og:description" content="<?= $fbDesc ?>"/>
	<meta property="og:image" content="<?= $fbImage ?>"/>
	<meta name="keywords" content="<?= $key ?>">
	<meta name="description" content="<?= $desc ?>">

	<meta name="author" content="">
	<meta name="robots" CONTENT="index, follow">
	<meta name="google-site-verification" content="-eY8YFGpWBtpIv8jNE7K44QPiWAugui-IBpcsdNsGmA"/>

	<title><?= array_key_exists( $sp, $metaTitle ) ? $metaTitle[$sp]:$metaTitle['home'] ?></title>
	<base href="<?= $site->baseURL() ?>"/>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<!-- Bootstrap core CSS -->
	<link href="<?= $site->baseURL( 'assets/css/bootstrap.css' ) ?>" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="<?= $site->baseURL( 'assets/css/main.css' ) ?>" rel="stylesheet">
	<link href="<?= $site->baseURL( 'assets/css/font-awesome.min.css' ) ?>" rel="stylesheet">

	<script src="<?= $site->baseURL( 'assets/js/jquery-latest.js' ) ?>"></script>
	<script src="<?= $site->baseURL( 'assets/js/chart.js' ) ?>"></script>
	<script src="<?= $site->baseURL( 'js/common.min.js' ) ?>"></script>


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<?php $oPixel->getTrackingCode( $p, $_GET );
	while( $oPixel->nextRecord() ):
		echo html_entity_decode( $oPixel->Record->shortcode );
	endwhile; ?>
</head>

<body>
<!-- Fixed navbar -->

<div class="navbar navbar-default">
	<div class="container nav-bg">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?= $site->baseURL() ?>">
				<img src="<?= $site->baseURL( 'assets/img/logo.png' ) ?>" alt="<?= $site->name ?>"
					 style="z-index: 10; margin-top:-7px; margin-bottom:10px; position: relative;"/>
			</a>
		</div>
		<div class="navbar-collapse collapse">

			<div>
				<div class="pull-left welcome">
					<?= @$oUser->getWelcomeName() ?>
				</div>
				<?php if( !$oUser->loginCheck() ): ?>
					<div class="pull-right">
						<form class="navbar-form navbar-right form-edit" id="frmLogin" role="search"
							  method="post" action="<?= $site->baseURL( "do-login" ) ?>">
							<div class="form-group form-gp">
								<input name="login_user" type="text" class="form-control2 required"
									   placeholder="Username"
									   style="color: #35589C;">

								<input name="login_pass" type="password" class="form-control2 required"
									   placeholder="Password"
									   style="color: #35589C;">
							</div>

							<div class="btn-warning btn-default" id="btnLogin">
								Login<br><i class="fa fa-arrow-circle-o-right-36"></i></div>

							<div class="text-right not-a-member">
								<a href="<?= $site->baseURL( 'signup' ) ?>" class="remember">Not a member Sign up
									now</a>
							</div>
						</form>
					</div>
				<?php else: ?>
					<style>.nav-bg {
							padding-bottom: 2px;
						}</style>
					<div class="navbar-right" style="text-align:center">
						<button id="headplaynow" style="background:#35589c" type="button" class="btn-edit btn-info link"
								data-link="<?= $site->baseURL() ?>">Play Now
						</button>
						<button type="button" class="btn-edit btn-success link"
								data-link="<?= $site->baseURL( 'account/deposit' ) ?>">Quick Deposit
						</button>

						<button type="button" class="btn-edit btn-default link" id="btnMyAccount"
								data-link="<?= $site->baseURL( 'account/dashboard' ) ?>">My Account
						</button>
						<br/>
						<a href="<?= $oUser->getLogout() ?>" class="remember" style="color: #3B599F; float:right">Sign
							Out</a>
					</div>
				<?php endif; ?>

				<div style="clear: both;"></div>
			</div>

		</div><!--/.nav-collapse -->
	</div>
</div>

<div style="clear: both;"></div>
