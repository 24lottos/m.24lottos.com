<div class="select-style">
	<div class="drop-downbox" id="lottery_dropdown">
		<?php
		$order = isset($_GET['o']) ? $_GET['o']:null;
		$oLotteryList = new Lottery;
		$rUpcomingLotteries = $oLotteryList->upcomingLotteries();
		$firstLottery = [ ];
		$firstLotteryID = '';
		$count = 0;
		$impressions = [ ];
		foreach( $rUpcomingLotteries as $t ) {
			$timeToDraw = $t['NextPurchaseDeadline'];
			$stopT = date( 'Y-m-d H:i:s');
			if( !ENVIRONMENT ) { // do not hide lottery on DEV
				$stopT = $timeToDraw; // do not hide lottery on DEV
				$t['CurrentJackpot'] = !$t['CurrentJackpot'] ? 1:$t['CurrentJackpot'];
			}
			if( !$timeToDraw[0] || $timeToDraw<$stopT || !(int)$t['CurrentJackpot'] ) continue;

			$gameName = explode( '-', $t['DisplayName'] );
			if( $count==0 ) {
				array_push( $firstLottery, $t );
				$firstLotteryID = 'featured_lottery_'.$t['GameId'];
			}
			$_n = str_replace('"','&quot;',$t['DisplayName']);//loterry name
			$_b = str_replace('"','&quot;',$t['CurrentJackpot']); // Jackpot value
			// OPTION block --start--
			?>
			<div class="dev-lottery-option<?= $count == 0 ? ' curlottery':'' /**/ ?>"
				 id="featured_lottery_<?= $t['GameId'] ?>"
				 data-balls="<?= $t['GameDetails']['Balls'] ?>"
				 data-extraballs="<?= $t['GameDetails']['Extra_Balls'] ?>"
				 data-ballsmaxnum="<?= $t['GameDetails']['Balls_max_number'] ?>"
				 data-extraballsmaxnum="<?= $t['GameDetails']['Extra_Ball_max_number'] ?>"
				 data-price="<?= $t['GameDetails']['PRICE'] ?>"
				 data-currency="<?= $t['JackpotCurrency'] ?>"
				 data-pdtid="<?= $t['GameDetails']['PDTID'] ?>"
				 data-jackpot-encoded="<?php echo base64_encode( $t['CurrentJackpot'] ); ?>"
				 data-gamename="<?= $t['GameName'] ?>"
                 data-displayname="<?= $t['DisplayName'] ?>"
                 data-alias="<?= $t['alias'] ?>"
				 data-drawdate="<?= $timeToDraw ?>"
				 data-jackpot="<?= getCurrencySign( $t['JackpotCurrency'] ) ?> <?= number_format( $t['CurrentJackpot'] ) ?>"
				 onclick='flClick(this,"<?= $_n ?>","<?= $_b ?>")'
			>
				<div class="logo-dec">
					<img class="logo img-responsive" title="img" alt="<?= $t['DisplayName'] ?>" src="<?= $site->baseURL( 'images/lottery_logos/small_logos/'.$t['Small_Image_url'] ) ?>"><br/>
					<span><?= $gameName['0'] ?>-</span><?= $gameName['1'] ?>
				</div>
				<div class="jp-dec">
					<?= $NOINDEX['on'] ?>
					<i><span style="position:  relative;
                                    background:  none;
                                    padding-left: 0;
                                    font-size: 20px;
                                    bottom: 0;
                                    left: 0;"
                             class="symbol" data-symbol="<?= getCurrencySign($t['JackpotCurrency']) ?>"></span> <?= number_format($t['CurrentJackpot'],0) ?></i>
					<span data-cntdwn="<?= $timeToDraw ?>"></span>
					<?= $NOINDEX['off'] ?>
				</div>
			</div>
			<?php
			// OPTION block --end--
			$impressions[] = [
				'name'    => "'".$t['GameName']."'",
				'brand'   => "'".$t['CurrentJackpot']."'",
				'category'=> 'numbers',
				'position'=> $count,
				'list'    => 'mainPage',
			];
			$count++;
		}
		?>
	</div>

	<div class="select-box" id="lottery_select">
		<div>
			<div id="lottery_select_name">
				<div class="logo-dec">
					<img class="logo" id="lottery_select_image" src="<?= $site->baseURL( 'images/lottery_logos/small_logos/'.$firstLottery['0']['Small_Image_url'] ) ?>" alt="<?= $firstLottery['0']['DisplayName']; ?>" title="<?= $firstLottery['0']['DisplayName']; ?>"><br/>
					<?= $firstLottery['0']['DisplayName'] ?>
				</div>
				<div class="jp-dec">
					<?= $NOINDEX['on'] ?>
					<i><?= getCurrencySign( $firstLottery['0']['JackpotCurrency'] ).' '.number_format($firstLottery['0']['CurrentJackpot']) ?></i>
					<span data-cntdwn="<?php
						$timeToDraw = $firstLottery['0']['NextPurchaseDeadline'];
						echo $timeToDraw ?>"></span>
					<?= $NOINDEX['off'] ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
//emsgd($firstLottery);
?>

<script>
	var hiddenOpt="#<?= $firstLotteryID ?>";
    $(document).ready(function ($) {
		$(hiddenOpt).hide();
    });
	function flClick(o,n,b) {
		$(hiddenOpt).show();
//        $('#how-to-play-link a').attr('href', '/lottery/' + $(o).data('alias') + '#lottery-how-to-play');
//        $('#how-to-play-link a').text('How to Play ' + $(o).data('displayname'));
		hiddenOpt="#"+o.id;
		$(hiddenOpt).hide();
		$('#lottery_dropdown').fadeToggle(200);
<?php if( GAENABLED ) { ?>
		dataLayer.push({
			'event': 'lottos',
			'eventCategory': 'numbers',
			'eventAction': 'click',
			'eventLabel': 'play',
			'ecommerce': {
				'click': {
					'actionField': {
						"list": "mainPage" // list name
					},
					'products': [{
						'name': n, //loterry name
						'brand': b, // Jackpot value in USD
						'category': 'numbers', //for numbers always =numbers
					}]
				}
			}
		});
<?php } ?>
		return;
	}
</script>
