<?php
require_once __DIR__."/bootstrap.php";
if(isset($_GET['unset'])) {
    emsgd($_SESSION['ref']);
    unset($_SESSION['ref']);
}

if( !isset($_SESSION['ref']) ) {
    $_SESSION['ref'] =  isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'undefined';
}

ob_start();

$site   = new Site;
$oUser  = new User;
$oEmail = new Email;
$oPixel = new Pixel;
$oGen   = new General;

if( !isset($_SESSION['logged_id']) || !$_SESSION['logged_id'] ) {
    if( $oUser->doAutoLogin() ) $oUser->autoLoginCookie( false );
}

$detect = new MobileDetect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet':'phone'):'computer');
if( $deviceType == 'tablet' ):
    $_SESSION['siteMode'] = 'desktop';
    $toURL = BASE_URL.substr( $_SERVER["REQUEST_URI"], 1 ); // BASE_URL contains '/' at the end
    echo "<script>window.location='".$toURL."'</script></head></html>"; // <html><head> already printed out!
    exit;
else:
    $_SESSION['siteMode'] = 'mobile';
endif;

$p = $_GET ? @$_GET['p']:'home';
$sp = isset($_GET['sp']) ? @$_GET['sp']:null;
$invitation = isset($_GET['invitation']) ? @$_GET['invitation']:null;
$pmeta = $p=='promotion' ? "$p-".$_GET['l'] : $p;

// add last results data for some pages
if( in_array($p,['lottery','results','lottery-archive']) ) {
    require_once BASE_PATH ."config/last_results_data.php";
}

extract( $_SESSION );

if( !$oUser->loginCheck() && isset($_COOKIE[AUTO_LOGIN_COOKIE]) ) {
    $oUser->doAutoLogin( $_COOKIE[AUTO_LOGIN_COOKIE], $site );
} //else $oUser->updateLastVisitOnly();

$meta = ( new Page() )->getMeta( $pmeta );
if( empty($meta->txtmetadescription) ) $meta->txtmetadescription = !array_key_exists( $p, $metaTitle ) ? $meta->vartitle:$metaTitle[$p];

$refTail = '';
if( !empty($_GET['ref']) ) {
	$refTail = '. Partner ID:'.(int)$_GET['ref'];
	if( !empty($_GET['username']) ) $refTail.= ', '.ucwords(trim($_GET['username']));
	if( strstr(':;,.', substr($meta->vartitle,-1))!==false ) $meta->vartitle = substr($meta->vartitle,0,-1);
	if( strstr(':;,.', substr($meta->txtmetadescription,-1))!==false ) $meta->txtmetadescription = substr($meta->txtmetadescription,0,-1);
}

if( GAENABLED ) { ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';
	j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TRKHWV');</script>
<!-- End Google Tag Manager -->
<?php } ?>
<meta charset="utf-8">
<title><?= gitBranch(ENVIRONMENT).$meta->vartitle.$refTail ?></title>
<meta name="description" content="<?= $meta->txtmetadescription.$refTail ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta name="author" content="">
<meta name="robots" CONTENT="index, follow">
<meta name="google-site-verification" content="-eY8YFGpWBtpIv8jNE7K44QPiWAugui-IBpcsdNsGmA"/>
<base href="<?= BASE_URLm ?>"/>
<script type="text/javascript">
    var ENVIRONMENT=<?= (int)ENVIRONMENT ?>;
    var NO_TICKETS='<?= str_replace("'", "\\'", NO_TICKETS) /*"*/ ?>';
</script>
<? // add jquery on top for some pages
if ( in_array($p,['deposit','wirecard','3gdp','cashenvoy','flutterwave','how-to-play','how-to-fund'])
    || stristr($_SERVER["SCRIPT_FILENAME"],'iframe') ) { ?>
    <script src="<?= BASE_URL ?>assets/js/jquery-latest.js?v=<?= JSVERSION ?>"></script>
<? } ?>
<?php
$head_inline_css_file = BASE_PATHm .'css/head-inline.min.css';
$head_inline_css = file_get_contents( $head_inline_css_file );
?>
<style id="css-head_inline_css"><?=$head_inline_css?></style>

<?php
$head_inline_js_file = BASE_PATHm .'js/head-inline.min.js';
$head_inline_js = file_get_contents( $head_inline_js_file    );
?>
<script type="text/javascript" id="js-head_inline_js"><?=$head_inline_js?></script>

<link rel="shortcut icon" href="<?= BASE_URL ?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?= BASE_URL ?>favicon.ico" type="image/x-icon">
<link rel="canonical" href="<?= substr( BASE_URL, 0, -1 ).$_SERVER['REQUEST_URI'] ?>">

<?php

$oPixel->getTrackingCode( $p, $_GET );
while( $oPixel->nextRecord() ) echo html_entity_decode( $oPixel->Record->shortcode );

?>
<script type="text/javascript">
    var mlotto = {};
    mlotto.baseURL = "<?= BASE_URL ?>";
    mlotto.baseURLm = "<?= BASE_URLm ?>";
</script>
<?php
$url = $_SERVER['REQUEST_URI']; //get_permalink();
$parts = array_filter(explode('/', $url));
$breadcramb_id = $_SERVER['SERVER_NAME'];
?>
<?php if ( $p != 'home'){ ?>
    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": [{
        "@type": "ListItem",
        "position": 1,
        "item": {
          "@id": "<?php echo $breadcramb_id ?>",
          "name": "24Lottos"
        }
      },{
        "@type": "ListItem",
        "position": 2,
        "item": {
          "@id": "<?php $breadcramb_id .= "/" .$parts[1]; echo $breadcramb_id ?>",
          "name": "<?php echo $parts[1] ?>"
        }
            <?php if(isset($parts[2])) { ?>
      },{
        "@type": "ListItem",
        "position": 3,
        "item": {
          "@id": "<?php $breadcramb_id .= "/" .$parts[2]; echo $breadcramb_id ?>",
          "name": "<?php echo ( new Lottery() )->getLottoName($parts[2]); ?>"
        }
            <?php } if(isset($parts[3])) { ?>
      },{
        "@type": "ListItem",
        "position": 4,
        "item": {
          "@id": "<?php $breadcramb_id .= "/" .$parts[3]; echo $breadcramb_id ?>",
          "name": "<?php echo $parts[3] ?>"
        }
            <?php } ?>
      }]
    }
    </script>
<?php } ?>
