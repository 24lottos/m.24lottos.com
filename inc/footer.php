<?php include('inc/faq.php'); ?>

<footer>
	<ul>
		<li><a href="<?= BASE_URLm ?>privacy-policy">Privacy Policy</a></li>
		<li><a href="<?= BASE_URLm ?>terms-and-conditions">Terms & Conditions</a></li>
		<li><a href="<?= BASE_URL ?>?sitemode=desktop">Desktop Site</a></li>
<!--<li><a href="http://www.24monetize.com/" target='_blank'>Affiliate Program</a></li>-->
	</ul>
</footer>
</div>
</div>
</div>

<script type="text/javascript">var countryCode = "<?= !ENVIRONMENT ? 'IN':geoip_country_code_by_name( $_SERVER['REMOTE_ADDR'] )?>";</script>
<?php echo "<script type='text/javascript'> var visitorCountry='{$site->country->code}'</script>";?>

<?php
$footer_inline_js_file = BASE_PATHm .'js/footer-inline.min.js';
$footer_inline_js = file_get_contents( $footer_inline_js_file );
?>
<script type="text/javascript" id="js-footer_inline_js"><?=$footer_inline_js?></script>

<script type="text/javascript">
    /* global cfg, fallback, req */
    cfg({
//        "debug": true,
//        "globals": true,

        "libs": {
            "jQuery172": {
                "alias": ["$", "jq", "jquery"],
                "exports": ["jQuery172"],
                "urls": [
                    "<?=BASE_URL?>assets/js/jquery-latest.js?v=<?=JSVERSION?>"
                ]
            },

            "jqueryui": {
                "exports": "jQuery.ui",
                "deps": "$",
                "urls": [
                    "<?=BASE_URL?>js/common/jquery-ui.js?v=<?=JSVERSION?>"
                ]
            },

            "jquery_countdown": {
                "alias": "countdown",
                "deps": ["$"],
                "urls": [
                    "<?=BASE_URL?>assets/countdown/jquery.countdown.min.js?v=<?=JSVERSION?>"
                ]
            },

            "jquery_validate": {
                "alias": "validate",
                "deps": "$",
                "urls": [
                    "<?=BASE_URL?>assets/js/jquery.validate.js?v=<?=JSVERSION?>"
                ]
            },

            <?php
            if( isset($_GET['p']) ) {
                switch( $_GET['p'] ) {
                    case 'winners':
                        ?>
                        "dataTables": {
                            "alias": "dataTables",
                            "deps": ["$"],
                            "urls": [
                                "<?=BASE_URL?>assets/data-tables/jquery.dataTables.min.js?v=<?=JSVERSION?>"
                            ]
                        },

                        "DT_bootstrap": {
                            "alias": "DT_bootstrap",
                            "deps": ["dataTables"],
                            "urls": [
                                "<?=BASE_URL?>assets/data-tables/DT_bootstrap.min.js?v=<?=JSVERSION?>"
                            ]
                        },
                        <?php
                        break;

                    case 'account':
                    case 'customer-support':
                        ?>
                        "intlTelInput": {
                            "alias": "intlTelInput",
                            "deps": ["$"],
                            "urls": [
                                "<?=BASE_URL?>js/intlTelInput.min.js?v=<?=JSVERSION?>"
                            ]
                        },
                        <?php
                        break;
                }
            }
            ?>
        }
    });

    req(function( jquery_validate, jquery_countdown, jqueryui, jQuery172 ) {
        loadJS('<?=BASE_URL?>js/account.min.js?v=<?=JSVERSION?>');
        loadJS('<?=BASE_URLm?>js/lottery.min.js?v=<?=JSVERSION?>');
        loadJS('<?=BASE_URLm?>/src/js/lib/common.js?v=<?=JSVERSION?>');
    });

    loadCSS('<?= BASE_URLm.'css/main.min.css?v='.CSSVERSION ?>');
    loadCSS('<?= BASE_URLm.'css/font-awesome.min.css?v='.CSSVERSION ?>');

</script>
<?php
/* fetch custom scripts */
if( isset($_GET['p']) ) {
    switch( $_GET['p'] ) {
        case 'winners':
            ?>
            <script type="text/javascript">
                req(function( DT_bootstrap, dataTables ) {
                    loadJS('<?=BASE_URL?>js/winners.min.js?v=<?=JSVERSION?>');
                    loadCSS('<?= BASE_URL.'assets/data-tables/DT_bootstrap.min.css?v='.CSSVERSION ?>');
                });
            </script>
            <?php
            break;
        case 'login':
            break;
        case 'home':
        case 'deposit':
        case 'vcheckout':
        case 'lottery':
            break;
        case 'dashboard':
            break;
        case 'tickets':
            break;
        case 'account':
        case 'customer-support':
            ?>
            <script type="text/javascript">
                req(function( intlTelInput ) {
                    //console.log('intlTelInput');
                    loadCSS('<?= BASE_URL.'assets/css/intlTelInput.css?v='.CSSVERSION ?>');
                });
            </script>
            <?php
            break;
        case 'signup':
            if( ENVIRONMENT ) echo "<script type='text/javascript'> var visitorCountry = '".geoip_country_code_by_name( $_SERVER['REMOTE_ADDR'] )."'</script>"; else
                echo "<script type='text/javascript'> var visitorCountry = 'IN'</script>";
            break;
    }
}

$oSeo = new Seo;
echo stripslashes( $oSeo->getTrackingCode() );
?>

<?php
if( ENVIRONMENT ) { ?>
<!-- start Mixpanel -->
<script type="text/javascript">(function (f, b) {
	if (!b.__SV) {
		var a, e, i, g;
		window.mixpanel = b;
		b._i = [];
		b.init = function (a, e, d) {
			function f(b, h) {
				var a = h.split(".");
				2 == a.length && (b = b[a[0]], h = a[1]);
				b[h] = function () {
					b.push([h].concat(Array.prototype.slice.call(arguments, 0)))
				}
			}

			var c = b;
			"undefined" !== typeof d ? c = b[d] = [] : d = "mixpanel";
			c.people = c.people || [];
			c.toString = function (b) {
				var a = "mixpanel";
				"mixpanel" !== d && (a += "." + d);
				b || (a += " (stub)");
				return a
			};
			c.people.toString = function () {
				return c.toString(1) + ".people (stub)"
			};
			i = "disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
			for (g = 0; g < i.length; g++)f(c, i[g]);
			b._i.push([a, e, d])
		};
		b.__SV = 1.2;
		a = f.createElement("script");
		a.type = "text/javascript";
		a.async = !0;
		a.src = "//cdn.mxpnl.com/libs/mixpanel-2.2.min.js";
		e = f.getElementsByTagName("script")[0];
		e.parentNode.insertBefore(a, e)
	}
})(document, window.mixpanel || []);
mixpanel.init("4c407eddcfb8649b33171025e8b76df9");
</script><!-- end Mixpanel -->
<?php

 if( $_GET['p']=='home' ) { ?>
<script type="application/ld+json">
{
	"@context" : "http://schema.org",
	"@type" : "Organization",
	"name" : "24lottos.com",
	"url" : "https://www.24lottos.com/",
	"logo" : "https://www.24lottos.com/assets/img/logo.png",
	"sameAs" : [
		"https://plus.google.com/+24lottos",
		"https://www.facebook.com/24Lottos/",
		"https://twitter.com/24Lottos/",
                "https://www.instagram.com/24Lottos/"
	],
        "address": {
        "@type": "PostalAddress",
        "streetAddress": "31 Southampton Row",
        "addressRegion": "London",
        "postalCode": "WC1B 5HJ",
        "addressCountry": "EN"
        }
}
</script>
<script type="application/ld+json">
{
	"@context" : "http://schema.org",
	"@type" : "WebSite",
	"name" : "24lottos.com",
	"url" : "https://www.24lottos.com/",
	"potentialAction" : {
		"@type": "SearchAction",
		"target": "https://www.24lottos.com//?s=={search_term_string}",
		"query-input": "required name=search_term_string"
	}
}
</script>
<?php
 }
} ?>
<?php if( in_array( $p, ['customer-support'] ) ): ?>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/57a83c1de66e70fb764a5848/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
<?php endif; ?>
</body>
</html>
<?php ob_flush(); flush();
