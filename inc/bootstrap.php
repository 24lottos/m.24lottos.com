<?php
/**
 * Created by IntelliJ IDEA.
 * User: Home
 * Date: 19.05.2017
 * Time: 11:34
 */
define('IS_MOBILE',true);
require_once(__DIR__.'/../config/config.php');
require_once(PATH2CONFIG.'constants.php');
require_once(BASE_PATH.'config/functions.php');
require_once __DIR__.'/../vendor/autoload.php';
if(!ENVIRONMENT) require_once __DIR__.'/emsgd.php';

spl_autoload_register(function($class){
    $lower = strtolower( $class );
    include PATH2CONFIG."class.{$lower}.php";

});


session_status() === PHP_SESSION_ACTIVE || session_start();