<h5 class="pageHeading blue-list">Latest <?= $oLottery->Record->lotteryNameModified ?> Results</h5>
<img src='/img/px.gif' alt='Latest Results' class='icon results'/>
<div class="col-lg-12 no-padding">
	<?= $oLottery->Record->latestResults ?>
	<div class="table-responsive centered">
		<table class="table table-striped bmgrid yourLotteryPicksTable">
			<thead>
			<tr>
				<th>Date</th>
				<th>Result</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$oLotteryResultsArchive = new Lottery();
			$lotteryArchiveName = strtolower( str_replace('-', '', $lotteryName ) );
			$lotteryArchiveName = ( array_key_exists( $lotteryArchiveName, $alias_archive_name) ) ? $alias_archive_name[$lotteryArchiveName] : $lotteryArchiveName;
			$qLotteryResultsArchive = "
				SELECT lotto_rrc_results_archive.datetime, lotto_rrc_results_archive.winnumbers,
					   lotto_rrc_results_archive.timezone
				  FROM lotto_rrc_results_archive
				 WHERE lotto_rrc_results_archive.lotteryname = '" . $lotteryArchiveName."'
				 ORDER BY lotto_rrc_results_archive.datetime DESC
				 LIMIT " .NUM_ROWS_FROM_ARCHIVE;
			try {
				$oLotteryResultsArchive->query($qLotteryResultsArchive);
			} catch (ErrorException $message) {}

			$has_archive = ( $oLotteryResultsArchive->numRows() && !isset($message) ) ? true : false;
			while( $oLotteryResultsArchive->nextRecord() ) {
				$winnumbers = json_decode( $oLotteryResultsArchive->Record->winnumbers );

				$current_lottery_day = new \DateTime( 'now', new \DateTimeZone( $oLotteryResultsArchive->Record->timezone ) );
				$current_lottery_day->setTime( 0, 0, 0 );

				$draw_date = new \DateTime( $oLotteryResultsArchive->Record->datetime, new \DateTimeZone( $oLotteryResultsArchive->Record->timezone ) );
				$draw_date->setTime( 0, 0, 0 );

				$diff = $current_lottery_day->diff( $draw_date );
				$diffDays = intval( $diff->format( "%R%a" ) );

				$relative_to_today = '';
				switch( $diffDays ){
					case 0: $relative_to_today = 'Today, '; break;
					case -1: $relative_to_today = 'Yesterday, '; break;
					default: $relative_to_today = ''; break;
				}

				$draw_date = date( 'd M', strtotime( $oLotteryResultsArchive->Record->datetime ) );
				?>
				<tr>
					<td class="centered"><?= $relative_to_today .$draw_date ?></td>
					<td class="centered">
						<?php foreach( $winnumbers->numbers as $number ): ?>
							<div class="red-circle">
								<span class="white"><?= $number ?></span>
							</div>
						<?php endforeach; ?>
						<?php foreach( $winnumbers->numbers_extra as $number ): ?>
							<div class="red-circle" style="background:#666">
								<span class="white"><?= $number ?></span>
							</div>
						<?php endforeach; ?>
					</td>
				</tr>
			<?php } ?>

			</tbody>
		</table>
		<?php if ( $has_archive ) { ?>
			<a href="<?= $site->baseURLM() ?>lottery/<?= $lotteryName ?>/results" id="show_more_results" class="btn2 btn-orange pull-right checkoutBth">
				<div>More results &nbsp;<i class="fa fa-arrow-circle-right"></i></div>
			</a>
		<?php } ?>
	</div>
</div>
