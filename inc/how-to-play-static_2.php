<div class="row col-lg-12">
	<!-- Swiper -->
	<div class="swiper-container">
		<div class="swiper-wrapper">
			<div class="swiper-slide">
				<div class="row col-lg-12">
					<div class="row col-lg-12 nopad">
						<div class="howtoplayheading"><b class="howtoplaybullet">1</b>Select <span>lottery</span>,
							Select <span>game type</span> and Place your <span>bet</span></div>
					</div>
					<div class="col-lg-12 howtoplayimagecontainer">
						<img class="img-responsive"
							 src="<?= $site->baseURL( 'images/howitworks/howtoplay-01.jpg' ) ?>"/>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="row col-lg-12">
					<div class="row col-lg-12 nopad">
						<div class="howtoplayheading col-lg-12"><b class="howtoplaybullet">2</b>Choose your lucky
							numbers and click <span class="green">PLAY</span> If you don't have your set
							of numbers, use the <span class="yellow">iForecast</span> button.
						</div>
					</div>
					<div class="col-lg-12 howtoplayimagecontainer">
						<img class="img-responsive"
							 src="<?= $site->baseURL( 'images/howitworks/howtoplay-02.jpg' ) ?>"/>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="row col-lg-12">
					<div class="row col-lg-12 nopad">
						<div class="howtoplayheading  col-lg-12"><b class="howtoplaybullet">3</b>After clicking on
							<span>PLAY</span>, The bet will be added below.Click <span class="red">Checkout</span> To
							continue.
						</div>
					</div>
					<div class="col-lg-12 howtoplayimagecontainer">
						<img class="img-responsive"
							 src="<?= $site->baseURL( 'images/howitworks/howtoplay-03.jpg' ) ?>"/>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="row col-lg-12">
					<div class="row col-lg-12 nopad">

						<div class="howtoplayheading col-lg-12"><b class="howtoplaybullet">4</b>If you already have an
							account, <span>Login</span>. If not, click on <span class="red">Register now</span>. You can
							also use our <span>Facebook login</span> for a quick and easy login.
						</div>
					</div>
					<div class="col-lg-12 howtoplayimagecontainer">
						<img class="img-responsive"
							 src="<?= $site->baseURL( 'images/howitworks/howtoplay-05.jpg' ) ?>"/>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="row col-lg-12">
					<div class="row col-lg-12 nopad">

						<div class="howtoplayheading col-lg-12"><b class="howtoplaybullet">5</b>Fill our our
							registration form and you are one step from playing your numbers for real. Good Luck
						</div>
					</div>
					<div class="col-lg-12 howtoplayimagecontainer">
						<img class="img-responsive"
							 src="<?= $site->baseURL( 'images/howitworks/howtoplay-06.jpg' ) ?>"/>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="row col-lg-12">
					<div class="row col-lg-12 nopad">

						<div class="howtoplayheading col-lg-12"><b class="howtoplaybullet">6</b>Choose your payment
							option and follow the direction on page.
						</div>
					</div>
					<div class="col-lg-12 howtoplayimagecontainer">
						<img class="img-responsive"
							 src="<?= $site->baseURL( 'images/howitworks/howtoplay-07.jpg' ) ?>"/>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="row col-lg-12">
					<div class="row col-lg-12 nopad">

						<div class="howtoplayheading col-lg-12"><b class="howtoplaybullet">7</b>After deposit, go to
							your dashboard and click on <span class="red">Checkout</span> again to get your bets LIVE
						</div>
					</div>
					<div class="col-lg-12 howtoplayimagecontainer">
						<img class="img-responsive"
							 src="<?= $site->baseURL( 'images/howitworks/howtoplay-08.jpg' ) ?>"/>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="row col-lg-12">
					<div class="row col-lg-12 nopad">

						<div class="howtoplayheading col-lg-12"><b class="howtoplaybullet">8</b>After the payment and
							Checkout you will see your bets at your dashboard as <span>Active Tickets</span>. We'are
							crossing our fingers for you!
						</div>
					</div>
					<div class="col-lg-12 howtoplayimagecontainer">
						<img class="img-responsive"
							 src="<?= $site->baseURL( 'images/howitworks/howtoplay-09.jpg' ) ?>"/>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="row col-lg-12">
					<div class="row col-lg-12 nopad">

						<div class="howtoplayheading col-lg-12"><b class="howtoplaybullet">9</b>Get ready to count your
							winnings
						</div>
					</div>
					<div class="col-lg-12 howtoplayimagecontainer">
						<img class="img-responsive"
							 src="<?= $site->baseURL( 'images/howitworks/howtoplay-10.jpg' ) ?>"/>
					</div>
				</div>
			</div>
		</div>
		<!-- Add Pagination -->
		<div class="swiper-pagination"></div>
		<!-- Add Arrows -->
		<div class="swiper-button-next"></div>
		<div class="swiper-button-prev"></div>
	</div>

	<!-- Swiper JS -->
	<link rel="stylesheet" href="<?= $site->baseURL( 'assets/swiper/css.css' ) ?>">
	<link rel="stylesheet" href="<?= $site->baseURL( 'assets/swiper/slider.css' ) ?>">
	<script src="<?= $site->baseURL( 'assets/swiper/js.js' ) ?>"></script>

	<!-- Initialize Swiper -->
	<script>
		var swiper = new Swiper('.swiper-container', {
			pagination: '.swiper-pagination',
			nextButton: '.swiper-button-next',
			prevButton: '.swiper-button-prev',
			slidesPerView: 1,
			paginationClickable: true,
			spaceBetween: 30,
			loop: true
		});
	</script>

</div>
