<?php
/**
 * header.php - HTML <head> part and top of the page
 */
?><!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once('inc/header_head.php') ?>
</head>

<body class="cbp-spmenu-push">

<?php if( GAENABLED ) { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TRKHWV" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php } // if( GAENABLED ) /

unset($_SESSION['loginStatus']);

?>
<div id="loading-overlay"></div>

<div class="overlay" id="bookmark_overlay">
    <div class="overlay_inner">
        <img src="img/close.png" class="close_overlay" id="bkmoc" alt='close'>
        <?php /* <span>Dummy text Dummy text Dummy text Dummy text</span> */ ?>
        <div class="bookmark_div">
            <img id="bookmark_btn" src="img/bookmark-button.png" alt='bookmark'>
        </div>
    </div>
</div>
<!-- Nav -->
<div class="modal-backdrop in nav"></div>
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">

    <?php /*
	 if( $oUser->loginCheck() ) {
		<div class="nav-sectionbox">
			<a href="<?= BASE_URLm ?>account/dashboard" class="dashboard_link">Dashboard</a>
			<a href="<?= BASE_URLm ?>account/deposit" class="quick_deposit" onclick="confirmBtnClick('quickDeposit')">Quick Deposit</a>
		</div>
	}
	// removed 2017-04-17: <a href="<?= BASE_URLm ?>account/account"><i class="fa fa-user" aria-hidden="true" title="Account"></i> Account</a>
*/ ?>

    <div class="account-sectonbox">
        <a href="/" class="white_tab"><i class="fa fa-home" aria-hidden="true" title="Home"></i> Home</a>
        <a href="<?= BASE_URLm ?>results" class="white_tab"><i class="fa fa-bullhorn" aria-hidden="true" title="Results"></i> Results</a>
    </div>

</nav>
<!-- /Nav -->
<div class="container">
    <div class="main">
        <div class="section">
            <header style='text-align:center'>
                <button type="button" class="navbar-toggle" id="showLeft">
                    <i class="fa fa-bars" aria-hidden="true" style="font-size:25px"></i>
                </button>

                <a href="<?= BASE_URLm ?>" class='hdrlogo'><img src="img/logo.png" alt="logo" title="logo"/></a>

                <?php
                $oLottery = new Lottery;
                $oLottery->areNumbersPicked( session_id() );
                $pendTicNum = $oLottery->numRows();
                ?>
            </header>
            <?php
            /* message right after a SignUp */
            if( @$_SESSION['showSignupGreeting'] ) { ?>
                <div class="success-msg"><i class="fa fa-star"></i> <?= SIGNUPGREETING ?></div>
                <?php
                unset($_SESSION['showSignupGreeting']);
            } ?>

            <div id="hello">
            <div class="container whitebg">
                <?php
//                $lotteryName = trim($_GET['l']);
//                $lotteryName = ( $lotteryName == 'baba-ijebu' ) ? 'baba-ijebu-international' : $lotteryName;
//
//                $_Lotteries = $oLottery->upcomingLotteries( null, true );
//                $lotteryFound = false;
//                foreach( $_Lotteries as $t ) {
//                    if( stringURLSafe($t['alias'])==$lotteryName ) {
//                        $sLotteryID = $t['GameId'];
//                        $sLottery = $t['GameName'];
//                        $sLotteryJackPot = $t['CurrentJackpot'];
//                        $sLotteryJackPotCurrency = $t['JackpotCurrency'];
//                        $isOmitted   = $t['isOmitted'];
//                        $sTimeNow    = gmdate( 'Y-m-d H:i:s' );
//                        $sTimeToDraw = $t['NextPurchaseDeadline'];
//                        $sTimeToDraw = $isOmitted || $sTimeToDraw[0]==0 ? $sTimeNow : $sTimeToDraw;
//                        $lotteryFound = true;
//                        break;
//                    }
//                }
//
//                unset($_Lotteries);
//                $oLottery->getDetails( $sLottery );
//
//                $name = explode( '-', $oLottery->Record->lotteryNameModified );
//                if( count($name)<2 ) $name[1] = '';
//                $lottery_full_name = trim($name['0']) ." " .trim($name['1']);
//
//                $default_results_description = "See the official ".$lottery_full_name ." results for the last 10 draws.";
//                global $results_description;
                ?>
                <section id="msection">
                    <!-- blue header Html Here -->
                    <article>
                        <div class="blue-header">
                            <div class="center">
                                <span class="white font11">Baba Ijebu Results</span>
                            </div>
                        </div>
                        <!-- blue header Html End Here -->
                        <div>
                            <p>Here at 24Lottos you will find the latest all Baba Ijebu lotteries results.</p>
                        </div>
                        <div id="baba-ijebu-lotteries">
                            <ul>
                                <li id="Bingo-nav">
                                    <a href="">Bingo</a>
                                </li>
                                <li id="Enugu-nav">
                                    <a href="">Enugu</a>
                                </li>
                                <li id="International-nav" class="active">
                                    <a href="">International</a>
                                </li>
                                <li id="National-nav">
                                    <a href="">National</a>
                                </li>
                                <li id="Lucky-nav">
                                    <a href="">Lucky</a>
                                </li>
                                <li id="Peoples-nav">
                                    <a href="">Peoples</a>
                                </li>
                            </ul>
                        </div>
                        <!-- Table  str Html Here -->
                        <?php
                        global $baba_ijebu_lotteries;

                        foreach ($baba_ijebu_lotteries as $lotteryName => $lotteryArchiveName) {
                            ?>
                            <div id="<?= $lotteryName ?>" style="display: <?= ( $lotteryName == 'International' ) ? 'block' : 'none'; ?>" class="lottery-results">
                                <h2>Latest results for <?= $lotteryName ?> lottery</h2>
                                <table class="table table-striped bmgrid yourLotteryPicksTable">
                                    <thead>
                                    <tr>
                                        <th class="centered" style="width: 20%;">Date</th>
                                        <th class="centered">Results</th>
                                    </tr>
                                    </thead>
                                    <tbody style="font-weight: 500;color: #333;">
                                    <?php
                                    $oLotteryResultsArchive = new Lottery();

                                    //                                $lotteryArchiveName = $baba_ijebu_lotteries[$lotteryName];
                                    $qLotteryResultsArchive = "SELECT
                                      lotto_rrc_results_archive.datetime,
                                      lotto_rrc_results_archive.winnumbers,
                                      lotto_rrc_results_archive.timezone
                                    FROM lotto_rrc_results_archive
                                    WHERE lotto_rrc_results_archive.lotteryname = '" . $lotteryArchiveName."'
                                    ORDER BY lotto_rrc_results_archive.datetime DESC
                                    LIMIT " .NUM_ROWS_ON_ARCHIVE;
                                    try {
                                        $oLotteryResultsArchive->query($qLotteryResultsArchive);
                                    } catch (ErrorException $message) {}

                                    $has_archive = ( $oLotteryResultsArchive->numRows() && !isset($message) ) ? true : false;
                                    if ( $has_archive ) {
                                        while( $oLotteryResultsArchive->nextRecord() ):
                                            $winnumbers = json_decode( $oLotteryResultsArchive->Record->winnumbers );

                                            $current_lottery_day = new \DateTime( 'now', new \DateTimeZone( $oLotteryResultsArchive->Record->timezone ) );
                                            $current_lottery_day->setTime( 0, 0, 0 );

                                            $draw_date = new \DateTime( $oLotteryResultsArchive->Record->datetime, new \DateTimeZone( $oLotteryResultsArchive->Record->timezone ) );
                                            $draw_date->setTime( 0, 0, 0 );

                                            $diff = $current_lottery_day->diff( $draw_date );
                                            $diffDays = intval( $diff->format( "%R%a" ) );

                                            $relative_to_today = '';
                                            switch( $diffDays ){
                                                case 0: $relative_to_today = 'Today, '; break;
                                                case -1: $relative_to_today = 'Yesterday, '; break;
                                                default: $relative_to_today = ''; break;
                                            }

                                            $draw_date = date( 'd M', strtotime( $oLotteryResultsArchive->Record->datetime ) );
                                            ?>
                                            <tr>
                                                <td class="centered" style="width: 20%;"><?= $relative_to_today .$draw_date ?></td>
                                                <td class="centered">
                                                    <?php foreach( $winnumbers->numbers as $number ): ?>
                                                        <div class="red-circle">
                                                            <span class="white"><?= $number ?></span>
                                                        </div>
                                                    <?php endforeach; ?>
                                                    <?php foreach( $winnumbers->numbers_extra as $number ): ?>
                                                        <div class="red-circle" style="background:#666">
                                                            <span class="white"><?= $number ?></span>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </td>
                                            </tr>
                                            <?php
                                        endwhile; ?>
                                        <?php
                                    }
                                    else {
                                        echo "No results archive for this lottery...";
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>

                        <style>
                            h2 {
                                font-size: 16px;
                                color: #3a599c;
                                text-align: center;
                            }
                            #baba-ijebu-lotteries {
                                width: 100%;
                                height: 50px;
                                background-color: #3a599c;
                                padding: 5px 15px;
                            }
                            #baba-ijebu-lotteries ul {
                                margin: 0;
                                padding: 0;
                            }
                            #baba-ijebu-lotteries ul li {
                                float: left;
                                list-style-type: none;
                                margin: 1px 0;
                                width: 33%;
                            }
                            #baba-ijebu-lotteries ul li.active a {
                                text-decoration: underline;
                            }
                            #baba-ijebu-lotteries ul li a {
                                color: white;
                            }
                        </style>
                        <!-- Table  str Html Here -->
                    </article>
                    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Table",
      "about": "<?= $lottery_full_name ?>  - Latest Results"
    }
    </script>
            </section>


            <script>
                $(document).ready(function () {
                    $('[data-countdown]').each(function () {
                        var $this = $(this), finalDate = $(this).data('countdown');
                        $this.countdown(finalDate, function (event) {
                            $this.html(event.strftime('%D days %H:%M:%S'));
                        });
                    });
                    $('#baba-ijebu-lotteries a').on('click', function (event) {

                        event = event || window.event;
                        if ( event.preventDefault() ) {
                            event.preventDefault();
                        } else {
                            event.returnValue = false;
                        }
//                    console.log(this.text);
                        $('#baba-ijebu-lotteries li').removeClass('active');
                        $('#' + this.text + '-nav').addClass('active');

                        $('.lottery-results').hide();
                        $('#' +this.text).show();
                    });
                });
            </script>
        </div> <!-- /container -->
    </div><!-- /hello -->

<?php

include('inc/footer.php');

// affiliate-pro - run it from a last line!
if( AFFPRO_PATH ) {
    $_include = AFFPRO_PATH.AFFPRO_TRACK_REF;
    if( is_file($_include) ) @include_once( $_include );
}