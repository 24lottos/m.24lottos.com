<?php
require_once('./config/config.php');
require_once(PATH2CONFIG.'constants.php');
require_once(BASE_PATH.'config/functions.php');

function __autoload($classname) {
    $lower = strtolower($classname);
	include("config/class.{$lower}.php");
}
$ce = new Cashenvoy;
$ce->doDeposit($_POST, new General, new User);
